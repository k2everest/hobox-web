FROM node:8.2

ARG NODE_ENV=development
ENV NODE_ENV=${NODE_ENV}

#RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY package*.json ./

# install node packages
RUN npm install

COPY . .

EXPOSE 8080

CMD ["npm","start"]
