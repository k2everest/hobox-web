//const webpack = require('webpack');
const merge = require('webpack-merge');
const commons = require('./webpack.commons.js');
const HOST = process.env.HOST || "0.0.0.0";
const PORT = process.env.PORT || "8080";

module.exports = merge(commons, {
    node: {
        fs: "empty"
     },
    devtool: 'source-map',
    module: {
        preLoaders: [
            {
                test: /\.ts[x]*$/,
                loader: "tslint"
            }
        ],
        rules: [{
            test: /\.tsx?$/,
            loader: "babel-loader",
            exclude: /node_modules/
        }]
    },
    tslint: {
        failOnHint: false
    },
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000,
        ignored: /node_modules/
    },
    devServer: {
        noInfo: true,
        hot: false,
        inline: true,
        open: true,
        overlay: {
            warnings: false,
            errors: true
        },
        historyApiFallback: {
            index: 'index.html',
            rewrites: [
                {from: /^(?!css|js|[^\\.]*\\.hot-update\\.json)/, to: '/index.html'}
            ]
        },
        port: PORT,
        host: HOST
    }
});
