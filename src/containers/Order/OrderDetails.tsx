import * as React from "react";
import { observer } from "mobx-react";
import { Route } from "react-router";
import { observable } from "mobx";
import stores from "../../stores/";
import { Row, Col, Panel, Grid } from "react-bootstrap";
import CustomJumbotron from "../../components/CustomJumbotron";
const Spinner = require("react-spinkit");
import Moment from "react-moment";
import { Button } from "react-bootstrap";
import { history } from "../Route/RouteStore";
import OrderItems from "./OrderDetails/OrderItems/";
import { formatBrMoney } from "../../helpers";
const queryString = require("qs");

//style
const s = require("./style.scss");

interface IProps {
  route: Route;
}

interface IState {}

@observer
export default class OrderDetails extends React.Component<IProps, IState> {
  @observable error: string = "";
  orderStore = stores.orderStore;
  @observable orderId = undefined;
  @observable packId = undefined;

  constructor(props: IProps, context: any) {
    super(props, context);
  }

  componentWillMount() {
    this.orderId = this.props;
    this.packId = this.props;
    this.orderId = this.orderId.match.params.orderId;
    this.packId = queryString.parse(this.packId.location.search, {
      ignoreQueryPrefix: true,
    });
    if (this.packId.packId) this.orderStore.getOrderCart(this.packId.packId);
    else if (this.orderId != undefined && this.packId.packId == undefined) {
      this.orderStore.getOrder(this.orderId);
    }
  }

  checkIsNull(value) {
    return value ? value : "";
  }

  render() {
    return (
      <div>
        {this.orderStore.inProgress === true ? (
          <Spinner
            name="line-scale-pulse-out"
            fadeIn="none"
            className={s.spinner}
          />
        ) : (
          <div className={s.content}>
            <Grid fluid={true}>
              <h2>Detalhes do Pedido</h2>
              <Row className="show-grid">
                <Col xs={12} md={6}>
                  <Panel header={"Origem"}>
                    <b>Código do pedido na origem:</b>
                    <p>{this.orderStore.order.id}</p>
                  </Panel>
                </Col>
                <Col xs={12} md={6}>
                  <Panel header={"Destino"}>
                    {this.orderStore.order.sync === "ok" ? (
                      <div>
                        <b>Código do pedido no destino:</b>
                        <p> {this.orderStore.order.code}</p>
                        <b> Exportado do Hobox para o Magento em:</b>
                        <p>
                          {" "}
                          <Moment format="DD/MM/YYYY HH:mm">
                            {
                              this.orderStore.order.logs[
                                this.orderStore.order.logs.length - 1
                              ].dateCreated
                            }
                          </Moment>
                        </p>
                      </div>
                    ) : (
                      <b>Pedido não está sincronizado com o magento</b>
                    )}
                    {this.orderStore.order.sync === "error" ? (
                      <div>
                        <p className={"label label-danger"}>
                          Ocorreu um erro na sincronização às{" "}
                          <Moment format="DD/MM/YYYY HH:mm">
                            {
                              this.orderStore.order.logs[
                                this.orderStore.order.logs.length - 1
                              ].dateCreated
                            }
                          </Moment>
                        </p>
                        <p className={s.errorMessage}>
                          <b>Mensagem:</b>{" "}
                          {
                            this.orderStore.order.logs[
                              this.orderStore.order.logs.length - 1
                            ].message
                          }
                        </p>
                      </div>
                    ) : (
                      ""
                    )}
                  </Panel>
                </Col>
              </Row>
              <Row className="show-grid">
                <Col sm={2} md={6} xs={12}>
                  <CustomJumbotron
                    title="Pedido"
                    content={[
                      {
                        label: "Data do pedido",
                        content: (
                          <Moment format="DD/MM/YYYY HH:mm">
                            {this.orderStore.order.dateCreated}
                          </Moment>
                        ),
                      },
                      {
                        label: "Status",
                        content: this.orderStore.order.status.label,
                      },
                      {
                        label: "Método de envio",
                        content: this.orderStore.order.shippingMethod
                          ? this.orderStore.order.shippingMethod
                          : "",
                      },
                      {
                        label: "Valor do Frete para Cliente",
                        content: this.orderStore.order.sellerShippingCost
                          ? formatBrMoney(this.orderStore.order.shippingCost)
                          : "Desconhecido",
                      },
                      {
                        label: "Custo do Frete",
                        content: this.orderStore.order.sellerShippingCost
                          ? formatBrMoney(
                              this.orderStore.order.sellerShippingCost
                            )
                          : "Desconhecido",
                      },
                      {
                        label: "Total do pedido",
                        content: formatBrMoney(this.orderStore.order.total),
                      },
                    ]}
                  />
                </Col>
                <Col md={6} xs={12}>
                  <CustomJumbotron
                    title="Dados do Cliente"
                    content={[
                      {
                        label: "Nome",
                        content:
                          this.orderStore.order.buyer.firstName +
                          " " +
                          this.orderStore.order.buyer.lastName,
                      },
                      {
                        label: "E-mail",
                        content: this.orderStore.order.buyer.email,
                      },
                      {
                        label: "CPF/CNPJ",
                        content: this.orderStore.order.buyer.vatNumber,
                      },
                      /*  {label:'Nascimento', content: 'this.orderStore.order.buyer'},
                                        {label:'Sexo', content:'this.orderStore.order.buyer'}, */
                      {
                        label: "Telefone",
                        content: this.orderStore.order.buyer.phones[0],
                      },
                      {
                        label: "Celular",
                        content: this.orderStore.order.buyer.phones[1],
                      },
                    ]}
                  />
                </Col>
              </Row>
              <Row className="show-grid">
                <Col sm={2} md={6} xs={12}>
                  {this.orderStore.order.billingAddress ? (
                    <CustomJumbotron
                      title="Endereço de Cobrança"
                      content={[
                        {
                          label: "Rua",
                          content: this.orderStore.order.billingAddress.street,
                        },
                        {
                          label: "Número",
                          content: this.orderStore.order.billingAddress.number,
                        },
                        {
                          label: "Bairro",
                          content: this.orderStore.order.billingAddress
                            .neighborhood,
                        },
                        {
                          label: "Cidade/Estado",
                          content:
                            this.orderStore.order.billingAddress.city +
                            "/" +
                            this.orderStore.order.billingAddress.region.name,
                        },
                        {
                          label: "CEP",
                          content: this.orderStore.order.billingAddress
                            .postcode,
                        },
                        {
                          label: "Telefone",
                          content: this.orderStore.order.billingAddress.phone,
                        },
                      ]}
                    />
                  ) : (
                    <CustomJumbotron
                      title="Endereço de Cobrança"
                      content={[]}
                    />
                  )}
                </Col>
                <Col md={6} xs={12}>
                  {this.orderStore.order.shippingAddress ? (
                    <CustomJumbotron
                      title="Endereço de Entrega"
                      content={[
                        {
                          label: "Rua",
                          content: this.orderStore.order.shippingAddress.street,
                        },
                        {
                          label: "Número",
                          content: this.orderStore.order.shippingAddress.number,
                        },
                        {
                          label: "Bairro",
                          content: this.orderStore.order.shippingAddress
                            .neighborhood,
                        },
                        {
                          label: "Cidade/Estado",
                          content:
                            this.orderStore.order.shippingAddress.city +
                            "/" +
                            this.orderStore.order.shippingAddress.region.name,
                        },
                        {
                          label: "CEP",
                          content: this.orderStore.order.shippingAddress
                            .postcode,
                        },
                        {
                          label: "Telefone",
                          content: this.orderStore.order.shippingAddress.phone,
                        },
                      ]}
                    />
                  ) : (
                    <CustomJumbotron title="Endereço de entrega" content={[]} />
                  )}
                </Col>
              </Row>
              <Panel header={"Itens do Pedido"}>
                <OrderItems orderItems={this.orderStore.order.orderItems} />
              </Panel>
              <div className={s.btnGroup}>
                <Button
                  type="button"
                  bsStyle="default"
                  onClick={history.goBack}
                >
                  Voltar
                </Button>
              </div>
            </Grid>
          </div>
        )}
      </div>
    );
  }
}
