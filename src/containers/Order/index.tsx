import * as React from "react";
import { observer } from "mobx-react";
import { observable } from "mobx";
import stores from "../../stores";
import {
  linkToOrderDetails,
  linkToCartDetails,
  history,
} from "../Route/RouteStore";
import Moment from "react-moment";
import PaginationAdvanced from "../../components/PaginationAdvanced";
import { Table } from "react-bootstrap";
import { formatBrMoney } from "../../helpers";
import AsyncFilters from "../../components/AsyncFilters";
import TheadSortable from "../../components/TheadSortable";

const FaSearchPlus = require("react-icons/lib/fa/search-plus");
const Cart = require("react-icons/lib/fa/shopping-cart");
const Spinner = require("react-spinkit");
const s = require("./style.scss");

interface Props {}

@observer
export class Order extends React.Component<Props, any> {
  @observable error: string = "";
  orderStore = stores.orderStore;
  @observable page: number = 1;
  query: string = "";
  action;

  constructor(props: Props, context: any) {
    super(props, context);
  }

  componentWillMount() {
    //this.orderStore.listOrders(this.page,this.query);
  }

  onClick(value) {
    console.log(value);
    this.orderStore.listOrders(value, this.query);
    this.page = value;
  }

  goToOrder(id) {
    history.push(linkToOrderDetails(id));
  }

  goToCart(id) {
    history.push(linkToCartDetails(id));
  }

  sortQuery(field) {
    if (this.query.indexOf("sortBy") > -1) {
      let queryArray = this.query.split("&");
      queryArray.map((e) =>
        e.indexOf("sortBy") > -1 ? (e = `&sortBy=${field}`) : e
      );
      this.query = queryArray.join("&").toString();
    } else {
      this.query = this.query.concat(`&sortBy=${field}`);
    }
    history.push({
      search: this.query,
      pathname: location.pathname,
    });
  }

  runQuery(query) {
    this.query = query;
    clearTimeout(this.action);
    this.action = setTimeout(
      () => this.orderStore.listOrders(this.page, query),
      500
    );
  }

  goOrderDetails(u) {
    u.packId === null || u.packId === ""
      ? this.goToOrder(u._id)
      : this.goToCart(u.packId);
  }

  showCart(u) {
    return u.packId && u.packId != "" ? <Cart /> : "";
  }

  handleOrderStatusSync(sync) {
    const syncStatus = {
      class: "",
      status: "",
    };
    if (sync && sync == "ok") {
      syncStatus.class = "label label-success";
      syncStatus.status = "Enviado";
    } else if (sync == "pending") {
      syncStatus.class = "label label-warning";
      syncStatus.status = "Não enviado";
    } else {
      syncStatus.class = "label label-danger";
      syncStatus.status = "Falhou";
    }

    return syncStatus;
  }

  render() {
    return (
      <div>
        <h1>Pedidos</h1>
        <AsyncFilters
          getQuery={this.runQuery.bind(this)}
          title="Filtros"
          fields={[
            { label: "Código", name: "id", type: "text" },
            { label: "Cliente", name: "customer", type: "text" },
            { label: "Rastreamento", name: "trackingNumber", type: "text" },
            { label: "Preço de", name: "priceFrom", type: "number" },
            { label: "Preço até", name: "priceTo", type: "number" },
            {
              label: "Status",
              name: "status[]",
              type: "multiselect",
              options: [
                { label: "Completo", value: "complete" },
                { label: "Processando", value: "processing" },
              ],
            },
          ]}
        />

        {this.orderStore.inProgress === true ||
        this.orderStore.orders == undefined ||
        !this.orderStore.orders.orders ? (
          <Spinner
            name="line-scale-pulse-out"
            fadeIn="none"
            className={s.spinner}
          />
        ) : (
          <div className="table-responsive">
            <Table className={s.table} responsive hover>
              <TheadSortable
                columns={[
                  { label: "#", name: "packId" },
                  { label: "Código", name: "id" },
                  { label: "ID", name: "externalId" },
                  { label: "Plataforma", name: "platform" },
                  { label: "Data", name: "dateCreated" },
                  { label: "Rastreamento", name: "trackingNumber" },
                  { label: "Sku", name: "sku" },
                  { label: "Cliente", name: "customer" },
                  { label: "Total", name: "total" },
                  { label: "Status", name: "status" },
                  { label: "Sincronização", name: "sync" },
                ]}
                getQuery={this.runQuery.bind(this)}
              />
              {/*  <thead>
                            <tr>
                                <th>Código</th>
                                <th  onClick={()=>this.sortQuery('platform')}><a>Plataforma</a></th>
                                <th>Data</th>
                                <th>Rastreamento</th>
                                <th>Skus</th>
                                <th>Cliente</th>
                                <th>Valor</th>
                                <th>Status</th>
                                 <th>Sincronização</th>
                            </tr>
                            </thead> */}
              <tbody>
                {this.orderStore.orders.orders.map((u) => (
                  <tr key={u._id}>
                    <td>{this.showCart(u)}</td>
                    <td>{u.id}</td>
                    <td>{u.externalId}</td>
                    <td>{u.plataform}</td>
                    <td>
                      <Moment format="DD/MM/YYYY HH:mm">{u.dateCreated}</Moment>
                    </td>
                    <td>{u.tracking_number}</td>
                    <td>
                      {u.orderItems
                        ? u.orderItems.map((skus, i) =>
                            i == 0 ? skus.sku : "," + skus.sku
                          )
                        : ""}
                    </td>
                    <td>{u.buyer.firstName + " " + u.buyer.lastName}</td>
                    <td className={s.tdInline}>{formatBrMoney(u.total)}</td>
                    <td>{u.status ? u.status.label : "?"}</td>
                    <td>
                      <span
                        className={this.handleOrderStatusSync(u.sync).class}
                      >
                        {this.handleOrderStatusSync(u.sync).status}
                      </span>
                    </td>
                    <td onClick={() => this.goOrderDetails(u)}>
                      <FaSearchPlus />
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
            <PaginationAdvanced
              items={this.orderStore.orders.pages}
              maxButtons={5}
              page={this.page}
              onClick={this.onClick.bind(this)}
            />
          </div>
        )}
      </div>
    );
  }
}
