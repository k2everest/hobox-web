import * as React from "react";
import { observer } from "mobx-react";
import stores from "../../../../stores";
import { formatBrMoney } from "../../../../helpers";

const Spinner = require("react-spinkit");
const s = require("./style.scss");

interface Props {
  orderItems: any;
}

@observer
export default class OrderItems extends React.Component<Props, any> {
  orderStore = stores.orderStore;
  constructor(props: Props, context: any) {
    super(props, context);
  }

  render() {
    return (
      <div>
        {this.orderStore.inProgress === true ? (
          <Spinner
            name="line-scale-pulse-out"
            fadeIn="none"
            className={s.spinner}
          />
        ) : (
          <table className="table">
            <thead>
              <tr>
                <th>SKU</th>
                {/*                                 <th className='hidden-mobile'>Variação</th>
                 */}{" "}
                <th>Nome</th>
                <th>Qtd</th>
                <th>Preço</th>
              </tr>
            </thead>
            <tbody>
              {this.props.orderItems.map((u) => (
                <tr key={u.sku}>
                  <td>{u.sku}</td>
                  {/*                                 <td className='hidden-mobile'>{u.item.variation_id ? u.item.variation_id :'Produto simples'}</td>
                   */}{" "}
                  <td>{u.name}</td>
                  <td>{u.qty}</td>
                  <td className={s.tdInline}> {formatBrMoney(u.price)}</td>
                </tr>
              ))}
            </tbody>
          </table>
        )}
      </div>
    );
  }
}
