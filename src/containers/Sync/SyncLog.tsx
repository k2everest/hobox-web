import * as React from "react";
import { observer } from "mobx-react";
import { observable } from "mobx";
import stores from "../../stores";
import Moment from "react-moment";
import PaginationAdvanced from "../../components/PaginationAdvanced";
import { Table } from "react-bootstrap";
import TooltipCustom from "../../components/TooltipCustom";
import SimpleModal from "../../components/Modal/Simple";
import AsyncFilters from "../../components/AsyncFilters";
import TheadSortable from "../../components/TheadSortable";

const s = require("./style.scss");

interface Props {}

@observer
export default class SyncLog extends React.Component<Props, any> {
  @observable error: string = "";
  store = stores.syncStore;
  @observable page: number = 1;
  @observable currentBoxMessage = "";
  @observable openedBox = false;
  @observable titleBox = "";
  query: string = "";
  action;

  constructor(props: Props, context: any) {
    super(props, context);
  }

  onClick(value) {
    this.store.getSyncLogs(value, this.query);
    this.page = value;
  }

  runQuery(query) {
    this.page = 1;
    this.query = query;
    clearTimeout(this.action);
    this.action = setTimeout(
      () => this.store.getSyncLogs(this.page, query),
      500
    );
  }

  render() {
    return (
      <div>
        <h2>Logs de Sincronização</h2>
        <AsyncFilters
          getQuery={this.runQuery.bind(this)}
          title="Filtros"
          fields={[
            {
              label: "Recurso",
              name: "resource[]",
              type: "multiselect",
              options: [
                { label: "Produto", value: "product" },
                { label: "Pedido", value: "order" },
                { label: "Estoque", value: "stock" },
              ],
            },
            {
              label: "Origem",
              name: "origin[]",
              type: "multiselect",
              options: [
                { label: "Mercado livre", value: "meli" },
                { label: "Magento", value: "magento" },
                { label: "Hobox", value: "hobox" },
              ],
            },
            {
              label: "Destino",
              name: "destiny[]",
              type: "multiselect",
              options: [
                { label: "Mercado livre", value: "meli" },
                { label: "Magento", value: "magento" },
                { label: "Hobox", value: "hobox" },
              ],
            },
            { label: "Código", name: "id", type: "text" },
            {
              label: "Status",
              name: "status[]",
              type: "multiselect",
              options: [
                { label: "Sucesso", value: "success" },
                { label: "Falha", value: "fail" },
              ],
            },
          ]}
        />

        {this.store.syncLogs === undefined || this.store.loading == true ? (
          <div className={s.spinner} />
        ) : (
          <div className="table-responsive">
            <Table className="table" responsive hover>
              <TheadSortable
                columns={[
                  { label: "Recurso", name: "resource" },
                  { label: "Origem", name: "origin" },
                  { label: "Destino", name: "destiny" },
                  { label: "Data", name: "dateTime" },
                  { label: "Código", name: "id" },
                  { label: "Status", name: "status" },
                  { label: "Mensagem", name: "message" },
                  { label: "Info", name: "info" },
                ]}
                getQuery={this.runQuery.bind(this)}
              />
              <tbody>
                {this.store.syncLogs.logs.map((u) => (
                  <tr
                    key={u._id}
                    onClick={() => {
                      this.titleBox = "Detalhes de " + u.id;
                      this.currentBoxMessage =
                        u.info && u.info.length > 0
                          ? u.info.map(
                              (c, i) =>
                                i +
                                1 +
                                ". " +
                                c.code +
                                " -> " +
                                c.message +
                                "\n"
                            )
                          : u.message;
                      this.openedBox = true;
                    }}
                  >
                    <td>{u.resource}</td>
                    <td>{u.origin}</td>
                    <td>{u.destiny}</td>
                    <td>
                      <Moment format="DD/MM/YYYY HH:mm" key={u._id}>
                        {u.dateTime}
                      </Moment>
                    </td>
                    <td>{u.id}</td>
                    <td>
                      <span
                        className={
                          u.status == "success"
                            ? "label label-success"
                            : !!u.status
                            ? "label label-danger"
                            : "label label-warning"
                        }
                      >
                        {u.status == "success" ? "Sucesso" : "Falha"}
                      </span>
                    </td>
                    <td>
                      {u.message ? (
                        <div className={s.message}>{u.message}</div>
                      ) : (
                        ""
                      )}
                    </td>
                    <td>
                      {u.info && u.info.length > 0 ? (
                        <TooltipCustom
                          message={u.info.map(
                            (c) => c.code + " -> " + c.message + "  "
                          )}
                        />
                      ) : (
                        ""
                      )}
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
            <PaginationAdvanced
              items={this.store.syncLogs.pages}
              maxButtons={5}
              page={this.page}
              onClick={this.onClick.bind(this)}
            />
          </div>
        )}
        <SimpleModal
          context={this}
          show={this.openedBox}
          title={this.titleBox}
          message={this.currentBoxMessage}
          onClick={() => (this.openedBox = false)}
        />
      </div>
    );
  }
}
