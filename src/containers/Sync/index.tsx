import * as React from "react";
import { observer } from "mobx-react";
import { Route, Switch } from "react-router";
import { observable } from "mobx";
import { history } from "../Route/RouteStore";

//components
import ControlledTabs from "../../components/ControlledTabs";

//containers
import SyncAction from "./SyncAction";
import SyncLog from "./SyncLog";

//style
const s = require("./style.scss");

interface IProps {
  route: Route;
}

interface IState {}

@observer
export default class Sync extends React.Component<IProps, IState> {
  @observable error: string = "";
  constructor(props: IProps, context: any) {
    super(props, context);
  }

  render() {
    return (
      <div className={s.content}>
        {console.log(history.location.pathname)}

        <h1>Sincronizações</h1>
        <ControlledTabs
          activeTab={history.location.pathname}
          tab={[
            { title: "Sincronizar", key: "/sync" },
            { title: "Histórico", key: "/sync-logs" },
          ]}
        />
        <Switch>
          <Route path={"/sync"} render={(props) => <SyncAction {...props} />} />
          <Route
            path={"/sync-logs"}
            render={(props) => <SyncLog {...props} />}
          />
        </Switch>
      </div>
    );
  }
}
