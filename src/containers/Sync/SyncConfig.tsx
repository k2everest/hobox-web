import * as React from "react";
import CrossTable from "../../components/CrossTable";
import ToogleSwitch from "../../components/ToogleSwitch";
import { observer } from "mobx-react";
import { observable } from "mobx";
import { syncStore } from "../../stores/rootStore";

interface Props {}

const s = require("./style.scss");

const CrossTableController = ({ config, changeConfig }) => {
  if (!config.syncConfig) return null;
  else
    return (
      <CrossTable
        collumns={[
          { label: "Receber", name: "receive" },
          { label: "Enviar", name: "send" },
        ]}
        title={config.platform}
        lines={[
          {
            label: "Pedidos",
            name: "order",
            value: [
              <ToogleSwitch
                statusAlt={{
                  activated: "Habilitado",
                  disabled: "desabilitado",
                }}
                value={config.syncConfig.order.receive.value}
                onChange={(e) =>
                  changeConfig(
                    (config.syncConfig.order.receive.value = e),
                    config
                  )
                }
                active={config.syncConfig.order.receive.changeable}
              />,
              <ToogleSwitch
                statusAlt={{
                  activated: "Habilitado",
                  disabled: "desabilitado",
                }}
                value={config.syncConfig.order.send.value}
                onChange={(e) =>
                  changeConfig((config.syncConfig.order.send.value = e), config)
                }
                active={config.syncConfig.order.send.changeable}
              />,
            ],
          },
          {
            label: "Produtos",
            name: "product",
            value: [
              <ToogleSwitch
                statusAlt={{
                  activated: "Habilitado",
                  disabled: "desabilitado",
                }}
                value={config.syncConfig.product.receive.value}
                onChange={(e) =>
                  changeConfig(
                    (config.syncConfig.product.receive.value = e),
                    config
                  )
                }
                active={config.syncConfig.product.receive.changeable}
              />,
              <ToogleSwitch
                statusAlt={{
                  activated: "Habilitado",
                  disabled: "desabilitado",
                }}
                value={config.syncConfig.product.send.value}
                onChange={(e) =>
                  changeConfig(
                    (config.syncConfig.product.send.value = e),
                    config
                  )
                }
                active={config.syncConfig.product.send.changeable}
              />,
            ],
          },
          {
            label: "Estoque",
            name: "stock",
            value: [
              <ToogleSwitch
                statusAlt={{
                  activated: "Habilitado",
                  disabled: "desabilitado",
                }}
                value={config.syncConfig.stock.receive.value}
                onChange={(e) =>
                  changeConfig(
                    (config.syncConfig.stock.receive.value = e),
                    config
                  )
                }
                active={config.syncConfig.stock.receive.changeable}
              />,
              <ToogleSwitch
                statusAlt={{
                  activated: "Habilitado",
                  disabled: "desabilitado",
                }}
                value={config.syncConfig.stock.send.value}
                onChange={(e) =>
                  changeConfig((config.syncConfig.stock.send.value = e), config)
                }
                active={config.syncConfig.stock.send.changeable}
              />,
            ],
          },
        ]}
      />
    );
};

@observer
export default class SyncConfig extends React.Component<Props, any> {
  @observable syncStore = syncStore;

  componentWillMount() {
    this.syncStore.getSyncConfig();
  }

  changeConfig(e, config) {
    e = e;
    syncStore.setSyncConfig(config._id, config.syncConfig);
  }

  render() {
    return (
      <div>
        {this.syncStore.syncConfigPlatforms.length > 0 ? (
          <div className={s.containerConfig}>
            {this.syncStore.syncConfigPlatforms.map((syncConfig, index) => (
              <CrossTableController
                key={index}
                config={syncConfig}
                changeConfig={this.changeConfig}
              />
            ))}
          </div>
        ) : (
          ""
        )}
      </div>
    );
  }
}
