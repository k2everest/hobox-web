import * as React from "react";
import { observer } from "mobx-react";
import { observable } from "mobx";
import { syncStore } from "../../stores/rootStore";
import CheckboxRadioButton from "../../components/CheckboxRadioButton";
const s = require("./style.scss");
import { Button, Modal } from "react-bootstrap";
import ToogleSwitch from "../../components/ToogleSwitch";
import SyncConfig from "./SyncConfig";
import TooltipCustom from "../../components/TooltipCustom";

interface Props {}

@observer
export default class SyncAction extends React.Component<Props, any> {
  @observable checkboxValue: string[] = [];
  @observable radioValue: string;
  @observable isFullSync: boolean = false;
  syncStore = syncStore;

  componentWillMount() {
    this.syncStore.getSyncConfig();
  }

  constructor(props: Props, context: any) {
    super(props, context);
  }

  requestSync() {
    this.syncStore.sync(this.checkboxValue, this.radioValue, this.isFullSync);
    console.log(this.checkboxValue, this.radioValue);
  }

  close() {
    this.syncStore.unsetDone();
  }

  isDisabled(platform, context): boolean {
    let result = context.syncStore.syncConfigPlatforms.reduce(
      (acum, current) => {
        if (current.platform == platform) {
          console.log("in", current.platform);
          acum = false;
        }
        return acum;
      },
      true
    );

    return result;
  }

  render() {
    return (
      <div>
        <h2>
          Executar Sincronização
          <div className={s.isFullSyncField}>
            <label>Sincronização Completa</label>
            <TooltipCustom
              message={
                "Ative para obter uma sincronização completa, ou seja, independente das atualizações dos dados. Desative para sincronizar apenas os dados que foram atualizados desde a última sincronização  "
              }
            />
            <div className={s.customField}>
              <ToogleSwitch
                statusAlt={{
                  activated: "Habilitado",
                  disabled: "desabilitado",
                }}
                value={this.isFullSync}
                onChange={(e) => (this.isFullSync = e)}
                active={true}
              />
            </div>
          </div>
        </h2>

        <div className={s.container}>
          {this.syncStore.syncConfigPlatforms.length > 0 ||
          this.syncStore.inProgress === false ? (
            <form>
              <div className={s.field}>
                <CheckboxRadioButton
                  label={"Quais plataformas deseja sincronizar?"}
                  options={[
                    {
                      label: "Magento",
                      value: "magento",
                      disabled: this.isDisabled("magento", this),
                    },
                    {
                      label: "Mercado Livre",
                      value: "mercadolivre",
                      disabled: this.isDisabled("mercadolivre", this),
                    },
                    {
                      label: "Bling",
                      value: "bling",
                      disabled: this.isDisabled("bling", this),
                    },
                  ]}
                  type={"checkbox"}
                  name={"plataformas"}
                  value={this.checkboxValue.slice()}
                  onChanges={(value) => (this.checkboxValue = value)}
                />
              </div>
              <div className={s.field}>
                <CheckboxRadioButton
                  label={"O que deseja sincronizar?"}
                  options={[
                    { label: "Produtos", value: "product" },
                    { label: "Estoque", value: "stock" },
                    { label: "Pedidos", value: "order" },
                  ]}
                  type={"radio"}
                  name={"artefato"}
                  value={this.radioValue}
                  onChanges={(value) => (this.radioValue = value)}
                />
              </div>

              <Button
                type="button"
                bsStyle="primary"
                onClick={this.requestSync.bind(this)}
              >
                Sincronizar
              </Button>
            </form>
          ) : (
            "waiting"
          )}
        </div>

        <SyncConfig />

        {this.syncStore.inProgress === true ? (
          <Modal show={true} container={this} onHide={() => {}}>
            <div className={s.spinner} />
          </Modal>
        ) : null}

        <Modal show={this.syncStore.done} container={this} onHide={() => {}}>
          <Modal.Body>
            {this.syncStore.errors
              ? this.syncStore.errors
              : "A Sincronização foi iniciada, dentro de alguns instantes o processo estará concluído."}
          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle="primary" onClick={this.close.bind(this)}>
              OK
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
