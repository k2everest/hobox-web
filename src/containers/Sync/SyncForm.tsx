import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Button } from "react-bootstrap";
/**
 ** Style
 */

const s = require("./style.scss");

export interface LoginFormFields {
  user: string;
  password: string;
}

interface ILoginFormProps {
  onSubmit: (fields: LoginFormFields) => any;
}

interface ILoginFormState {}

@observer
export default class LoginForm extends React.Component<
  ILoginFormProps,
  ILoginFormState
> {
  @observable user: string = "";
  @observable password: string = "";

  onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    this.props.onSubmit({
      user: this.user,
      password: this.password,
    });
  };
  handleChange(e) {
    this.user = e.target.value;
  }

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit} className={s.form}>
          {/*                     <FormControl type="email" value={this.user}  placeholder="e-mail" onChange={this.handleChange.bind(this)} />
           */}
          <div className={s.field}>
            <input
              type="text"
              placeholder="E-mail"
              value={this.user}
              onChange={(e) => (this.user = e.target.value)}
            />
          </div>

          <div className={s.field}>
            <input
              type="password"
              placeholder="Senha"
              value={this.password}
              onChange={(e) => (this.password = e.target.value)}
            />
          </div>
          <Button type="submit" bsStyle="primary">
            Entrar
          </Button>

          {/*    <Button  submit={true} title="Entrar"/> */}
        </form>
      </div>
    );
  }
}
