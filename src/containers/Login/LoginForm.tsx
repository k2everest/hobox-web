import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { GoogleLogin } from "react-google-login";
import { Config } from "../App/Config";
import stores from "../../stores/";
const GoogleIcon = require("react-icons/lib/fa/google");

/** Style*/
const s = require("./style.scss");

export interface LoginFormFields {
  user: string;
  password: string;
}

interface ILoginFormProps {
  onSubmit: (fields: LoginFormFields) => any;
}

interface ILoginFormState {}

@observer
export default class LoginForm extends React.Component<
  ILoginFormProps,
  ILoginFormState
> {
  @observable user: string = "";
  @observable password: string = "";

  componentWillUnmount() {
    stores.authStore.errors = undefined;
  }

  onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    this.props.onSubmit({
      user: this.user,
      password: this.password,
    });
  };

  handleChange(e) {
    this.user = e.target.value;
  }

  onFailure = (obj) => {
    stores.authStore.errors = obj.error;
  };

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit.bind(this)} className={s.form}>
          <div className={s.field}>
            <input
              type="text"
              placeholder="E-mail"
              value={this.user}
              onChange={(e) => (this.user = e.target.value)}
            />
          </div>

          <div className={s.field}>
            <input
              type="password"
              placeholder="Senha"
              value={this.password}
              onChange={(e) => (this.password = e.target.value)}
            />
          </div>
          <Button type="submit" bsStyle="primary">
            Entrar
          </Button>
          <div className={s.links}>
            <Link to={`/user/forgot-password`}>Esqueci minha senha</Link>
            <Link to={`/signup`}>Inscreva-se</Link>
          </div>
        </form>
        <div className={s.divider}>
          <hr />
          <span>ou</span>
        </div>
        <div className={s.socialButton}>
          <GoogleLogin
            clientId={Config.GOOGLE_CLIENT_ID}
            onSuccess={stores.authStore.googleResponse}
            onFailure={this.onFailure}
          >
            <GoogleIcon />
            <span>Entre com Google</span>
          </GoogleLogin>
        </div>
      </div>
    );
  }
}
