import * as React from "react";
import { observer } from "mobx-react";
import { observable } from "mobx";
import stores from "../../stores/";
import { Redirect } from "react-router";
import { Modal } from "react-bootstrap";

/**
 * Components
 */
import LoginForm, { LoginFormFields } from "./LoginForm";

/* Style */
const s = require("./style.scss");

interface ILoginProps {}

interface ILoginState {}

@observer
export default class Login extends React.Component<ILoginProps, ILoginState> {
  @observable error: string = "";
  authStore = stores.authStore;
  commonStore = stores.commonStore;

  componentWillMount() {
    this.authStore.logged = false;
  }

  onSubmit = (fields: LoginFormFields) => {
    this.authStore.setEmail(fields.user);
    this.authStore.setPassword(fields.password);
    this.authStore.login();
  };

  render() {
    return (
      <div>
        {this.authStore.inProgress === true ? (
          <Modal show={true} container={this} onHide={() => {}}>
            <div className={s.spinner} />
          </Modal>
        ) : null}

        <div className="col-md-4 col-md-offset-4">
          <div className={s.form}>
            <h1>Login</h1>

            {this.commonStore.logged ? (
              <Redirect to={"/"} />
            ) : (
              <LoginForm onSubmit={this.onSubmit} />
            )}
            {this.authStore.errors === undefined ? null : (
              <div className={s.error}> Usuário ou senha incorretos</div>
            )}
          </div>
        </div>
      </div>
    );
  }
}
