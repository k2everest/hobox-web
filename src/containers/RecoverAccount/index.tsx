import * as React from "react";
import { observer } from "mobx-react";
import { observable } from "mobx";
import stores from "../../stores/";

import { Modal, Button, Label } from "react-bootstrap";
const s = require("./style.scss");

interface Props {}

@observer
export default class RecoverAccount extends React.Component<Props, {}> {
  @observable error: string = "";
  userStore = stores.userStore;

  onSubmit = () => {
    if (this.userStore.userPassword.currentPassword)
      this.userStore.recoverAccount();
    else {
      this.userStore.errors = "Informe a senha para continuar";
      this.userStore._setDone();
    }
  };

  render() {
    return (
      <Modal
        bsSize="large"
        aria-labelledby="contained-modal-title-lg"
        onHide={() => {}}
        show={true}
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-lg">
            Reativação de conta Hobox
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h4>Reative sua conta</h4>
          <p>
            Forneça sua senha de acesso e clique no botão para reativar sua
            conta Hobox.
          </p>
          <div className={s.field}>
            <label>Senha:</label>
            <input
              type="password"
              value={this.userStore.userPassword.currentPassword}
              onChange={(e) =>
                (this.userStore.userPassword.currentPassword = e.target.value)
              }
            />
          </div>
          {this.userStore.errors ? (
            <Label bsStyle="danger">{this.userStore.errors}</Label>
          ) : null}

          <div className={s.buttons}>
            <Button onClick={this.onSubmit} bsStyle="primary">
              Reativar Conta
            </Button>
            <Button onClick={stores.authStore.logout} bsStyle="Secondary">
              Sair
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    );
  }
}
