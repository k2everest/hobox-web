import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import stores from "../../../stores";
import Spinner from "../../../components/Spinner";
import SimpleModal from "../../../components/Modal/Simple";
import Form from "../../../components/Form";

//const s = require('./style.scss');

interface IState {}

interface IProps {}

@observer
export default class AccountEdit extends React.PureComponent<IProps, IState> {
  @observable userStore = stores.userStore;
  userId = undefined;
  @observable modalSimple = false;

  componentWillMount() {
    this.userId = this.props;
    this.userStore.pullUser();
    this.userId = this.userId.match.params.userId;
    if (this.userId != "new" && this.userId != undefined) {
      this.userStore.pullUser();
    }
  }

  close() {
    this.modalSimple = false;
  }

  onSubmit = (e: boolean) => {
    this.modalSimple = true;
    console.log(e);
    this.userStore.updateUser(this.userStore.currentUser);
  };

  render() {
    console.log("store", this.userStore.currentUser);

    return (
      <div>
        <h2>Dados da conta</h2>

        <SimpleModal
          context={this}
          show={this.userStore.done && this.modalSimple}
          message={
            this.userStore.errors
              ? this.userStore.errors
              : "Dados salvos com sucesso"
          }
          onClick={this.close.bind(this)}
        />

        {this.userStore.inProgress ||
        this.userStore.currentUser == undefined ? (
          <Spinner />
        ) : (
          <Form
            onSubmit={this.onSubmit}
            fields={[
              {
                label: "Nome",
                type: "text",
                defaultValue: this.userStore.currentUser.name,
                onChange: (value) => (this.userStore.currentUser.name = value),
              },
              {
                label: "Sobrenome",
                type: "text",
                defaultValue: this.userStore.currentUser.lastname,
                onChange: (value) =>
                  (this.userStore.currentUser.lastname = value),
              },
              {
                label: "E-mail",
                type: "text",
                defaultValue: this.userStore.currentUser.email,
                onChange: (value) => (this.userStore.currentUser.email = value),
                disabled: true,
              },
            ]}
          />
        )}
      </div>
    );
  }
}
