import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import stores from "../../../stores";
import Spinner from "../../../components/Spinner";
import SimpleModal from "../../../components/Modal/Simple";
import Form from "../../../components/Form";

interface IState {}

interface IProps {}

@observer
export default class PasswordEdit extends React.PureComponent<IProps, IState> {
  @observable userStore = stores.userStore;
  @observable modalSimple = false;

  close() {
    this.modalSimple = false;
    this.userStore.errors = undefined;
  }

  onSubmit = (e: boolean) => {
    this.modalSimple = true;
    console.log(e);
    if (
      this.userStore.userPassword.confirmPassword ==
      this.userStore.userPassword.newPassword
    )
      this.userStore.updatePasswordUser();
    else {
      this.userStore.errors = "Nova Senha diverge da Senha de Confirmação";
      this.userStore._setDone();
    }
  };

  componentWillUnmount() {
    this.userStore.clearUserPassword();
  }

  render() {
    return (
      <div>
        <h2>Alterar Senha</h2>

        <SimpleModal
          context={this}
          show={this.userStore.done && this.modalSimple}
          message={
            this.userStore.errors
              ? this.userStore.errors
              : "Senha alterada com sucesso"
          }
          onClick={this.close.bind(this)}
        />

        {this.userStore.inProgress ||
        this.userStore.userPassword == undefined ? (
          <Spinner />
        ) : (
          <Form
            onSubmit={this.onSubmit}
            fields={[
              {
                label: "Senha Atual",
                type: "password",
                defaultValue: this.userStore.userPassword.currentPassword,
                onChange: (value) =>
                  (this.userStore.userPassword.currentPassword = value),
              },
              {
                label: "Nova Senha",
                type: "password",
                defaultValue: this.userStore.userPassword.newPassword,
                onChange: (value) =>
                  (this.userStore.userPassword.newPassword = value),
              },
              {
                label: "Confirmar Nova Senha",
                type: "password",
                defaultValue: this.userStore.userPassword.confirmPassword,
                onChange: (value) =>
                  (this.userStore.userPassword.confirmPassword = value),
              },
            ]}
          />
        )}
      </div>
    );
  }
}
