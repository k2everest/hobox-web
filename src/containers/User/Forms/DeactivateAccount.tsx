import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import stores from "../../../stores";
import Spinner from "../../../components/Spinner";
import SimpleModal from "../../../components/Modal/Simple";
import Form from "../../../components/Form";

interface IState {}

interface IProps {}

@observer
export default class DeactivateAccount extends React.PureComponent<
  IProps,
  IState
> {
  @observable userStore = stores.userStore;
  @observable modalSimple = false;

  close() {
    this.modalSimple = false;
    this.userStore.errors = undefined;
    window.location.href = "/";
  }

  onSubmit = (e: boolean) => {
    console.log("e", e);
    this.modalSimple = true;
    if (this.userStore.userPassword.currentPassword)
      this.userStore.deactivateAccount();
    else {
      this.userStore.errors = "Informe a senha para continuar";
      this.userStore._setDone();
    }
  };

  componentWillUnmount() {
    this.userStore.clearUserPassword();
  }

  render() {
    return (
      <div>
        <h2>Desativar Conta</h2>

        <SimpleModal
          context={this}
          show={this.userStore.done && this.modalSimple}
          message={
            this.userStore.errors
              ? this.userStore.errors
              : "Sua conta foi desativada com sucesso.\n Dentro do prazo de 48 horas ainda é possível reativar sua conta, após esse período todos os dados serão apagados permanentemente."
          }
          onClick={this.close.bind(this)}
        />

        {this.userStore.inProgress ||
        this.userStore.userPassword == undefined ? (
          <Spinner />
        ) : (
          <Form
            onSubmit={this.onSubmit}
            fields={[
              {
                label: "Senha Atual",
                type: "password",
                defaultValue: this.userStore.userPassword.currentPassword,
                onChange: (value) =>
                  (this.userStore.userPassword.currentPassword = value),
              },
            ]}
          />
        )}
      </div>
    );
  }
}
