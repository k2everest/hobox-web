import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import stores from "../../../stores";
import Spinner from "../../../components/Spinner";
import SimpleModal from "../../../components/Modal/Simple";
import Form from "../../../components/Form";

//const s = require('./style.scss');

interface IState {}

interface IProps {}

@observer
export default class CompanyEdit extends React.PureComponent<IProps, IState> {
  @observable userStore = stores.userStore;
  userId = undefined;
  @observable modalSimple = false;

  close() {
    this.modalSimple = false;
  }

  componentWillMount() {
    this.userStore.getCompanyUser();
  }

  onSubmit = (e: boolean) => {
    this.modalSimple = true;
    console.log(e);
    this.userStore.updateCompanyUser();
  };

  render() {
    console.log("store", this.userStore.userCompany);

    return (
      <div>
        <h2>Dados da empresa</h2>

        <SimpleModal
          context={this}
          show={this.userStore.done && this.modalSimple}
          message={
            this.userStore.errors
              ? this.userStore.errors
              : "Dados salvos com sucesso"
          }
          onClick={this.close.bind(this)}
        />

        {this.userStore.inProgress || this.userStore.getCompanyUser === null ? (
          <Spinner />
        ) : (
          <Form
            onSubmit={this.onSubmit}
            fields={[
              {
                label: "Nome",
                type: "text",
                defaultValue: this.userStore.userCompany.name,
                onChange: (value) => (this.userStore.userCompany.name = value),
              },
              {
                label: "Razão social",
                type: "text",
                defaultValue: this.userStore.userCompany.documentName,
                onChange: (value) =>
                  (this.userStore.userCompany.documentName = value),
              },
              {
                label: "CNPJ",
                type: "text",
                defaultValue: this.userStore.userCompany.documentNumber,
                onChange: (value) =>
                  (this.userStore.userCompany.documentNumber = value),
              },
              {
                label: "Celular/Telefone",
                type: "text",
                defaultValue: this.userStore.userCompany.phone,
                onChange: (value) => (this.userStore.userCompany.phone = value),
              },
              {
                label: "Site",
                type: "text",
                defaultValue: this.userStore.userCompany.website,
                onChange: (value) =>
                  (this.userStore.userCompany.website = value),
              },
              {
                label: "Endereço",
                type: "text",
                defaultValue: this.userStore.userCompany.address.street,
                onChange: (value) =>
                  (this.userStore.userCompany.address.street = value),
              },
              {
                label: "CEP",
                type: "text",
                defaultValue: this.userStore.userCompany.address.zip,
                onChange: (value) =>
                  (this.userStore.userCompany.address.zip = value),
              },
              {
                label: "Cidade",
                type: "text",
                defaultValue: this.userStore.userCompany.address.city,
                onChange: (value) =>
                  (this.userStore.userCompany.address.city = value),
              },
              {
                label: "Estado",
                type: "text",
                defaultValue: this.userStore.userCompany.address.state,
                onChange: (value) =>
                  (this.userStore.userCompany.address.state = value),
              },
            ]}
          />
        )}
      </div>
    );
  }
}
