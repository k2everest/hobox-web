import * as React from "react";
const EBANX = window.EBANX;

interface Props {}

export class PaymentForm extends React.Component<Props, any> {
  form = () => {
    return (
      <form>
        <input type="text" id="card-number"></input>
        <input type="text" id="card-name"></input>
        <input type="text" id="card-due-date"></input>
        <input type="text" id="card-cvv"></input>
        <button onClick={this.submitForm} />
      </form>
    );
  };

  createTokenCallback = function (ebanxResponse) {
    if (ebanxResponse.data.hasOwnProperty("status")) {
      console.log(ebanxResponse);
      document.getElementById("status").textContent =
        "Success, the token is: " + ebanxResponse.data.token;
    } else {
      console.log(ebanxResponse);
      var errorMessage =
        ebanxResponse.error.err.status_message ||
        ebanxResponse.error.err.message;
      document.getElementById("status").textContent = "Error " + errorMessage;
    }
  };

  submitForm = () => {
    EBANX.card.createToken(
      {
        card_number: "1111",
        card_name: "Rogeres Nascimento",
        card_due_date: "22/2020",
        card_cvv: "122",
      },
      this.createTokenCallback
    );
  };

  componentWillMount() {
    EBANX.config.setMode("test");
    EBANX.config.setPublishableKey(
      "Lf7NeFyk8HS9XIwEgtu_zMemDWyls3FgmBMBsWusx6k"
    );
    EBANX.config.setCountry("br");
  }

  render() {
    return this.form();
  }
}
