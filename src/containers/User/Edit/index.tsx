import * as React from "react";
import { observer } from "mobx-react";
import { Route, Switch } from "react-router";
import { observable } from "mobx";
import { history } from "../../Route/RouteStore";

//components
import ControlledTabs from "../../../components/ControlledTabs";
import AccountEdit from "../Forms/AccountEdit";
import PasswordEdit from "../Forms/PasswordEdit";
import DeactivateAccount from "../Forms/DeactivateAccount";
import CompanyEdit from "../Forms/CompanyEdit";
import { PaymentView } from "../PaymentView";

//style
const s = require("../style.scss");

interface IProps {
  route: Route;
}

interface IState {}

@observer
export default class UserEdit extends React.Component<IProps, IState> {
  @observable error: string = "";
  constructor(props: IProps, context: any) {
    super(props, context);
  }

  render() {
    return (
      <div className={s.content}>
        <h1>Gerenciar conta</h1>
        <ControlledTabs
          activeTab={history.location.pathname}
          tab={[
            { title: "Dados", key: "/users/me" },
            { title: "Dados da Empresa", key: "/users/me/company" },
            { title: "Senha", key: "/users/password" },
            { title: "Desativar Conta", key: "/users/deactivate" },
            { title: "Assinatura/Pagamento", key: "/users/payment" },
          ]}
        />
        <Switch>
          <Route
            path={"/users/me/company"}
            render={(props) => <CompanyEdit {...props} />}
          />
          <Route
            path={"/users/me"}
            render={(props) => <AccountEdit {...props} />}
          />
          <Route
            path={"/users/password"}
            render={(props) => <PasswordEdit {...props} />}
          />
          <Route
            path={"/users/deactivate"}
            render={(props) => <DeactivateAccount {...props} />}
          />
          <Route
            path={"/users/payment"}
            render={(props) => <PaymentView {...props} />}
          />
        </Switch>
      </div>
    );
  }
}
