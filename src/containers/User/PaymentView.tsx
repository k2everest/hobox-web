import * as React from "react";
import { observer } from "mobx-react";
import { Jumbotron, Grid, Table, Button } from "react-bootstrap";
import { observable } from "mobx";
import { paymentStore, userStore } from "../../stores/rootStore";
import Moment from "react-moment";
import { formatBrMoney } from "../../helpers";
const Spinner = require("react-spinkit");
const s = require("./style.scss");
interface Props {}

const isPlanActive = (plan) =>
  plan && plan.status === "active" ? "Habilitado" : "Desativado";
const cancelVisibility = (plan) =>
  plan && plan.status == "active" && plan.canceled == false
    ? "visible"
    : "invisible";
const shouldSubscribeOrPay = (plan) => {
  return plan && plan.pending && plan.canceled
    ? "Pagar Pendência"
    : plan && !plan.pending && plan.canceled
    ? "Assinar"
    : "Pagar";
};
const infoPlan = (plan) => {
  if (plan.canceled)
    return "Sua assinatura encontra-se cancelada e com data de expiração em:";
  if (plan.pending)
    return "Reguralize o pagamento do seu plano com data de expiração:";
  else return "Seu Próximo Pagamento:";
};

@observer
export class PaymentView extends React.Component<Props, any> {
  @observable error: string = "";
  @observable modalSimple = false;

  constructor(props: Props, context: any) {
    super(props, context);
  }

  componentDidMount() {
    paymentStore.get();
    userStore.pullUser();
  }

  render() {
    return (
      <Grid fluid={true}>
        <h1>Assinatura</h1>

        {userStore.currentUser && userStore.currentUser.plan ? (
          <Jumbotron>
            <label>Seu Plano encontra-se:</label>
            <h1>{isPlanActive(userStore.currentUser.plan)}</h1>
            <p>Premium por R$100,00/mês</p>
            <div className="info">
              <label>{infoPlan(userStore.currentUser.plan)}</label>
              <p className="lead">
                <Moment format="DD/MM/YYYY">
                  {userStore.currentUser.plan.dueDate}
                </Moment>
                {}
              </p>
            </div>
            <Button
              onClick={() => paymentStore.subscribe()}
              className="btn btn-primary"
            >
              {shouldSubscribeOrPay(userStore.currentUser.plan)}
            </Button>
            <Button
              onClick={() => paymentStore.unsubscribe()}
              className={`${cancelVisibility(
                userStore.currentUser.plan
              )} btn btn-link`}
            >
              Cancelar Assinatura
            </Button>
          </Jumbotron>
        ) : (
          "carregando"
        )}
        {userStore.currentUser && userStore.currentUser.plan == undefined ? (
          <Jumbotron>
            <label>Seu Plano encontra-se:</label>

            <h1>Desabilitado</h1>
            <p>Assine o Premium por R$100,00/mês com o 1º mês grátis</p>
            <Button
              onClick={() => paymentStore.subscribe()}
              className="btn btn-primary"
            >
              Assinar
            </Button>
          </Jumbotron>
        ) : (
          "carregando"
        )}
        {/* <div className={s.payment}>
                    <Link to={'/'} className="btn btn-primary">
                        Efetuar Pagamento
                    </Link>
                </div> */}

        {userStore.inProgress === true ? (
          <Spinner
            name="line-scale-pulse-out"
            fadeIn="none"
            className={s.spinner}
          />
        ) : (
          <div className="table-responsive">
            <Table className="table" hover>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Descrição</th>
                  <th>Período</th>
                  <th>Forma de Pagamento</th>
                  <th>Valor Total</th>
                  <th>Data Pagamento</th>
                  <th>Data Vencimento</th>
                  <th>Situação</th>
                </tr>
              </thead>
              <tbody>
                {paymentStore.paymentList.length > 0 &&
                  paymentStore.paymentList.map((u) => (
                    <tr key={u._id}>
                      <td>{u.orderIncrementId}</td>
                      <td>{u.description}</td>
                      <td>{}</td>
                      <td>{u.paymentType}</td>
                      <td>{formatBrMoney(u.total)}</td>
                      <td>
                        <Moment format="DD/MM/YYYY">{u.confirmDate}</Moment>
                      </td>
                      <td>
                        <Moment format="DD/MM/YYYY">{u.dueDate}</Moment>
                      </td>
                      <td>{u.status}</td>
                      {/* <td>
                                    <Link to={'/'} className="btn btn-primary">

                                    </Link>
                                </td> */}
                    </tr>
                  ))}
              </tbody>
            </Table>
          </div>
        )}
      </Grid>
    );
  }
}
