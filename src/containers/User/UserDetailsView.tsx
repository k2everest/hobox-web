import * as React from "react";
import { observer } from "mobx-react";
import { userStore } from "../../stores/rootStore";
import { observable } from "mobx";

interface Props {
  match: { params: Params };
}

interface Params {
  userId: number;
}

@observer
export class UsersDetailsView extends React.Component<Props, any> {
  @observable userStore = userStore;

  constructor(props: Props, context: any) {
    super(props, context);
  }

  componentWillMount(props) {
    this.userStore.getInfo(props.match.params.userId);
  }

  render() {
    if (this.userStore.user) {
      return (
        <div>
          <h1>{this.userStore.user.name}</h1>

          <table className="table">
            <thead>
              <tr>
                <th>Operações</th>
              </tr>
            </thead>
            <tbody>
              {/*  {this.userStore..map(t =>
                        <tr key={t.id}>
                            <td>{t.description}</td>
                        </tr>
                    )} */}
            </tbody>
          </table>
        </div>
      );
    } else {
      return null;
    }
  }
}
