import * as React from "react";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";
import { observable } from "mobx";
//import {linkToUserDetails} from '../Route/RouteStore';
import { userStore } from "../../stores/rootStore";
const Spinner = require("react-spinkit");
const s = require("./style.scss");

interface Props {}

@observer
export class UsersView extends React.Component<Props, any> {
  @observable error: string = "";
  userStore = userStore;

  constructor(props: Props, context: any) {
    super(props, context);
  }

  componentWillMount() {
    this.userStore.listUsers();
  }

  render() {
    return (
      <div>
        <h1>Usuários</h1>
        {this.userStore.inProgress === true ? (
          <Spinner
            name="line-scale-pulse-out"
            fadeIn="none"
            className={s.spinner}
          />
        ) : (
          <table className="table">
            <thead>
              <tr>
                <th>Nome</th>
                <th>E-mail</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {this.userStore.users.map((u) => (
                <tr key={u._id}>
                  <td>{u.name}</td>
                  <td>{u.email}</td>
                  <td>
                    <Link to={"/"} className="btn btn-primary"></Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        )}
      </div>
    );
  }
}
