export class Config {
  //static BASE_URL = process.env.NODE_ENV === 'development' ? 'http://localhost:3000' : 'https://www.hobox.com.br:8080';
  static BASE_URL = process.env.API_URL;
  static GOOGLE_CLIENT_ID = process.env.GOOGLE_CLIENT_ID;
}
