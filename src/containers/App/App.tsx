import * as React from "react";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { Provider } from "mobx-react";
import { Menu } from "../Route/Menu";
import Login from "../Login";
import UserMenu from "../../components/UserMenu";
import { RouteStore, history } from "../Route/RouteStore";
import "../../styles/styles.scss";
import { diContext } from "./DIContext";
import stores from "../../stores/rootStore";
import ForgotPassword from "../ForgotPassword";
import RecoverAccount from "../RecoverAccount";
import ReactGA from "react-ga";
import { loadReCaptcha } from "react-recaptcha-google";
import Signup from "../Signup";

//style
require("../../styles/bootstrap.css");

const check = <Redirect to={"/login"} />;

export class App extends React.Component<any, any> {
  constructor(props: any, context: any) {
    super(props, context);
    diContext.stores = { ...stores };
    diContext.routeStore = new RouteStore();
    ReactGA.initialize("UA-125642712-1");
    diContext.complete = { ...diContext, ...diContext.stores };
    console.log(diContext.complete);
  }
  componentDidMount() {
    loadReCaptcha();
  }

  render() {
    return (
      <Provider {...diContext.complete}>
        <Router history={history}>
          <OutContainer>
            <Switch>
              <Route exact path="/login" component={Login} />
              <Route exact path="/signup" component={Signup} />
              <Route
                exact
                path="/user/forgot-password"
                component={ForgotPassword}
              />
              <Route
                exact
                path="/user/reset-password"
                component={ForgotPassword}
              />
              <Container children={check}>
                {diContext.routeStore.routes.map((r) => (
                  <Route
                    key={r.mapping}
                    exact
                    path={r.mapping}
                    component={r.component}
                  />
                ))}
                {/* <Route  component={NotFound}/>    */}
              </Container>
            </Switch>
          </OutContainer>
        </Router>
      </Provider>
    );
  }
}

export const Container = (props: { children?: any }) => (
  <div className="container-wrapped">
    {!stores.commonStore.logged || !window.localStorage.getItem("jwt") ? (
      <Redirect to={"/login"} />
    ) : null}
    {stores.authStore.recoverMode == true ||
    window.localStorage.getItem("recoverMode") ? (
      <RecoverAccount />
    ) : null}
    <div className="col-md-2 d-none d-md-block bg-light sidebar">
      <Menu />
    </div>

    <div className="col-xs-12 col-sm-10">{props.children}</div>
  </div>
);

export const OutContainer = (props: { children?: any }) => (
  <div className="page-container">
    {ReactGA.pageview(window.location.pathname + window.location.search)}
    <div className="navbar navbar-default navbar-fixed-top">
      <div className="navbar-header">
        <a className="navbar-brand" href="/">
          HOBOX
        </a>
        <UserMenu />
      </div>
    </div>
    <div className="container-fluid">
      <div className="row">{props.children}</div>
    </div>
  </div>
);

export const NotFound = () => <div>Page not found</div>;
