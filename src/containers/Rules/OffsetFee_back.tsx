import * as React from "react";
import { observer } from "mobx-react";
//import {linkToProductDetails, history} from '../Route/RouteStore';
import ToogleSwitch from "../../components/ToogleSwitch";
import stores from "../../stores";
const s = require("./style.scss");
const Icon = require("react-icons/lib/fa/cog");

interface IProps {}

@observer
export class OffsetFee extends React.Component<IProps, any> {
  handleConnection(currentStatus) {
    stores.ruleStore.setOffsetFeeMercadolivre(123, currentStatus);
  }

  render() {
    return (
      <div>
        <div className={s.rulebox}>
          <div className={s.controls}>
            <a title="Configurar">
              <Icon size="3em" />
            </a>
            <ToogleSwitch
              statusAlt={{
                activated: "Ativado",
                disabled: "Desativado",
                toggleInactive: "Desconectado",
              }}
              value={false}
              onChange={(e) => this.handleConnection(e)}
              active={true}
            />
          </div>
          <label>Somar R$5,00 em produtos com preço abaixo de R$ 120,00</label>
          <span id={s.about}>
            <a>Saiba mais</a>
          </span>
        </div>
        {/* <h2>Regra para somar R$5,00 em produtos com valores abaixo de R$120,00</h2>
            <ToogleSwitch statusAlt={{activated:'Ativado', disabled:'Desativado', toggleInactive:'Desconectado'}} value={false} onChange={e=>this.handleConnection(e)} active={true}/> */}
      </div>
    );
  }
}

{
  /*  <div className={s.rulebox}>
               <div className={s.controls}>
                <a title='Configurar'><Icon size="3em"/></a>
                <ToogleSwitch statusAlt={{activated:'Ativado', disabled:'Desativado', toggleInactive:'Desconectado'}} value={false} onChange={e=>this.handleConnection(e)} active={true}/>

               </div>
                <label>Somar R$5,00 em produtos com preço abaixo de R$ 120,00</label>
                <span id={s.about}><a>Saiba mais</a></span>

            </div> */
}
{
  /* <h2>Regra para somar R$5,00 em produtos com valores abaixo de R$120,00</h2>
            <ToogleSwitch statusAlt={{activated:'Ativado', disabled:'Desativado', toggleInactive:'Desconectado'}} value={false} onChange={e=>this.handleConnection(e)} active={true}/> */
}
