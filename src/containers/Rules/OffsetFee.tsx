import * as React from "react";
import { observer } from "mobx-react";
//import {linkToProductDetails, history} from '../Route/RouteStore';
//import ToogleSwitch from '../../components/ToogleSwitch';
import ProductList from "./ProductList";
import stores from "../../stores";

interface IProps {}

@observer
export class OffsetFee extends React.Component<IProps, any> {
  handleConnection(currentStatus) {
    stores.ruleStore.setOffsetFeeMercadolivre(123, currentStatus);
  }

  setProductsToRule() {
    stores.productStore.selectedsProducts;
  }

  async setRule() {
    //console.log('add rules',stores.productStore.selectedsProducts);
    await stores.ruleStore.addProductsToRuleMercadolivre("offsetFee");
  }

  async removeRule() {
    await stores.ruleStore.removeProductsToRuleMercadolivre("offsetFee");
    //console.log('remove rles',stores.productStore.selectedsProducts);
  }

  queryForAdd() {
    //console.log(stores.plataformStore.rules.offsetFee.products.join("&ninSkus="))
    return stores.plataformStore.rules.offsetFee.products.join("&ninSkus=");
  }

  queryForRemove() {
    //console.log(stores.plataformStore.rules.offsetFee.products.join("&ninSkus="))
    return stores.plataformStore.rules.offsetFee.products.join("&inSkus=");
  }

  render() {
    //console.log('rules store',stores.plataformStore.rules)
    return (
      <div>
        {stores.plataformStore.rules ? (
          <ProductList
            queryForAddRule={`priceTo=120&ninSkus=${this.queryForAdd()}`}
            queryForRemoveRule={`inSkus=${this.queryForRemove()}`}
            setRule={this.setRule.bind(this)}
            removeRule={this.removeRule.bind(this)}
            title={"Somar R$5,00 em produtos abaixo de R$100"}
          />
        ) : (
          " carregando"
        )}
      </div>
    );
  }
}

{
  /*  <div className={s.rulebox}>
               <div className={s.controls}>
                <a title='Configurar'><Icon size="3em"/></a>
                <ToogleSwitch statusAlt={{activated:'Ativado', disabled:'Desativado', toggleInactive:'Desconectado'}} value={false} onChange={e=>this.handleConnection(e)} active={true}/>

               </div>
                <label>Somar R$5,00 em produtos com preço abaixo de R$ 120,00</label>
                <span id={s.about}><a>Saiba mais</a></span>

            </div> */
}
{
  /* <h2>Regra para somar R$5,00 em produtos com valores abaixo de R$120,00</h2>
            <ToogleSwitch statusAlt={{activated:'Ativado', disabled:'Desativado', toggleInactive:'Desconectado'}} value={false} onChange={e=>this.handleConnection(e)} active={true}/> */
}
