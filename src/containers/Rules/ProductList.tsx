import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Row, Col } from "react-bootstrap";
import CheckboxRadioButton from "../../components/CheckboxRadioButton";
import { Table, Checkbox, Button } from "react-bootstrap";
import { formatBrMoney } from "../../helpers";
import TheadSortable from "../../components/TheadSortable";
import stores from "../../stores";
import FilterInfo from "../../components/FilterInfo";
import PaginationAdvanced from "../../components/PaginationAdvanced";
import SpinnerModal from "../../components/SpinnerModal";
import SimpleModal from "../../components/Modal/Simple";

const s = require("./style.scss");

interface IProps {
  queryForAddRule: string;
  queryForRemoveRule: string;
  setRule: () => {};
  removeRule: () => {};
  title: string;
}

interface IState {}

@observer
export default class ProductList extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);
  }

  @observable productStore = stores.productStore;
  @observable modalSimple = false;
  radioValue: string;
  query: string = "";
  @observable page: number = 1;
  action;
  @observable isProcessing = false;
  marketplace = "mercadolivre";

  componentDidMount() {
    this.productStore.unsetProducts();
    this.filterProductsByAction("add");
  }

  setMarketplace(marketplace) {
    return (this.marketplace = marketplace);
  }

  //props
  async filterProductsByAction(action: "add" | "remove") {
    this.action = action;
    this.radioValue = "";
    this.productStore.selectedsProducts = [];
    if (action == "remove") {
      this.query = this.props.queryForRemoveRule; //`offsetFreight=${this.marketplace}`;
    } else {
      console.log("entrou no filter interno");
      this.query = this.props.queryForAddRule; //`freeShipping=${this.marketplace}&offsetFreight[$ne]=${this.marketplace}`;
    }
    await this.getProductList(this.page);
  }

  close() {
    this.modalSimple = false;
  }

  async onClick(value) {
    await this.getProductList(value);
    this.page = value;
  }

  handler(e) {
    if (e.checked) this.productStore.selectedsProducts.push(e.value);
    else {
      let i = this.productStore.selectedsProducts.indexOf(e.value);
      if (i != -1) {
        this.productStore.selectedsProducts.splice(i, 1);
      }
    }
  }

  isChecked(sku) {
    return this.productStore.selectedsProducts.includes(sku) ? true : false;
  }

  runQuery(query) {
    this.query = query;
    clearTimeout(this.action);
    this.action = setTimeout(() => this.getProductList(this.page), 500);
  }

  async handleSelectAll(value) {
    this.radioValue = value;
    this.isProcessing = true;
    if (value == "all") {
      for (let _page = 1; _page <= this.productStore.productsPages; _page++) {
        await this.getProductList(_page);
        await this.productStore.products.map((p) => {
          this.productStore.selectedsProducts.push(p.sku);
        });
      }
    } else {
      for (let _page = 1; _page <= this.productStore.productsPages; _page++) {
        await this.getProductList(_page);
        await this.productStore.products.map((p) => {
          this.productStore.selectedsProducts.remove(p.sku);
        });
      }
    }
    this.isProcessing = false;
  }

  async getProductList(page) {
    console.log(this.query);
    await this.productStore.listProducts(page, this.query);
  }

  async applyRule() {
    if (this.action == "add") await this.props.setRule();
    else await this.props.removeRule();

    this.filterProductsByAction(this.action);

    //await this.productStore.setOffsetFreightRule(this.action,this.marketplace);
    this.modalSimple = true;
    await this.getProductList(this.page);
    this.radioValue = "";
  }

  render() {
    return (
      <div>
        <h2>{this.props.title}</h2>
        {(this.isProcessing == true || stores.ruleStore.inProgress == true) && (
          <SpinnerModal />
        )}

        <SimpleModal
          context={this}
          show={stores.ruleStore.inProgress === false && this.modalSimple}
          message={
            stores.ruleStore.errors
              ? stores.ruleStore.errors.message
              : "Operação Realizada com Sucesso"
          }
          onClick={this.close.bind(this)}
        />
        <Row className="show-grid">
          <Col xs={12} md={8}>
            <div className={s.field}>
              <CheckboxRadioButton
                label={"Ação:"}
                options={[
                  { label: "Adicionar produtos para a regra", value: "add" },
                  { label: "Remover produtos da regra", value: "remove" },
                ]}
                type={"radio"}
                name={"action_selected"}
                value={this.action}
                onChanges={(value) => this.filterProductsByAction(value)}
              />
            </div>
          </Col>
          <Col xs={12} md={4}>
            <div className={s.field}>
              <CheckboxRadioButton
                label={"Escolha os produtos para ação:"}
                options={[
                  { label: "Todos", value: "all" },
                  { label: "Nenhum", value: "none" },
                ]}
                type={"radio"}
                name={"product_selected"}
                value={this.radioValue}
                onChanges={(value) => this.handleSelectAll(value)}
              />
            </div>
          </Col>
        </Row>
        <Row className="show-grid">
          <Col xs={12} md={4}>
            <div className={s.field}>
              <Button
                type="button"
                bsStyle="primary"
                onClick={this.applyRule.bind(this)}
              >
                Aplicar Regra
              </Button>
            </div>
          </Col>
        </Row>
        <Row className="show-grid">
          <Col xs={12} md={12}>
            <FilterInfo
              seletedItems={this.productStore.selectedsProducts.length}
              currentPage={this.page}
              totalItems={this.productStore.productsTotal}
              totalPages={this.productStore.productsPages}
            />
            <PaginationAdvanced
              items={this.productStore.productsPages}
              maxButtons={5}
              page={this.page}
              onClick={this.onClick.bind(this)}
            />
            <Table className="table" hover>
              <TheadSortable
                columns={[
                  { label: "#", name: "#" },
                  { label: "#", name: "img" },
                  { label: "SKU", name: "sku" },
                  { label: "Nome", name: "name" },
                  { label: "Qtd.", name: "qty" },
                  { label: "Preço", name: "price" },
                  { label: "Categorias", name: "categories" },
                ]}
                getQuery={this.runQuery.bind(this)}
              />
              <tbody>
                {this.productStore.products &&
                  this.isProcessing == false &&
                  this.productStore.products.map((u) => (
                    <tr key={u.id}>
                      <td>
                        <Checkbox
                          value={u.sku}
                          defaultChecked={this.isChecked(u.sku)}
                          onClick={(e) => this.handler(e.target)}
                        />
                      </td>
                      <td className={s.image}>
                        <img className="img-thumbnail" src={u.image} />
                      </td>
                      <td>{u.sku}</td>
                      <td>{u.name}</td>
                      <td>{u.qty}</td>
                      <td className={s.tdInline}> {formatBrMoney(u.price)}</td>
                      <td className={s.categories}>
                        {u.categories.map((category) => " | " + category.name)}
                      </td>
                    </tr>
                  ))}
              </tbody>
            </Table>
            <PaginationAdvanced
              items={this.productStore.productsPages}
              maxButtons={5}
              page={this.page}
              onClick={this.onClick.bind(this)}
            />
          </Col>
        </Row>
      </div>
    );
  }
}
