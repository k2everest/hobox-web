import * as React from "react";
import { observer } from "mobx-react";
import { Route, Switch } from "react-router";
import { observable } from "mobx";
import { history, linkToPlataformRules } from "../Route/RouteStore";
import stores from "../../stores";

//components
import ControlledTabs from "../../components/ControlledTabs";

//containers
import OffsetFreight from "./OffsetFreight";
import { OffsetFee } from "./OffsetFee";

interface IProps {
  route: Route;
}

interface IState {}

@observer
export default class Rules extends React.Component<IProps, IState> {
  @observable error: string = "";
  @observable platformId = this.props.match.params.plataformId;
  constructor(props: IProps, context: any) {
    super(props, context);
  }

  componentDidMount() {
    this.platformId = this.props;
    this.platformId = this.platformId.match.params.plataformId;
    if (this.platformId != undefined) {
      stores.plataformStore.getPlataform(this.platformId);
    }
  }

  getPath(ruleName) {
    return `${linkToPlataformRules(this.platformId)}/${ruleName}`;
  }

  render() {
    return (
      <div>
        <h1>Definições de Regras</h1>
        <ControlledTabs
          activeTab={history.location.pathname}
          tab={[
            { title: "Compensar frete", key: this.getPath("offset-freight") },
            {
              title: "Compensar preço abaixo de 100",
              key: this.getPath("offset-fee"),
            },
          ]}
        />
        <Switch>
          <Route
            path={this.getPath("offset-freight")}
            render={(props) => <OffsetFreight {...props} />}
          />
          <Route
            path={this.getPath("offset-fee")}
            render={(props) => <OffsetFee {...props} />}
          />
          <Route
            path={this.getPath("")}
            render={(props) => <OffsetFreight {...props} />}
          />
        </Switch>
      </div>
    );
  }
}
