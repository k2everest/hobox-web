import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import stores from "../../../stores";
import Spinner from "../../../components/Spinner";
import SimpleModal from "../../../components/Modal/Simple";
import Form from "../../../components/Form";

//const s = require('../style.scss');

interface IState {}

interface IProps {}

@observer
export default class SignupForm extends React.PureComponent<IProps, IState> {
  @observable userStore = stores.userStore;
  @observable modalSimple = false;
  @observable submited = false;
  @observable errorExist = {
    name: null,
    lastname: null,
    email: null,
    password: null,
  };

  componentWillMount() {
    this.userStore.currentUser = {
      name: "",
      lastname: "",
      email: "",
      password: "",
      /*  phone:'',
        company: '',
        website:'' */
    };
  }

  componentWillUpdate() {
    if (this.submited == true) this.validateForm();
  }

  close() {
    this.modalSimple = false;
  }

  validateForm() {
    this.errorExist.name = this.validateName(this.userStore.currentUser.name);
    this.errorExist.lastname = this.validateName(
      this.userStore.currentUser.lastname
    );
    this.errorExist.email = this.validateEmail(
      this.userStore.currentUser.email
    );
    this.errorExist.password = this.validatePassword(
      this.userStore.currentUser.password
    );
  }

  validatePassword(value) {
    return value && value.length >= 6 ? "success" : "error";
  }

  validateEmail(value) {
    return value && value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
      ? "success"
      : "error";
  }

  validateName(value) {
    const length = value.length;
    if (length >= 5) return "success";
    else if (length > 0 || (this.submited && length == 0)) return "error";
    return null;
  }

  onSubmit = (isVerified) => {
    this.modalSimple = true;
    this.submited = true;
    this.validateForm();
    const checkErrors = !Object.keys(this.errorExist).some(
      (x) => this.errorExist[x] == "error"
    );
    isVerified && checkErrors ? this.userStore.register() : null;
  };

  render() {
    return (
      <div>
        <SimpleModal
          context={this}
          show={this.userStore.done && this.modalSimple}
          message={
            this.userStore.errors
              ? this.userStore.errors
              : "Dados salvos com sucesso"
          }
          onClick={this.close.bind(this)}
        />

        {this.userStore.inProgress ? (
          <Spinner />
        ) : (
          <Form
            enabledRecaptcha={true}
            onSubmit={this.onSubmit}
            buttonLabel={"Inscrever"}
            fields={[
              {
                label: "Nome",
                type: "text",
                defaultValue: this.userStore.currentUser.name,
                stateValidate: this.errorExist.name,
                errorMessage: "Deve ter ao menos 5 caracteres",
                onChange: (value) => (this.userStore.currentUser.name = value),
              },
              {
                label: "Sobrenome",
                type: "text",
                stateValidate: this.errorExist.lastname,
                errorMessage: "Deve ter ao menos 5 caracteres",
                defaultValue: this.userStore.currentUser.lastname,
                onChange: (value) =>
                  (this.userStore.currentUser.lastname = value),
              },
              {
                label: "E-mail",
                type: "email",
                stateValidate: this.errorExist.email,
                errorMessage: "Informe um e-mail válido",
                defaultValue: this.userStore.currentUser.email,
                onChange: (value) => (this.userStore.currentUser.email = value),
              },
              {
                label: "Senha",
                type: "password",
                stateValidate: this.errorExist.password,
                errorMessage: "A senha deve ter ao menos 6 caracteres",
                defaultValue: this.userStore.currentUser.password,
                onChange: (value) =>
                  (this.userStore.currentUser.password = value),
              },
              /*   {
                            label:'Telefone',
                            type:'text',
                            defaultValue: this.userStore.currentUser.phone,
                            onChange:(value)=>this.userStore.currentUser.phone= value,
                          },
                          {
                            label:'Empresa',
                            type:'text',
                            defaultValue: this.userStore.currentUser.company,
                            onChange:(value)=>this.userStore.currentUser.company= value,
                          },
                          {
                            label:'Website',
                            type:'text',
                            defaultValue: this.userStore.currentUser.website,
                            onChange:(value)=>this.userStore.currentUser.website= value,
                          } */
            ]}
          />
        )}
        {/*     <div className={s.captcha}>
                  <ReCaptcha
                      ref={(el) => {this.captchaDemo = el;}}
                      size="normal"
                      render="explicit"
                      sitekey="6LdyLnAUAAAAAEL72WwUtEFrLAY0q5SxdN14ecSC"
                      onloadCallback={this.onLoadRecaptcha}
                      verifyCallback={this.verifyCallback}
                      hl = {'pt-BR'}
                  />
                </div> */}
      </div>
    );
  }
}
