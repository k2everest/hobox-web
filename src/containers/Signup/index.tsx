import * as React from "react";
import { observer } from "mobx-react";
import { observable } from "mobx";
import stores from "../../stores/";

import { Redirect } from "react-router";
import { Modal } from "react-bootstrap";

/**
 * Components
 */
import SignupForm from "./Forms/SignupForm";

/* Style */
const s = require("./style.scss");

interface IProps {}

interface IState {}

@observer
export default class Signup extends React.Component<IProps, IState> {
  @observable error: string = "";
  authStore = stores.authStore;
  commonStore = stores.commonStore;

  componentWillMount() {
    this.authStore.logged = false;
  }

  render() {
    return (
      <div>
        {this.authStore.inProgress === true ? (
          <Modal show={true} container={this} onHide={() => {}}>
            <div className={s.spinner} />
          </Modal>
        ) : null}

        <div className="col-md-4 col-md-offset-4">
          <div className={s.form}>
            <h1>Inscrever-se</h1>

            {this.commonStore.logged ? <Redirect to={"/"} /> : <SignupForm />}
            {this.authStore.errors === undefined ? null : (
              <div className={s.error}> Usuário ou senha incorretos</div>
            )}
          </div>
        </div>
      </div>
    );
  }
}
