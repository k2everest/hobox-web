import * as React from "react";
import { observer } from "mobx-react";
import { observable } from "mobx";
import stores from "../../stores";
import { Row, Col, Grid } from "react-bootstrap";
import WidgetSimple from "../../components/Widget/Simple";
import ReactTable from "react-table";
import "react-table/react-table.css";
import { Line } from "react-chartjs-2";
import { formatBrMoney } from "../../helpers";

const Spinner = require("react-spinkit");
const s = require("./style.scss");

interface Props {}

const columns = [
  {
    Header: "Sku",
    maxWidth: 100,
    accessor: "_id", // String-based value accessors!
  },
  {
    Header: "Produto",
    accessor: "itemName",
    Cell: (props) => <span className="number">{props.value}</span>, // Custom cell components!
  },
  {
    Header: "Quantidade",
    accessor: "qty", // Custom value accessors!
    maxWidth: 100,
  },
  {
    Header: "Receita", // Custom header components!
    accessor: "total",
    Cell: (props) => (
      <span className="number">{formatBrMoney(props.value)}</span>
    ),
    maxWidth: 100,
  },
];

let data = {
  labels: [],
  datasets: [
    {
      label: "Vendas dos últimos 30 dias",
      backgroundColor: "rgba(255,99,132,0.2)",
      borderColor: "rgba(255,99,132,1)",
      borderWidth: 1,
      hoverBackgroundColor: "rgba(255,99,132,0.4)",
      hoverBorderColor: "rgba(255,99,132,1)",
      data: [],
    },
  ],
};

const Content = ({ context }) => {
  function getTicket(total, numberOfOrders) {
    return total > 0 ? total / numberOfOrders : 0;
  }

  return (
    <div>
      {context.orderStore.ordersSummaries == undefined ||
      context.orderStore.ordersSummaries.total == undefined ? (
        <Spinner
          name="line-scale-pulse-out"
          fadeIn="none"
          className={s.spinner}
        />
      ) : (
        <Grid fluid={true}>
          <Row className="show-grid">
            <Col xs={12} md={4}>
              <WidgetSimple
                label="Faturamento do Mês"
                content={formatBrMoney(
                  context.orderStore.ordersSummaries.total
                )}
                key={"receita"}
              />
            </Col>
            <Col xs={12} md={4}>
              <WidgetSimple
                label="Número de Vendas"
                content={context.orderStore.ordersSummaries.orderNumber.toFixed(
                  0
                )}
                key={"vendas"}
              />
            </Col>
            <Col xs={12} md={4}>
              <WidgetSimple
                label="Ticket Médio"
                content={formatBrMoney(
                  getTicket(
                    context.orderStore.ordersSummaries.total,
                    context.orderStore.ordersSummaries.orderNumber
                  )
                )}
                key={"media"}
              />
            </Col>
          </Row>
        </Grid>
      )}
      {context.orderStore.ordersReportsDates == undefined ? (
        <Spinner
          name="line-scale-pulse-out"
          fadeIn="none"
          className={s.spinner}
        />
      ) : (
        <Grid fluid={true}>
          <Row>
            <Col xs={12} md={12}>
              <div className={s.table}>
                <Line
                  data={data}
                  width={100}
                  height={50}
                  options={{
                    maintainAspectRatio: true,
                  }}
                />
              </div>
            </Col>
          </Row>
        </Grid>
      )}
      {context.orderStore.ordersReportsProducts == undefined ? (
        <Spinner
          name="line-scale-pulse-out"
          fadeIn="none"
          className={s.spinner}
        />
      ) : (
        <Grid fluid={true}>
          <Row>
            <Col xs={12} md={12}>
              <div className={s.table}>
                <ReactTable
                  data={context.orderStore.ordersReportsProducts as Array<any>}
                  columns={columns}
                  defaultPageSize={10}
                  className="-striped -highlight"
                  previousText="Retroceder"
                  nextText="Avançar"
                  loadingText="carregando..."
                  noDataText="nenhum pedido encontrado"
                  pageText="Página"
                  ofText="de"
                  rowsText="linhas"
                />
              </div>
            </Col>
          </Row>
        </Grid>
      )}
    </div>
  );
};

@observer
export class Dashboard extends React.Component<Props, any> {
  @observable error: string = "";
  orderStore = stores.orderStore;

  constructor(props: Props, context: any) {
    super(props, context);
  }

  componentWillMount() {
    this.orderStore.getOrdersReportsDates();
    this.orderStore.getOrdersSummaries();
    this.orderStore.getOrdersReportsProducts();
  }

  componentWillUpdate() {
    if (this.orderStore.ordersReportsDates != undefined) {
      data.labels = this.orderStore.ordersReportsDates.map((e) => {
        return e._id;
      });
      data.datasets[0].data = this.orderStore.ordersReportsDates.map((e) => {
        return e.total.toFixed(2);
      });
    }
  }

  render() {
    return (
      <div>
        <h1>Dashboard</h1>
        {
          /*   this.orderStore.ordersReportsDates && this.orderStore.ordersReportsDates.length == 0 &&
                this.orderStore.ordersReportsProducts && this.orderStore.ordersReportsProducts.length==0 ?
                  " Recurso indisponível. Por favor verifique as configurações."
                :  */
          this.orderStore.ordersReportsDates &&
          this.orderStore.ordersReportsProducts ? (
            <Content context={this} />
          ) : (
            <Spinner
              name="line-scale-pulse-out"
              fadeIn="none"
              className={s.spinner}
            />
          )
        }
      </div>
    );
  }
}
