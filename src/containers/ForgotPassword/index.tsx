import * as React from "react";
import { observer } from "mobx-react";
import { observable } from "mobx";
import stores from "../../stores";
import { Modal } from "react-bootstrap";
import { Redirect, Route, Switch } from "react-router";

/**
 * Components
 */
import SimpleModal from "../../components/Modal/Simple";
import ForgotForm, { FormFields } from "./ForgotForm";
import ResetForm, { ResetFormFields } from "./ResetForm";

/* Style */
const s = require("./style.scss");

interface IProps {
  route: Route;
}

interface IState {}

@observer
export default class ForgotPassword extends React.Component<IProps, IState> {
  @observable error: string = "";
  @observable authStore = stores.authStore;
  commonStore = stores.commonStore;

  constructor(props: IProps, context: any) {
    super(props, context);
  }

  componentWillMount() {
    this.authStore.logged = false;
  }

  onSubmit = (fields: FormFields) => {
    this.authStore.setEmail(fields.email);
    this.authStore.forgotPassword();
  };

  close() {
    this.authStore.done = false;
  }

  onSubmitReset = (fields: ResetFormFields) => {
    this.authStore.setPasswordReset(fields);
    //this.authStore.setEmail(fields.email);
    //this.authStore.forgotPassword();
  };

  render() {
    return (
      <div>
        <SimpleModal
          context={this}
          show={this.authStore.done}
          message={
            this.authStore.errors
              ? this.authStore.errors
              : this.authStore.successMessage
          }
          onClick={this.close.bind(this)}
        />
        <Modal
          show={this.authStore.inProgress}
          container={this}
          onHide={() => {}}
        >
          <div className={s.spinner} />
        </Modal>

        <div className="col-md-4 col-md-offset-4">
          <div className={s.form}>
            <h1>Recuperar Senha</h1>

            {this.commonStore.logged ||
            (this.authStore.resetedPassword == true &&
              this.authStore.done == false) ? (
              <Redirect to={"/"} />
            ) : (
              <Switch>
                <Route
                  path={"/user/forgot-password"}
                  render={(props) => (
                    <ForgotForm {...props} onSubmit={this.onSubmit} />
                  )}
                />
                <Route
                  path={"/user/reset-password"}
                  render={(props) => (
                    <ResetForm {...props} onSubmit={this.onSubmitReset} />
                  )}
                />
              </Switch>
            )}
            {this.authStore.errors === undefined ? null : (
              <div className={s.error}> {this.authStore.errors}</div>
            )}
          </div>
        </div>
      </div>
    );
  }
}
