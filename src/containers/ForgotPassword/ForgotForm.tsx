import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Button } from "react-bootstrap";

/**
 ** Style
 */

const s = require("./style.scss");

export interface FormFields {
  email: string;
}

interface IFormProps {
  onSubmit: (fields: FormFields) => any;
}

interface IFormState {}

@observer
export default class ForgotForm extends React.Component<
  IFormProps,
  IFormState
> {
  @observable email: string = "";

  onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    this.props.onSubmit({
      email: this.email,
    });
  };
  handleChange(e) {
    this.email = e.target.value;
  }

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit} className={s.form}>
          <div className={s.field}>
            <input
              type="text"
              placeholder="E-mail"
              value={this.email}
              onChange={(e) => (this.email = e.target.value)}
            />
          </div>

          <Button type="submit" bsStyle="primary">
            Enviar
          </Button>
        </form>
      </div>
    );
  }
}
