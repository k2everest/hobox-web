import * as React from "react";
import { observable, computed } from "mobx";
import { observer } from "mobx-react";
import { Button } from "react-bootstrap";
const queryString = require("qs");

/**
 ** Style
 */
const s = require("./style.scss");

export interface ResetFormFields {
  newPassword: string;
  confirmPassword: string;
  token: string;
}

interface IFormProps {
  onSubmit: (fields: ResetFormFields) => any;
}

interface IFormState {}

@observer
export default class ResetForm extends React.Component<IFormProps, IFormState> {
  @observable newPassword: string = "";
  @observable confirmPassword: string = "";
  @observable token: any = "";

  @computed get match() {
    return this.confirmPassword == this.newPassword && this.newPassword != ""
      ? false
      : true;
  }

  componentDidMount() {
    const parsed = queryString.parse(location.search);
    this.token = parsed.token;
  }

  onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    this.props.onSubmit({
      newPassword: this.newPassword,
      confirmPassword: this.confirmPassword,
      token: this.token,
    });
  };

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit} className={s.form}>
          <div className={s.field}>
            <input
              type="password"
              placeholder="Nova Senha"
              value={this.newPassword}
              onChange={(e) => (this.newPassword = e.target.value)}
            />
          </div>

          <div className={s.field}>
            <input
              type="password"
              placeholder="Confirmar senha"
              value={this.confirmPassword}
              onChange={(e) => (this.confirmPassword = e.target.value)}
            />
          </div>

          <Button
            type="submit"
            bsStyle="primary"
            disabled={this.match}
            label={"tes"}
          >
            Enviar
          </Button>
        </form>
      </div>
    );
  }
}
