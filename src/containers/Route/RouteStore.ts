import * as React from "react";
import SettingsView from "../Settings/SettingsView";
import PlataformMapping from "../Settings/Mappings";
//import {UsersView} from '../User/UsersView';
import { UsersDetailsView } from "../User/UserDetailsView";
import ProductDetails from "../Product/ProductDetails";
import UserEdit from "../User/Edit";
import ForgotPassword from "../ForgotPassword";

//import createHistory from 'history/createBrowserHistory';
import { createBrowserHistory } from "history";
export const history = createBrowserHistory({});
import { Product } from "../Product";
import { Dashboard } from "../Dashboard";
import { Order } from "../Order";
import Sync from "../Sync";
import Hub from "../Hub";
import Rules from "../Rules";

import OrderDetails from "../Order/OrderDetails";

//users
export const USERS_MAPPING = "/users";
export const USERS_MAPPING_EDIT = "/users/me";
export const USERS_MAPPING_PASSWORD_EDIT = "/users/password";
export const USERS_DEACTIVATE = "/users/deactivate";
export const USERS_MAPPING_COMPANY_EDIT = "/users/me/company";
export const USERS_PAYMENT = "/users/payment";

export const USER_DETAILS_MAPPING = "/user/:userId";

export const PLATAFORMS_MAGENTO = "/magento/:plataformId";
export const PLATAFORMS_BLING = "/bling/:plataformId";

export const PLATAFORMS_DETAILS_MAPPING = "/plataform/:plataformId";
//export const PLATAFORMS_MAPPING = '/plataform/mapping/:plataformId';
export const PLATAFORMS_MERCADO_LIVRE = "/mercadolivre/:plataformId";
export const PLATAFORMS_AMAZON = "/amazon/:plataformId";
export const PLATAFORMS_MERCADO_LIVRE_CONFIRM =
  "/mercadolivre/:plataformId/:email/:nickname";
export const PLATAFORM_ORDERSTATUS = "/plataform/orderStatus/:plataformId";
export const MERCADO_LIVRE_CATEGORIES =
  "/plataform/:plataformId/categories-map";
export const MERCADO_LIVRE_ATTRIBUTES =
  "/plataform/:plataformId/attributes-map";
export const PLATAFORM_ATTRIBUTESTYPES =
  "/plataform/:plataformId/attributesTypes";
export const PLATAFORM_RULES = "/plataform/:plataformId/rules";

export const PRODUCT_DETAILS = "/products/:productId";
//export const MAGENTO_PRODUCT = '/products/:productId/magento';
export const MERCADO_LIVRE_PRODUCT = "/products/:productId/mercadolivre";

export const ORDERS_MAPPING = "/orders";
export const ORDER_DETAILS = "/orders/:orderId";

export const linkToUserDetails = (userId: string) =>
  USER_DETAILS_MAPPING.replace(":userId", userId);

export const linkToProductDetails = (productId: string) =>
  PRODUCT_DETAILS.replace(":productId", productId);

export const linkToOrderDetails = (orderId: string) =>
  ORDER_DETAILS.replace(":orderId", orderId);

export const linkToCartDetails = (packId: string) =>
  ORDER_DETAILS.replace(":orderId", `${packId}?packId=${packId}`);

export const linkToPlataformDetails = (
  plataformId: string,
  plataform: string
) =>
  PLATAFORMS_DETAILS_MAPPING.replace(":plataformId", plataformId).replace(
    "plataform",
    plataform
  );
export const linkToPlataformMapping = (plataformId: string) =>
  PLATAFORM_ORDERSTATUS.replace(":plataformId", plataformId);
export const linkToPlataformRules = (plataformId: string) =>
  PLATAFORM_RULES.replace(":plataformId", plataformId);

export class RouteStore {
  routes: Array<Route> = [
    //dashboard
    {
      title: "Dashboard",
      icon: "dashboard",
      mapping: "/",
      component: Dashboard,
    },
    //users
    {
      mapping: "/user/forgot-password",
      component: ForgotPassword,
    },
    /*      {
            title: 'Usuários',
            icon: 'users',
            mapping: USERS_MAPPING,
            component: UsersView
        }, */

    {
      mapping: USER_DETAILS_MAPPING,
      component: UsersDetailsView,
    },
    {
      mapping: USERS_MAPPING_EDIT,
      component: UserEdit,
    },
    {
      mapping: USERS_MAPPING_PASSWORD_EDIT,
      component: UserEdit,
    },
    {
      mapping: USERS_DEACTIVATE,
      component: UserEdit,
    },

    //plataforms
    {
      title: "Plataformas",
      icon: "platforms",
      mapping: "/plataforms",
      component: SettingsView,
    },
    {
      mapping: PLATAFORMS_DETAILS_MAPPING,
      component: SettingsView,
    },
    {
      mapping: PLATAFORMS_MAGENTO,
      component: SettingsView,
    },
    {
      mapping: PLATAFORMS_AMAZON,
      component: SettingsView,
    },
    {
      mapping: PLATAFORMS_BLING,
      component: SettingsView,
    },
    {
      mapping: PLATAFORMS_MERCADO_LIVRE,
      component: SettingsView,
    },
    {
      mapping: MERCADO_LIVRE_CATEGORIES,
      component: PlataformMapping,
    },
    {
      mapping: MERCADO_LIVRE_ATTRIBUTES,
      component: PlataformMapping,
    },
    {
      mapping: PLATAFORM_ATTRIBUTESTYPES,
      component: PlataformMapping,
    },
    {
      mapping: PLATAFORMS_MERCADO_LIVRE_CONFIRM,
      component: SettingsView,
    },
    {
      mapping: PLATAFORM_ORDERSTATUS,
      component: PlataformMapping,
    },
    {
      mapping: USERS_MAPPING_COMPANY_EDIT,
      component: UserEdit,
    },
    {
      mapping: USERS_PAYMENT,
      component: UserEdit,
    },

    //products
    {
      title: "Produtos",
      icon: "products",
      mapping: "/products",
      component: Product,
    },
    {
      mapping: PRODUCT_DETAILS,
      component: ProductDetails,
    },
    {
      mapping: MERCADO_LIVRE_PRODUCT,
      component: ProductDetails,
    },

    //orders
    {
      title: "Pedidos",
      icon: "orders",
      mapping: ORDERS_MAPPING,
      component: Order,
    },
    {
      mapping: ORDER_DETAILS,
      component: OrderDetails,
    },

    //sync
    {
      title: "Sincronizações",
      icon: "sync",
      mapping: "/sync",
      component: Sync,
    },
    {
      mapping: "/sync-logs",
      component: Sync,
    },

    {
      mapping: PLATAFORM_RULES,
      component: Rules,
    },
    {
      mapping: PLATAFORM_RULES + "/offset-freight",
      component: Rules,
    },
    {
      mapping: PLATAFORM_RULES + "/offset-fee",
      component: Rules,
    },

    //hub
    {
      title: "Definições",
      icon: "definition",
      mapping: "/status-orders",
      component: Hub,
    },
    {
      mapping: "/categories",
      component: Hub,
    },
    {
      mapping: "/attributes",
      component: Hub,
    },
  ];
}

interface Route {
  title?: string;
  icon?: string;
  mapping: string;
  component: React.ReactType;
}
