import * as React from "react";
import { inject } from "mobx-react";
import { RouteStore } from "./RouteStore";
import { Link } from "react-router-dom";
import { observable } from "mobx";
import { observer } from "mobx-react";
const IconMenu = require("react-icons/lib/fa/align-justify");
const CloseMenu = require("react-icons/lib/fa/close");
const Dashboard = require("react-icons/lib/fa/dashboard");
const Platforms = require("react-icons/lib/fa/share-alt");
const Products = require("react-icons/lib/fa/product-hunt");
const Order = require("react-icons/lib/fa/file-text");
const Sync = require("react-icons/lib/fa/refresh");
const Definition = require("react-icons/lib/fa/database");

//import { FaUsers, FaAlignJustify as IconMenu, FaWindowClose as CloseMenu} from 'react-icons/fa';

interface Props {
  routeStore?: RouteStore;
}

const Icon = ({ icon }): any => {
  console.log(icon);
  switch (icon) {
    case "dashboard":
      return <Dashboard />;
    case "platforms":
      return <Platforms />;
    case "products":
      return <Products />;
    case "orders":
      return <Order />;
    case "sync":
      return <Sync />;
    case "definition":
      return <Definition />;
    default: {
      console.log("deault");
      return <CloseMenu />;
    }
  }
};

@inject("routeStore")
@observer
export class Menu extends React.Component<Props, any> {
  @observable isOpen = false;
  @observable dinamicClass = "nav flex-column mobileCollapse ";

  changeStatus() {
    this.isOpen = !this.isOpen;
    this.dinamicClass = this.isOpen
      ? "nav flex-column"
      : "nav flex-column mobileCollapse";
  }

  render() {
    return (
      <div className="sidebar-sticky">
        <button
          onClick={this.changeStatus.bind(this)}
          className="showMobile hamburger"
        >
          {this.isOpen ? <CloseMenu /> : <IconMenu />}
        </button>
        <ul className={this.dinamicClass}>
          {this.props.routeStore.routes
            .filter((r) => !!r.title)
            .map((r) => (
              <li key={r.mapping}>
                <Link
                  key={r.mapping}
                  to={r.mapping}
                  onClick={this.changeStatus.bind(this)}
                >
                  <Icon icon={r.icon} /> {r.title}
                </Link>
              </li>
            ))}
        </ul>
      </div>
    );
  }
}
