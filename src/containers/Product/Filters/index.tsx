import * as React from "react";
import { Filters } from "../../../components/Filters";
import { observer } from "mobx-react";
import { observable } from "mobx";
import stores from "../../../stores";

import MultiSelect from "@khanacademy/react-multi-select";

interface Props {}

@observer
export class ProductFilters extends React.Component<Props, any> {
  @observable productStore = stores.productStore;
  @observable hubStore = stores.hubStore;
  @observable page: number = 1;

  valueRenderer(selected, options) {
    if (selected.length === 0) {
      return "Selecione ...";
    }
    let result = options
      .filter((sel) => (selected.includes(sel.value) ? true : false))
      .map((a) => a.label);
    return result.join(",");
  }

  constructor(props: Props, context: any) {
    super(props, context);
  }

  shouldComponentUpdate(nextProps) {
    if (this.props != nextProps) return true;
  }

  defineQuery(e) {
    const name = e.target.name;
    const value = e.target.value;
    let direction = false;
    for (let i in this.productStore.query.slice()) {
      if (this.productStore.query[i].name == name) {
        if (name == "sort" && this.productStore.query[i].value == value)
          direction = true;
        this.productStore.query[i].value = value;
        break;
      }
    }

    if (direction)
      this.productStore.query = this.productStore.query.map((a) => {
        if (a.name == "direction") {
          a.value = a.value * -1;
        }
        return a;
      });
    else
      this.productStore.query = this.productStore.query.map((a) => {
        if (a.name == "direction") {
          a.value = 1;
        }
        return a;
      });
    //let query = this.convertQuery(this.productStore.query.slice());
    //return query;
  }

  filter(e) {
    this.defineQuery(e);
    this.getFilteredProducts();
  }

  convertQuery(queryArray) {
    let query = queryArray.map((a) => `${a.name}=${a.value}`).join("&");
    return query;
  }

  getFilteredProducts() {
    let categories = this.productStore.selectedCategories
      .map((a) => `categories[]=${a}`)
      .join("&");
    let mk = this.productStore.selectedMarketplaces
      .map((a) => `marketplaces[]=${a}`)
      .join("&");
    let status = this.productStore.selectedStatus
      .map((a) => `status[]=${a}`)
      .join("&");
    let query = `${this.convertQuery(
      this.productStore.query.slice()
    )}&${mk}&${status}&${categories}`;
    this.productStore.listProducts(this.page, query);
  }

  componentWillMount() {
    this.getFilteredProducts();
    this.hubStore.getCategories();
  }

  getFilterValue(name) {
    return this.productStore.query.reduce((v, e) =>
      e.name == name ? (v = e.value) : v
    );
  }

  render() {
    return (
      <Filters
        fields={[
          {
            label: "SKU",
            input: [
              {
                name: "sku",
                type: "text",
                value: this.getFilterValue("sku"),
                onChange: this.filter.bind(this),
              },
            ],
          },
          {
            label: "Nome",
            input: [
              {
                name: "name",
                type: "text",
                value: this.getFilterValue("name"),
                onChange: this.filter.bind(this),
              },
            ],
          },
          {
            label: "Qtd",
            input: [
              {
                name: "qtyFrom",
                type: "number",
                value: this.getFilterValue("qtyFrom"),
                placeholder: "Min",
                style: "small",
                onChange: this.filter.bind(this),
              },
              {
                name: "qtyTo",
                style: "small",
                value: this.getFilterValue("qtyTo"),
                placeholder: "Max",
                type: "number",
                onChange: this.filter.bind(this),
              },
            ],
          },
          {
            label: "Preço",
            input: [
              {
                name: "priceFrom",
                type: "number",
                value: this.getFilterValue("priceFrom"),
                placeholder: "Min",
                style: "small",
                onChange: this.filter.bind(this),
              },
              {
                name: "priceTo",
                style: "small",
                value: this.getFilterValue("priceTo"),
                placeholder: "Max",
                type: "number",
                onChange: this.filter.bind(this),
              },
            ],
          },
        ]}
      >
        <th>
          <label>Marketplaces</label>
          <MultiSelect
            options={[
              { label: "Mercado Livre", value: "mercadolivre" },
              { label: "Nenhum", value: "none" },
            ]}
            selected={this.productStore.selectedMarketplaces}
            onSelectedChanged={(selected) => {
              this.productStore.selectedMarketplaces = selected;
              this.getFilteredProducts();
            }}
            shouldToggleOnHover={false}
            valueRenderer={this.valueRenderer.bind(this)}
            disableSearch={true}
            hasSelectAll={false}
          />
        </th>
        {this.hubStore.categories ? (
          <th>
            <label>Categorias</label>
            <MultiSelect
              options={this.hubStore.categories.map((c) => {
                return { label: c.name, value: c.id };
              })}
              selected={this.productStore.selectedCategories}
              onSelectedChanged={(selected) => {
                this.productStore.selectedCategories = selected;
                this.getFilteredProducts();
              }}
              shouldToggleOnHover={false}
              hasSelectAll={false}
              valueRenderer={this.valueRenderer.bind(this)}
              disableSearch={true}
              selectAllLabel="Selecionar todos"
            />
          </th>
        ) : (
          <th />
        )}
        <th>
          <label>Status</label>
          <MultiSelect
            options={[
              { label: "Conectado", value: "conectado" },
              { label: "Desconectado", value: "desconectado" },
            ]}
            selected={this.productStore.selectedStatus}
            onSelectedChanged={(selected) => {
              this.productStore.selectedStatus = selected;
              this.getFilteredProducts();
            }}
            shouldToggleOnHover={false}
            hasSelectAll={true}
            valueRenderer={this.valueRenderer.bind(this)}
            disableSearch={true}
            selectAllLabel="Selecionar todos"
          />
        </th>
      </Filters>
    );
  }
}
