import * as React from "react";
import { observer } from "mobx-react";
import { observable } from "mobx";
import stores from "../../../stores";
import Select from "../../../components/Select";
import { DropdownButton, MenuItem, ButtonGroup } from "react-bootstrap";
import { MassActionsMercadoLivre } from "./MercadoLivre";
import SimpleModal from "../../../components/Modal/Simple";
import ConfirmModal from "../../../components/Modal/Confirm";

//styles scss
const s = require("./style.scss");

const handleResponseApi = (result: Array<any>) => {
  let text = "";
  console.log("result a", result);
  result.map((e) => {
    text = text + "\n" + e;
  });
  if (result.length == 0) text = "Carregando informações";

  return text;
};

interface Props {}

@observer
export default class MassActions extends React.Component<Props, any> {
  @observable productStore = stores.productStore;
  @observable resultApi = "";
  @observable isConfirmModalEnable = false;

  shouldComponentUpdate() {
    return this.productStore.responseApi.length > 0 ? true : false;
  }

  componentDidUpdate() {
    this.resultApi = handleResponseApi(this.productStore.responseApi.slice());
  }

  close() {
    this.isConfirmModalEnable = false;
  }

  render() {
    return (
      <div>
        {this.productStore.openedBox == true &&
        this.productStore.stepFormIsOpened == false ? (
          <SimpleModal
            context={this}
            show={true}
            message={
              this.productStore.errors
                ? this.productStore.errors.message
                : this.resultApi
            }
            onClick={() => this.productStore.closeBox()}
          />
        ) : (
          ""
        )}
        {this.isConfirmModalEnable == true ? (
          <ConfirmModal
            action={() => {
              this.productStore.sendToMarketplace("disconnect");
              this.isConfirmModalEnable = false;
            }}
            close={this.close.bind(this)}
            context={this}
            message="A exclusão dos anúncios no marketplace é irreversível, deseja realmente excluir os anúncios selecionados ?"
            show={this.isConfirmModalEnable}
            title={"Desconectar"}
          />
        ) : (
          ""
        )}
        {this.productStore.stepFormIsOpened == true ? (
          this.productStore.selectedMarketplace ? (
            <MassActionsMercadoLivre />
          ) : (
            <SimpleModal
              context={this}
              show={true}
              message={"Selecione a Plataforma"}
              onClick={() => this.productStore._closeStepForm()}
            />
          )
        ) : (
          ""
        )}
        <form className={s.actions}>
          <Select
            controlLabel="Marketplaces"
            placeholder={"Selecione"}
            onChange={(value) => this.productStore.chooseMarketplace(value)}
            value={this.productStore.selectedMarketplace}
            externalKey="marketplace"
            options={[{ label: "Mercado Livre", value: "mercadolivre" }]}
          />

          <ButtonGroup bsStyle="primary  pull-right" className={s.alignBotton}>
            <DropdownButton
              bsStyle="primary"
              title="Ação em Massa"
              id="right-panel-link"
            >
              <MenuItem
                eventKey="1"
                onClick={() => this.productStore._openStepForm()}
              >
                Conectar
              </MenuItem>
              <MenuItem
                eventKey="2"
                onClick={() => this.productStore.sendToMarketplace("pause")}
              >
                Pausar
              </MenuItem>
              <MenuItem
                eventKey="3"
                onClick={() => (this.isConfirmModalEnable = true)}
              >
                Desconectar
              </MenuItem>
            </DropdownButton>
          </ButtonGroup>
        </form>
      </div>
    );
  }
}
