import * as React from "react";
import { observer } from "mobx-react";
import { observable } from "mobx";
import stores from "../../../../stores";
import FormStepLoki from "../../../../components/FormStepLoki";
import CheckboxRadioButton from "../../../../components/CheckboxRadioButton";

const ListingType = ({ currentValue, onChange }) => {
  console.log(currentValue);
  return (
    <CheckboxRadioButton
      label={"Tipo de Anuncio?"}
      options={[
        { label: "Classico", value: "gold_special" },
        { label: "Premium", value: "gold_pro" },
        { label: "Grátis", value: "free" },
      ]}
      name={"listingType"}
      type={"radio"}
      value={currentValue}
      onChanges={(value) => onChange("listingType", value)}
    />
  );
};

const ShippingMode = ({ currentValue, onChange }) => {
  return (
    <CheckboxRadioButton
      label={"Tipo de Frete?"}
      options={[
        { label: "Customizado", value: "custom" },
        { label: "ME1", value: "me1" },
        { label: "ME2", value: "me2" },
        { label: "Não especificado", value: "not_specified" },
      ]}
      name={"shippingMode"}
      type={"radio"}
      value={currentValue}
      onChanges={(value) => onChange("shippingMode", value)}
    />
  );
};

const ShippingFree = ({ currentValue, onChange }) => {
  return (
    <CheckboxRadioButton
      label={"Frete Grátis?"}
      options={[
        { label: "Não", value: false },
        { label: "Expresso", value: "500645" },
        { label: "Normal", value: "100009" },
        //{label:'Expresso Noroeste', value:'505145'}
        /*   {label:'Não', value: false},
              {label:'Expresso', value: {id:'500645', freeOptions:'country'}},
              {label:'Expresso', value: {id:'182', freeOptions:'country'}},
              {label:'Super Expresso Noroeste', value: {id:'505145', freeOptions:'no'}},
              {label:'Normal', value: {id:'100009', freeOptions:'country'}} */
      ]}
      name={"shippingFree"}
      type={"radio"}
      value={currentValue}
      onChanges={(value) => onChange("shippingFree", value)}
    />
  );
};

const handleResponseApi = (result: Array<any>) => {
  let text = "";
  console.log("result a", result);
  result.map((e) => {
    text = text + "\n" + e;
  });
  if (result.length == 0) text = "Carregando informações...";

  return text;
};

interface Props {}

@observer
export class MassActionsMercadoLivre extends React.Component<Props, any> {
  @observable productStore = stores.productStore;
  @observable isFinalPage = false;
  @observable resultApi = "";

  componentWillMount() {
    this.productStore.errorMessage;
  }

  componentWillUpdate() {
    this.resultApi = handleResponseApi(this.productStore.responseApi.slice());
  }

  setData(name, value) {
    this.productStore.dataMercadolivre[name] = value;
  }

  _onFinish() {
    this.isFinalPage = true;
    this.productStore.sendToMarketplace("connect");
  }

  render() {
    console.log("erros", this.productStore.errors);
    return (
      <FormStepLoki
        steps={[
          {
            label: "Passo 1",
            component: (
              <ListingType
                currentValue={this.productStore.dataMercadolivre.listingType}
                onChange={this.setData.bind(this)}
              />
            ),
          },
          {
            label: "Passo 2",
            component: (
              <ShippingMode
                currentValue={this.productStore.dataMercadolivre.shippingMode}
                onChange={this.setData.bind(this)}
              />
            ),
          },
          {
            label: "Passo 3",
            component: (
              <ShippingFree
                currentValue={this.productStore.dataMercadolivre.shippingFree}
                onChange={this.setData.bind(this)}
              />
            ),
          },
        ]}
        onFinish={this._onFinish.bind(this)}
        closeBox={() => this.productStore._closeStepForm()}
        canFinish={this.productStore.errorMessage == ""}
        messageValidate={this.productStore.errorMessage}
        isFinalPage={this.isFinalPage}
        finalMessage={
          this.productStore.errors
            ? this.productStore.errors.message
            : this.resultApi
        }
      />
    );
  }
}
