import * as React from "react";
import { observer } from "mobx-react";
import { Route, Switch } from "react-router";
import { observable } from "mobx";
import {
  PRODUCT_DETAILS,
  MERCADO_LIVRE_PRODUCT,
  history,
} from "../Route/RouteStore";

//components
import ControlledTabs from "../../components/ControlledTabs";

//containers
import MagentoProduct from "./Plataforms/MagentoProduct";
import MercadoLivreProduct from "./Plataforms/MercadoLivreProduct";

//style
const s = require("./style.scss");

interface IProps {
  route: Route;
}

interface IState {}

@observer
export default class ProductDetails extends React.Component<IProps, IState> {
  @observable error: string = "";
  productId: any;
  constructor(props: IProps, context: any) {
    super(props, context);
  }

  componentWillMount() {
    this.productId = history.location.pathname.split("/")[2];
    console.log("mount", this.productId);
    //alterar product id -
  }

  render() {
    console.log(this.productId);
    return (
      <div className={s.content}>
        <ControlledTabs
          activeTab={history.location.pathname}
          tab={[
            {
              title: "Magento",
              key: PRODUCT_DETAILS.replace(":productId", this.productId),
            },
            {
              title: "Mercado Livre",
              key: MERCADO_LIVRE_PRODUCT.replace(":productId", this.productId),
            },
          ]}
        />
        <Switch>
          <Route
            exact
            path={PRODUCT_DETAILS}
            render={(props) => <MagentoProduct {...props} />}
          />
          <Route
            exact
            path={MERCADO_LIVRE_PRODUCT}
            render={(props) => <MercadoLivreProduct {...props} />}
          />
        </Switch>
      </div>
    );
  }
}
