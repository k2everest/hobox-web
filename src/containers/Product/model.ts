export interface Product {
  id: string;
  sku: string;
  name: string;
  qty: string;
  categories: [{}];
  price: string;
}

export interface Transaction {
  id: number;
  description: string;
}
