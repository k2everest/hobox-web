import * as React from "react";
import { observer } from "mobx-react";
import { linkToProductDetails, history } from "../Route/RouteStore";
import PaginationAdvanced from "../../components/PaginationAdvanced";
import { Table, Checkbox } from "react-bootstrap";
import { formatBrMoney } from "../../helpers";
import MassActions from "../Product/MassActions";
import { observable } from "mobx";
import TheadSortable from "../../components/TheadSortable";
import AsyncFilters from "../../components/AsyncFilters";
//import  SpinnerModal from '../../components/SpinnerModal';
import Spinner from "../../components/Spinner";
import ToogleSwitch from "../../components/ToogleSwitch";
import stores from "../../stores";
const FaSearchPlus = require("react-icons/lib/fa/search-plus");
const s = require("./style.scss");
interface Props {}
import ProgressModal from "../../components/Modal/Progress";
import SpanInfo from "../../components/SpanInfo";
import FilterInfo from "../../components/FilterInfo";

@observer
export class Product extends React.Component<Props, any> {
  @observable selectedsProducts = [];
  @observable productStore = stores.productStore;
  @observable hubStore = stores.hubStore;
  @observable page: number = 1;
  query: string = "";
  action;

  componentWillMount() {
    this.hubStore.getCategories();
    //this.productStore.listProducts(this.page,this.query);
  }

  onClick(value) {
    this.productStore.listProducts(value, this.query);
    this.page = value;
  }

  goToProduct(id) {
    history.push(linkToProductDetails(id));
  }

  handler(e) {
    if (e.checked) this.productStore.selectedsProducts.push(e.value);
    else {
      let i = this.productStore.selectedsProducts.indexOf(e.value);
      if (i != -1) {
        this.productStore.selectedsProducts.splice(i, 1);
      }
    }
  }

  handleConnection(currentStatus, sku) {
    this.productStore.selectedsProducts.push(sku);
    currentStatus == true
      ? this.productStore.sendToMarketplace("connect")
      : this.productStore.sendToMarketplace("pause");
  }

  runQuery(query) {
    this.query = query;
    clearTimeout(this.action);
    this.action = setTimeout(
      () => this.productStore.listProducts(this.page, query),
      500
    );
  }

  getColorStatus(currentStatus: string): string {
    const status: {
      key: string;
      color: string;
    }[] = [
      { key: "active", color: "#5cb85c" },
      { key: "paused", color: "#fbab60" },
      { key: "under_review", color: "#fbab60" },
      { key: "closed", color: "red" },
    ];
    return status.filter((a) => a.key == currentStatus)[0]
      ? status.filter((a) => a.key == currentStatus)[0].color
      : "gray";
  }

  handleStatus(status) {
    return status == "active" ? true : false;
  }

  render() {
    return (
      <div>
        <h1>Produtos</h1>
        <MassActions />
        <ProgressModal
          errorMessage={
            this.productStore.errors
              ? this.productStore.errors.message
              : undefined
          }
          title={"Processando Requisição"}
          context={this}
          show={this.productStore.openedBox}
          message={
            this.productStore.responseApi ? this.productStore.responseApi : []
          }
          onClick={() => this.productStore.closeBox()}
          isFinished={this.productStore.done}
        />
        {this.hubStore.categories != undefined ? (
          <AsyncFilters
            getQuery={this.runQuery.bind(this)}
            title="Filtros"
            fields={[
              { label: "SKU", name: "sku", type: "text" },
              { label: "Nome", name: "name", type: "text" },
              { label: "Quantidade de", name: "qtyFrom", type: "number" },
              { label: "Quantidade até", name: "qtyTo", type: "number" },
              { label: "Preço de", name: "priceFrom", type: "number" },
              { label: "Preço até", name: "priceTo", type: "number" },
              {
                label: "Status",
                name: "status[]",
                type: "multiselect",
                options: [
                  { label: "Ativo", value: "active" },
                  { label: "Pausado", value: "paused" },
                  { label: "Revisão", value: "under_review" },
                ],
              },
              {
                label: "Marketplaces",
                name: "marketplaces[]",
                type: "multiselect",
                options: [
                  { label: "Mercado Livre", value: "mercadolivre" },
                  { label: "Nenhum", value: "none" },
                ],
              },
              {
                label: "Categorias",
                name: "categories[]",
                type: "multiselect",
                options:
                  this.hubStore.categories != undefined
                    ? this.hubStore.categories.map((c) => {
                        return { label: c.name, value: c.id };
                      })
                    : [],
              },
            ]}
          />
        ) : (
          <Spinner />
        )}
        {this.productStore.inProgress === true ? (
          <Spinner />
        ) : (
          <div>
            <FilterInfo
              seletedItems={this.productStore.selectedsProducts.length}
              currentPage={this.page}
              totalItems={this.productStore.productsTotal}
              totalPages={this.productStore.productsPages}
            />

            <div className="table-responsive">
              <Table className="table" hover>
                <TheadSortable
                  columns={[
                    { label: "#", name: "#" },
                    { label: "#", name: "img" },
                    { label: "SKU", name: "sku" },
                    { label: "Nome", name: "name" },
                    { label: "Qtd.", name: "qty" },
                    { label: "Preço", name: "price" },
                    { label: "Categorias", name: "categories" },
                    { label: "Anúncios", name: "announce" },
                    { label: "Status", name: "status" },
                    { label: "", name: "sync" },
                  ]}
                  getQuery={this.runQuery.bind(this)}
                />
                <tbody>
                  {this.productStore.products &&
                    this.productStore.products.map((u) => (
                      <tr key={u.id}>
                        <td>
                          <Checkbox
                            value={u.sku}
                            defaultChecked={
                              this.productStore.selectedsProducts.indexOf(
                                u.sku
                              ) != -1
                                ? true
                                : false
                            }
                            onClick={(e) => this.handler(e.target)}
                            className={
                              this.productStore.selectedsProducts.indexOf(
                                u.sku
                              ) != -1
                                ? `${s.customCheckBox}  ${s.checkedlabel}`
                                : s.customCheckBox
                            }
                          />
                        </td>
                        <td className={s.image}>
                          <img className="img-thumbnail" src={u.image} />
                        </td>
                        <td>{u.sku}</td>
                        <td>{u.name}</td>
                        <td>{u.qty}</td>
                        <td className={s.tdInline}>
                          {" "}
                          {formatBrMoney(u.price)}
                        </td>
                        <td className={s.categories}>
                          {u.categories.map(
                            (category) => " | " + category.name
                          )}
                        </td>
                        <td>{u.marketplaces}</td>
                        <td>
                          <ToogleSwitch
                            statusAlt={{
                              activated: "Pausar Anúncio",
                              disabled: "Ativar Anúncio",
                              toggleInactive: "Desconectado",
                            }}
                            value={this.handleStatus(u.status)}
                            onChange={(e) => this.handleConnection(e, u.sku)}
                            active={u.status != "desconectado"}
                          />
                          {u.status == "desconectado" ? (
                            ""
                          ) : (
                            <SpanInfo
                              color={this.getColorStatus(u.status)}
                              label={u.status}
                            />
                          )}
                        </td>
                        <td onClick={() => this.goToProduct(u.sku)}>
                          <FaSearchPlus />
                        </td>
                      </tr>
                    ))}
                </tbody>
              </Table>
              <PaginationAdvanced
                items={this.productStore.productsPages}
                maxButtons={5}
                page={this.page}
                onClick={this.onClick.bind(this)}
              />
            </div>
          </div>
        )}
      </div>
    );
  }
}
