import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Button } from "react-bootstrap";
import { history } from "../../Route/RouteStore";
import stores from "../../../stores";
import Spinner from "../../../components/Spinner";
import LabelInput from "../../../components/LabelInput";
import Images from "../../../components/Images";
import { formatBrMoney } from "../../../helpers";

const s = require("./style.scss");

interface IProps {}

interface IState {}

@observer
export default class MagentoProduct extends React.PureComponent<
  IProps,
  IState
> {
  @observable productStore = stores.productStore;
  productId = undefined;
  @observable modalSimple = false;

  componentWillMount() {
    this.productId = this.props;
    this.productId = this.productId.match.params.productId;
    if (this.productId != undefined) {
      console.log(this.productId);
      this.productStore.getProductDetails(this.productId);
    }
  }

  componentWillUnmount() {
    //this.productStore.resetMagentoSettings();
  }

  close() {
    this.modalSimple = false;
    history.push("/plataforms");
  }

  render() {
    return (
      <div>
        <h2>Detalhes do produto</h2>
        {this.productStore.inProgress ? (
          <Spinner />
        ) : (
          <form className={s.form}>
            <LabelInput
              key="name"
              label={"Nome"}
              value={this.productStore.productDetails.name}
            />
            <LabelInput
              key="sku"
              label={"SKU"}
              value={this.productStore.productDetails.sku}
            />
            <LabelInput
              key="description"
              label={"Descrição"}
              value={this.productStore.productDetails.description}
              textArea={true}
            />
            <LabelInput
              key="qty"
              label={"Quantidade"}
              value={this.productStore.productDetails.qty}
            />
            <LabelInput
              key="price"
              label={"Preço"}
              value={formatBrMoney(this.productStore.productDetails.price)}
            />
            <LabelInput
              key="status"
              label={"Status"}
              value={this.productStore.productDetails.status}
            />
            <LabelInput
              key="weight"
              label={"Peso"}
              value={this.productStore.productDetails.weight}
            />
            <Images images={this.productStore.productDetails.images} />
            {this.productStore.productDetails.attributes.map((attribute) => (
              <LabelInput
                key={attribute.key}
                label={attribute.key}
                value={attribute.value}
              />
            ))}

            <div className={s.btnGroup}>
              <Button type="button" bsStyle="default" onClick={history.goBack}>
                Voltar
              </Button>
            </div>
          </form>
        )}
      </div>
    );
  }
}
