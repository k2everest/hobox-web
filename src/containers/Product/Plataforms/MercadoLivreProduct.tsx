import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Button } from "react-bootstrap";
import { history } from "../../Route/RouteStore";
import stores from "../../../stores";
import Spinner from "../../../components/Spinner";
import LabelInput from "../../../components/LabelInput";
import { formatBrMoney } from "../../../helpers";

const s = require("./style.scss");

interface IProps {}

interface IState {}

@observer
export default class MercadoLivreProduct extends React.PureComponent<
  IProps,
  IState
> {
  @observable productStore = stores.productStore;
  productId = undefined;
  @observable modalSimple = false;

  componentWillMount() {
    this.productId = this.props;
    this.productId = this.productId.match.params.productId;
    if (this.productId != undefined) {
      console.log(this.productId);
      this.productStore.getProductMercadolivre(this.productId);
    }
  }

  close() {
    this.modalSimple = false;
    history.push("/plataforms");
  }

  render() {
    return (
      <div>
        <h2>Produto no Mercado livre</h2>
        {this.productStore.inProgress ? (
          <Spinner />
        ) : this.productStore.productMercadolivre ? (
          <form className={s.form}>
            <LabelInput
              key="title"
              label={"Titulo"}
              value={this.productStore.productMercadolivre.title}
            />
            <LabelInput
              key="id"
              label={"ID"}
              value={this.productStore.productMercadolivre.id}
            />
            <LabelInput
              key="sku"
              label={"SKU"}
              value={this.productStore.productMercadolivre.seller_custom_field}
            />
            <LabelInput
              key="category_id"
              label={"ID de Categoria"}
              value={this.productStore.productMercadolivre.category_id}
            />
            <LabelInput
              key="qty"
              label={"Quantidade"}
              value={this.productStore.productMercadolivre.available_quantity}
            />
            <LabelInput
              key="price"
              label={"Preço"}
              value={formatBrMoney(this.productStore.productMercadolivre.price)}
            />
            <LabelInput
              key="status"
              label={"Status"}
              value={this.productStore.productMercadolivre.status}
            />
            <LabelInput
              key="listing_type_id"
              label={"Tipo de Anúncio"}
              value={this.productStore.productMercadolivre.listing_type_id}
            />
            <LabelInput
              key="permalink"
              label={"Link"}
              value={this.productStore.productMercadolivre.permalink}
            />

            <div className={s.btnGroup}>
              <Button type="button" bsStyle="default" onClick={history.goBack}>
                Voltar
              </Button>
            </div>
          </form>
        ) : (
          <label>Produto não está no Marketplace</label>
        )}
      </div>
    );
  }
}
