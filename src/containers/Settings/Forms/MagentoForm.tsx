import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { history } from "../../Route/RouteStore";
import stores from "../../../stores";
import Spinner from "../../../components/Spinner";
import ProgressModal from "../../../components/Modal/Progress";

import Form from "../../../components/Form";

interface IMagentoFormProps {}

interface IMagentoFormState {}

@observer
export default class MagentoForm extends React.PureComponent<
  IMagentoFormProps,
  IMagentoFormState
> {
  @observable plataformStore = stores.plataformStore;
  plataformId = undefined;
  @observable modalSimple = false;
  @observable submited = false;

  @observable errorExist = {
    url: null,
    urlApi: null,
    username: null,
    password: null,
  };

  componentWillMount() {
    this.plataformId = this.props;
    this.plataformId = this.plataformId.match.params.plataformId;
    this.plataformStore.magentoSettings.url_api = "/index.php/api/xmlrpc/";
    if (this.plataformId != "new" && this.plataformId != undefined) {
      this.plataformStore.getMagento(this.plataformId);
    }
  }

  componentWillUpdate() {
    if (this.submited == true) this.validateForm();
  }

  componentWillUnmount() {
    this.plataformStore.resetMagentoSettings();
  }

  validateForm() {
    this.errorExist.url = this.isEmpty(this.plataformStore.magentoSettings.url);
    this.errorExist.urlApi = this.isEmpty(
      this.plataformStore.magentoSettings.url_api
    );
    this.errorExist.username = this.isEmpty(
      this.plataformStore.magentoSettings.username
    );
    this.errorExist.password = this.isEmpty(
      this.plataformStore.magentoSettings.password
    );
  }

  isEmpty(value) {
    const length = value.length;
    if (length > 1) return "success";
    else if (this.submited && length == 0) return "error";
    return null;
  }

  close() {
    this.modalSimple = false;
    history.push("/plataforms");
  }

  onSubmit = (e: boolean) => {
    this.submited = true;
    if (e == true) {
      this.validateForm();
      const checkErrors = !Object.keys(this.errorExist).some(
        (x) => this.errorExist[x] == "error"
      );
      if (checkErrors) {
        this.plataformStore.setMagento({
          settings: this.plataformStore.magentoSettings,
          id: this.plataformId == "new" ? null : this.plataformId,
          plataform: "magento",
        });
        this.modalSimple = true;
      }
    }
  };

  render() {
    return (
      <div>
        <h2>Cadastrar plataforma Magento</h2>

        <ProgressModal
          errorMessage={this.plataformStore.errors}
          title={"Processando Requisição"}
          context={this}
          show={this.plataformStore.done && this.modalSimple}
          message={
            this.plataformStore.responseMessage
              ? this.plataformStore.responseMessage
              : []
          }
          onClick={this.close.bind(this)}
          isFinished={this.plataformStore.isFinished}
        />
        {this.plataformStore.inProgress ? (
          <Spinner />
        ) : (
          <Form
            onSubmit={this.onSubmit}
            fields={[
              {
                label: "URL da Loja",
                type: "text",
                placeholder: "Ex: www.loja.com.br",
                stateValidate: this.errorExist.url,
                errorMessage: "Deve informar a URL",
                defaultValue: this.plataformStore.magentoSettings.url,
                onChange: (value) =>
                  (this.plataformStore.magentoSettings.url = value),
              },
              {
                label: "URL da Api",
                type: "text",
                placeholder: "Ex: /index.php/api/xmlrpc/",
                stateValidate: this.errorExist.urlApi,
                errorMessage: "Deve informar a URL da API",
                defaultValue: this.plataformStore.magentoSettings.url_api,
                onChange: (value) =>
                  (this.plataformStore.magentoSettings.url_api = value),
              },
              {
                label: "Usuário",
                type: "text",
                placeholder: "Usuário da api",
                stateValidate: this.errorExist.username,
                errorMessage: "Deve informar o usuário da api",
                defaultValue: this.plataformStore.magentoSettings.username,
                onChange: (value) =>
                  (this.plataformStore.magentoSettings.username = value),
              },
              {
                label: "Senha",
                type: "password",
                placeholder: "senha da api",
                stateValidate: this.errorExist.password,
                errorMessage: "Deve informar a senha de acesso à api",
                defaultValue: this.plataformStore.magentoSettings.password,
                onChange: (value) =>
                  (this.plataformStore.magentoSettings.password = value),
              },
              {
                label: "Loja(Store)",
                type: "text",
                placeholder: "ex: default",
                defaultValue: this.plataformStore.magentoSettings.store,
                onChange: (value) =>
                  (this.plataformStore.magentoSettings.store = value),
              },
            ]}
          />
        )}
      </div>
    );
  }
}
