import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { history } from "../../Route/RouteStore";
import stores from "../../../stores";
import Spinner from "../../../components/Spinner";
import ProgressModal from "../../../components/Modal/Progress";

import Form from "../../../components/Form";

interface IFormProps {}

interface IFormState {}

@observer
export default class BlingForm extends React.PureComponent<
  IFormProps,
  IFormState
> {
  @observable plataformStore = stores.plataformStore;
  plataformId = undefined;
  @observable modalSimple = false;
  @observable submited = false;
  platforms = [
    { label: "Magento", value: "Magento" },
    { label: "Skyhub", value: "Skyhub" },
  ];

  @observable errorExist = {
    apiKey: null,
    syncOrdersFrom: null,
  };

  componentWillMount() {
    console.log("wiill mount");
    this.plataformId = this.props;
    this.plataformId = this.plataformId.match.params.plataformId;
    if (this.plataformId != "new" && this.plataformId != undefined) {
      this.plataformStore.getBling(this.plataformId);
    }
  }

  componentWillUpdate() {
    if (this.submited == true) this.validateForm();
  }

  componentWillUnmount() {
    this.plataformStore.resetBlingSettings();
  }

  validateForm() {
    this.errorExist.apiKey = this.isEmpty(
      this.plataformStore.blingSettings.apiKey
    );
  }

  isEmpty(value) {
    const length = value.length;
    if (length > 1) return "success";
    else if (this.submited && length == 0) return "error";
    return null;
  }

  close() {
    this.modalSimple = false;
    history.push("/plataforms");
  }

  onSubmit = (e: boolean) => {
    this.submited = true;
    if (e == true) {
      this.validateForm();
      const checkErrors = !Object.keys(this.errorExist).some(
        (x) => this.errorExist[x] == "error"
      );
      if (checkErrors) {
        this.plataformStore.setBling({
          settings: this.plataformStore.blingSettings,
          id: this.plataformId == "new" ? null : this.plataformId,
          plataform: "bling",
        });
        this.modalSimple = true;
      }
    }
  };

  render() {
    return (
      <div>
        <h2>Cadastrar plataforma Bling</h2>
        {console.log(this.plataformStore.blingSettings)}

        <ProgressModal
          errorMessage={this.plataformStore.errors}
          title={"Processando Requisição"}
          context={this}
          show={this.plataformStore.done && this.modalSimple}
          message={
            this.plataformStore.responseMessage
              ? this.plataformStore.responseMessage
              : []
          }
          onClick={this.close.bind(this)}
          isFinished={this.plataformStore.isFinished}
        />
        {this.plataformStore.inProgress ? (
          <Spinner />
        ) : (
          <Form
            onSubmit={this.onSubmit}
            fields={[
              {
                label: "Api Key",
                type: "text",
                placeholder: "",
                stateValidate: this.errorExist.apiKey,
                errorMessage: "Deve informar o Api key",
                defaultValue: this.plataformStore.blingSettings.apiKey,
                onChange: (value) =>
                  (this.plataformStore.blingSettings.apiKey = value),
              },
              {
                label: "Data Iníco de Sincronização",
                type: "date",
                placeholder: this.plataformStore.blingSettings.syncOrdersFrom,
                stateValidate: this.errorExist.syncOrdersFrom,
                errorMessage: "Deve informar a Data de início",
                defaultValue: this.plataformStore.blingSettings.syncOrdersFrom,
                onChange: (value) =>
                  (this.plataformStore.blingSettings.syncOrdersFrom = value),
              },
              {
                label: "Não Sincronizar com plataformas:",
                type: "multiSelect",
                placeholder: "",
                stateValidate: this.errorExist.apiKey,
                errorMessage: "",
                defaultValue: this.plataformStore.blingSettings.platforms,
                optionsSelect: this.platforms,
                onChange: (value) =>
                  (this.plataformStore.blingSettings.platforms = value.map(
                    (v) => v.value
                  )),
              },
            ]}
          />
        )}
      </div>
    );
  }
}
