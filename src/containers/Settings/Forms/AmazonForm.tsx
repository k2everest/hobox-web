import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { history } from "../../Route/RouteStore";
import stores from "../../../stores";
import Spinner from "../../../components/Spinner";
import ProgressModal from "../../../components/Modal/Progress";

import Form from "../../../components/Form";

interface IFormProps {}

interface IFormState {}

@observer
export default class AmazonForm extends React.PureComponent<
  IFormProps,
  IFormState
> {
  @observable plataformStore = stores.plataformStore;
  plataformId = undefined;
  @observable modalSimple = false;
  @observable submited = false;

  @observable errorExist = {
    sellerId: null,
    accessKeyId: null,
    secretKey: null,
  };

  componentWillMount() {
    console.log("wiill mount");
    this.plataformId = this.props;
    this.plataformId = this.plataformId.match.params.plataformId;
    if (this.plataformId != "new" && this.plataformId != undefined) {
      this.plataformStore.getAmazon(this.plataformId);
    }
  }

  componentWillUpdate() {
    if (this.submited == true) this.validateForm();
  }

  componentWillUnmount() {
    this.plataformStore.resetAmazonSettings();
  }

  validateForm() {
    this.errorExist.sellerId = this.isEmpty(
      this.plataformStore.amazonSettings.sellerId
    );
    this.errorExist.accessKeyId = this.isEmpty(
      this.plataformStore.amazonSettings.accessKeyId
    );
    this.errorExist.secretKey = this.isEmpty(
      this.plataformStore.amazonSettings.secretKey
    );
  }

  isEmpty(value) {
    const length = value.length;
    if (length > 1) return "success";
    else if (this.submited && length == 0) return "error";
    return null;
  }

  close() {
    this.modalSimple = false;
    history.push("/plataforms");
  }

  onSubmit = (e: boolean) => {
    this.submited = true;
    if (e == true) {
      this.validateForm();
      const checkErrors = !Object.keys(this.errorExist).some(
        (x) => this.errorExist[x] == "error"
      );
      if (checkErrors) {
        this.plataformStore.setAmazon({
          settings: this.plataformStore.amazonSettings,
          id: this.plataformId == "new" ? null : this.plataformId,
          plataform: "amazon",
        });
        this.modalSimple = true;
      }
    }
  };

  render() {
    return (
      <div>
        <h2>Cadastrar plataforma Amazon</h2>

        <ProgressModal
          errorMessage={this.plataformStore.errors}
          title={"Processando Requisição"}
          context={this}
          show={this.plataformStore.done && this.modalSimple}
          message={
            this.plataformStore.responseMessage
              ? this.plataformStore.responseMessage
              : []
          }
          onClick={this.close.bind(this)}
          isFinished={this.plataformStore.isFinished}
        />
        {this.plataformStore.inProgress ? (
          <Spinner />
        ) : (
          <Form
            onSubmit={this.onSubmit}
            fields={[
              {
                label: "Seller ID",
                type: "text",
                placeholder: "",
                stateValidate: this.errorExist.sellerId,
                errorMessage: "Deve informar o Seller Id",
                defaultValue: this.plataformStore.amazonSettings.sellerId,
                onChange: (value) =>
                  (this.plataformStore.amazonSettings.sellerId = value),
              },
              {
                label: "Access Key ID",
                type: "text",
                placeholder: "",
                stateValidate: this.errorExist.accessKeyId,
                errorMessage: "Deve informar a Access Key ID",
                defaultValue: this.plataformStore.amazonSettings.accessKeyId,
                onChange: (value) =>
                  (this.plataformStore.amazonSettings.accessKeyId = value),
              },
              {
                label: "Secret Key",
                type: "text",
                placeholder: "",
                stateValidate: this.errorExist.secretKey,
                errorMessage: "Deve informar o secret key",
                defaultValue: this.plataformStore.amazonSettings.secretKey,
                onChange: (value) =>
                  (this.plataformStore.amazonSettings.secretKey = value),
              },
            ]}
          />
        )}
      </div>
    );
  }
}
