import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Button } from "react-bootstrap";
import { history } from "../../Route/RouteStore";
import stores from "../../../stores";
import Spinner from "../../../components/Spinner";
import SimpleModal from "../../../components/Modal/Simple";
const DatePicker = require("react-datepicker");
import "react-datepicker/dist/react-datepicker.css";
import * as moment from "moment";

const s = require("./style.scss");

interface Props {}

interface States {}

@observer
export default class MercadoLivreForm extends React.Component<Props, States> {
  @observable plataformStore = stores.plataformStore;
  @observable params = undefined;
  @observable modalSimple = false;
  @observable teste = false;
  @observable startDate = null;

  componentWillMount() {
    this.params = this.props;
    this.params = this.params.match.params;
    if (
      this.params &&
      this.params.plataformId != "new" &&
      this.params.plataformId != undefined
    ) {
      if (this.params.email) {
        const settings = {
          id: this.params.plataformId,
          nickname: this.params.nickname,
          email: this.params.email,
        };
        this.plataformStore._setMercadoLivre(settings);
      } else {
        this.plataformStore.getMercadoLivre(this.params.plataformId);
      }
    }
  }

  componentWillUnmount() {
    this.plataformStore.resetMercadoLivreSettings();
  }

  close() {
    this.modalSimple = false;
    history.push("/plataforms");
  }

  autorizar() {
    this.plataformStore.authMercadoLivre();
  }

  confirm() {
    this.modalSimple = true;
    this.plataformStore.setMercadoLivre({
      settings: this.plataformStore.mercadoLivreSettings,
      _id: this.params.plataformId,
      plataform: "mercadolivre",
    });
  }
  changeDate(date) {
    this.plataformStore.mercadoLivreSettings.syncOrdersFrom = date;
    this.startDate = date;
  }

  render() {
    return this.plataformStore.mercadoLivreSettings != null &&
      this.plataformStore.mercadoLivreSettings.email != "" ? (
      <div>
        <SimpleModal
          context={this}
          show={this.plataformStore.done && this.modalSimple}
          message={
            this.plataformStore.errors
              ? this.plataformStore.errors
              : "Dados salvos com sucesso"
          }
          onClick={this.close.bind(this)}
        />
        <h2>Confirmar Cadastro de mercado Livre</h2>
        {this.plataformStore.inProgress ? (
          <Spinner />
        ) : (
          <form className={s.form}>
            <label>
              Para concluir a inclusão de sua conta do MercadoLivre no Hobox,
              verifique os dados da conta e clique no botão salvar
            </label>
            <div className={s.field}>
              <label>Nome abreviado</label>
              <input
                type="text"
                disabled
                value={this.plataformStore.mercadoLivreSettings.nickname}
              />
            </div>

            <div className={s.field}>
              <label>E-mail</label>
              <input
                type="text"
                disabled
                value={this.plataformStore.mercadoLivreSettings.email}
              />
            </div>
            <div className={s.field}>
              <label>Início dos pedidos</label>
              <DatePicker
                selected={
                  this.startDate
                    ? this.startDate
                    : moment(
                        this.plataformStore.mercadoLivreSettings.syncOrdersFrom
                      )
                }
                onChange={this.changeDate.bind(this)}
                locale={"pt-BR"}
              />
            </div>

            <div className={s.message}>
              Para conectar o Hobox a outra conta do MercadoLivre, clique{" "}
              <a onClick={this.autorizar.bind(this)}>aqui</a>
            </div>

            <div className={s.btnGroup}>
              <Button type="button" bsStyle="default" onClick={history.goBack}>
                Voltar
              </Button>
              <Button
                type="button"
                bsStyle="primary"
                onClick={this.confirm.bind(this)}
              >
                Salvar
              </Button>
            </div>
          </form>
        )}
      </div>
    ) : (
      <div>
        <h2>Cadastrar plataforma Mercado livre</h2>
        <label>
          Para conectar o Hobox a sua conta do MercadoLivre, clique no botão
          autorizar
        </label>
        <div className={s.btnGroup}>
          <Button type="button" bsStyle="default" onClick={history.goBack}>
            Voltar
          </Button>
          <Button
            type="button"
            bsStyle="primary"
            onClick={this.autorizar.bind(this)}
          >
            Autorizar
          </Button>
        </div>
      </div>
    );
  }
}
