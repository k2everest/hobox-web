import * as React from "react";
import { observable } from "mobx";
import { inject, observer } from "mobx-react";
import { Button, ButtonGroup } from "react-bootstrap";
import stores from "../../../stores";
import Spinner from "../../../components/Spinner";
import CategoriesMercadolivre from "./Categories/Mercadolivre";
import CategoriesAmazon from "./Categories/Amazon";
import CategoriesList from "../../../components/Categories/List";

const s = require("./style.scss");

interface IProps {
  categoryStore?: any;
}

interface IState {}

@inject("categoryStore")
@observer
export default class Categories extends React.Component<IProps, IState> {
  plataformStore = stores.plataformStore;
  categoryStore = this.props.categoryStore;
  plataformId = undefined;
  @observable scrollY = "0";
  mappedCategories = [];
  categories = [];

  handleScroll(event) {
    let lastScrollY = event.target.scrollTop;
    if (window.innerWidth > 991) this.scrollY = lastScrollY + "px";
  }

  componentWillMount() {
    this.plataformId = this.props;
    this.plataformId = this.plataformId.match.params.plataformId;
    this.categoryStore.getCategories();
    if (this.plataformId != undefined) {
      this.categoryStore.getMappedCategories(this.plataformId);
    }
  }

  clickCategories() {
    this.categoryStore.isMappedCategories = false;
  }

  clickMapped() {
    this.categoryStore.getMappedCategories(this.plataformId);
    this.categoryStore.isMappedCategories = true;
  }

  disassociate(mappedCategory) {
    this.categoryStore.setMappedCategory({
      hobox: mappedCategory.id as string,
      marketplace: undefined,
      marketplaceName: undefined,
    });
  }

  ModalBox = (props: { isOpened; platform }) => {
    return (
      <div>
        {
          {
            mercadolivre: <CategoriesMercadolivre />,
            amazon: <CategoriesAmazon />,
          }[props.platform]
        }
      </div>
    );
  };

  Header = () => {
    return (
      <div>
        <h2>Mapeamento de Categorias</h2>
        <h2>
          <input
            className={s.search}
            placeholder={"Buscar..."}
            type="text"
            defaultValue={this.categoryStore.queryCategory}
            onChange={(e) =>
              (this.categoryStore.queryCategory = e.target.value)
            }
          />
          <ButtonGroup className={s.groupButton}>
            <Button
              bsStyle={
                !this.categoryStore.isMappedCategories ? "success" : "default"
              }
              onClick={this.clickCategories.bind(this)}
            >
              {"A Mapear"}
            </Button>
            <Button
              bsStyle={
                this.categoryStore.isMappedCategories ? "success" : "default"
              }
              onClick={this.clickMapped.bind(this)}
            >
              {"Mapeadas"}
            </Button>
          </ButtonGroup>
        </h2>
      </div>
    );
  };

  render() {
    return this.plataformStore.platform.name == "bling" ? (
      <div>Recurso Não Disponivel</div>
    ) : (
      <div>
        <this.Header />
        <this.ModalBox
          platform={this.plataformStore.platform.name}
          isOpened={this.props.categoryStore.isOptionOpened}
        />

        {this.categoryStore.inProgress ||
        !this.props.categoryStore.categories ? (
          <Spinner />
        ) : (
          <form className={s.form}>
            <CategoriesList
              categories={this.categories}
              disassociate={this.disassociate.bind(this)}
              filteredCategories={this.categoryStore.filteredCategories}
              isMappedCategories={this.categoryStore.isMappedCategories}
              mappedCategories={this.categoryStore.mappedCategories}
              marketplaceName={stores.plataformStore.PlatformLabel}
              openOption={(c) => this.props.categoryStore.openModal(c)}
            />
          </form>
        )}
      </div>
    );
  }
}
