import * as React from "react";
import { observer } from "mobx-react";
import { Route, Switch } from "react-router";
import { observable } from "mobx";
import {
  PLATAFORM_ORDERSTATUS,
  MERCADO_LIVRE_CATEGORIES,
  MERCADO_LIVRE_ATTRIBUTES,
  PLATAFORM_ATTRIBUTESTYPES,
  history,
} from "../../Route/RouteStore";
import SimpleModal from "../../../components/Modal/Simple";
import stores from "../../../stores";

//components
import ControlledTabs from "../../../components/ControlledTabs";

//containers
import StatusOrder from "./StatusOrder";
import Categories from "./Categories";
import Attributes from "./Attributes";
import AttributesTypes from "./AttributeTypes";

//style
const s = require("./style.scss");

interface IProps {
  route: Route;
}

interface IState {}

@observer
export default class ProductDetails extends React.Component<IProps, IState> {
  @observable error: string = "";
  @observable plataformStore = stores.plataformStore;
  @observable plataformId: any;
  constructor(props: IProps, context: any) {
    super(props, context);
  }

  componentDidMount() {
    this.plataformId = this.props;
    this.plataformId = this.plataformId.match.params.plataformId;
    if (this.plataformId != undefined) {
      console.log(this.plataformId);
      this.plataformStore.getPlataform(this.plataformId);
    }
  }

  close() {
    this.plataformStore.modalSimple = false;
  }

  createLink(parameter: string) {
    return parameter.replace(":plataformId", this.plataformId);
  }

  render() {
    return (
      <div className={s.content}>
        {console.log(history.location.pathname)}

        <SimpleModal
          context={this}
          show={this.plataformStore.done && this.plataformStore.modalSimple}
          message={
            this.plataformStore.errors
              ? this.plataformStore.errors.message
              : "Dados salvos com sucesso"
          }
          onClick={this.close.bind(this)}
        />

        <h2>Mapeamentos</h2>
        <ControlledTabs
          activeTab={history.location.pathname}
          tab={[
            {
              title: "Status de Pedidos",
              key: this.createLink(PLATAFORM_ORDERSTATUS),
            },
            {
              title: "Atributos",
              key: this.createLink(MERCADO_LIVRE_ATTRIBUTES),
            },
            {
              title: "Categorias",
              key: this.createLink(MERCADO_LIVRE_CATEGORIES),
            },
            {
              title: "Unidades de Atributos",
              key: this.createLink(PLATAFORM_ATTRIBUTESTYPES),
            },
          ]}
        />
        {stores.plataformStore.platform ? (
          <Switch>
            <Route
              path={PLATAFORM_ORDERSTATUS}
              render={(props) => <StatusOrder {...props} />}
            />
            <Route
              path={MERCADO_LIVRE_ATTRIBUTES}
              render={(props) => <Attributes {...props} />}
            />
            <Route
              path={MERCADO_LIVRE_CATEGORIES}
              render={(props) => <Categories {...props} />}
            />
            <Route
              path={PLATAFORM_ATTRIBUTESTYPES}
              render={(props) => <AttributesTypes {...props} />}
            />
          </Switch>
        ) : (
          "carregando..."
        )}
      </div>
    );
  }
}
