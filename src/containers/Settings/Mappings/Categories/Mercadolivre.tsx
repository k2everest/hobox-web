import { inject } from "mobx-react";
import * as React from "react";
import ModalCategories from "./Modal";
import { observer } from "mobx-react";

interface IProps {
  categoryStore?: any;
}

interface IState {}

@inject("categoryStore")
@observer
export default class CategoriesMercadolivre extends React.Component<
  IProps,
  IState
> {
  componentDidMount() {
    this.props.categoryStore.getCategoryMercadolivre(0);
  }

  shouldComponentUpdate() {
    return false;
  }

  componentDidUpdate() {
    if (
      !this.props.categoryStore.data &&
      this.props.categoryStore.isModalOpened
    )
      this.props.categoryStore.getCategoryMercadolivre(0);
  }

  handleChange = (e, data) => {
    console.log("action", e);
    console.log("data", data);
    const selected = {
      id: data.selected[0],
      path: data.instance.get_path(data.node, "/"),
    };
    this.props.categoryStore.setMarketplaceCategorySelected(selected);
    this.props.categoryStore.getCategoryMercadolivre(data.selected[0]);
  };

  save() {
    this.props.categoryStore.setMappedCategory({
      hobox: this.props.categoryStore.hoboxCategorySelected.id as string,
      marketplace: this.props.categoryStore.marketplaceCategorySelected.id,
      marketplaceName: this.props.categoryStore.marketplaceCategorySelected
        .path,
    });
  }

  render() {
    return (
      <div>
        {this.props.categoryStore.isModalOpened ? (
          <ModalCategories
            save={this.save.bind(this)}
            handleChangeTreeView={this.handleChange}
            platformName={"Mercado Livre"}
          />
        ) : (
          ""
        )}
      </div>
    );
  }
}
