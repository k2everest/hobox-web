import { inject } from "mobx-react";
import * as React from "react";
import ModalCategories from "./Modal";
import { observer } from "mobx-react";

interface IProps {
  categoryStore?: any;
}

interface IState {}

@inject("categoryStore")
@observer
export default class CategoriesAmazon extends React.Component<IProps, IState> {
  componentDidMount() {
    this.props.categoryStore.getAmazonCategories();
  }

  componentDidUpdate() {
    if (
      !this.props.categoryStore.data &&
      this.props.categoryStore.isModalOpened
    )
      this.props.categoryStore.getAmazonCategories();
  }

  handleChange = (e, data) => {
    console.log("action", e);
    const selected = {
      id: data.instance.get_path(data.node, "/"),
      path: data.instance.get_path(data.node, "/"),
    };

    this.props.categoryStore.setMarketplaceCategorySelected(selected);
  };

  save() {
    this.props.categoryStore.setMappedCategory({
      hobox: this.props.categoryStore.hoboxCategorySelected.id as string,
      marketplace: this.props.categoryStore.marketplaceCategorySelected.id,
      marketplaceName: this.props.categoryStore.marketplaceCategorySelected
        .path,
    });
  }

  render() {
    return (
      <div>
        {this.props.categoryStore.isModalOpened ? (
          <ModalCategories
            save={this.save.bind(this)}
            handleChangeTreeView={this.handleChange}
            platformName={"Amazon"}
          />
        ) : (
          ""
        )}
      </div>
    );
  }
}
