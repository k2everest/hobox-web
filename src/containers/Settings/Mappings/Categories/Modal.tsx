import * as React from "react";
import { Row, Col, Grid, Modal } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { observable } from "mobx";
import { observer, inject } from "mobx-react";
import TreeView from "react-simple-jstree";
import { toJS } from "mobx";

interface IProps {
  categoryStore?: any;
  plataformStore?: any;
  platformName: string;
  save: () => any;
  handleChangeTreeView?(e, data): void;
}

interface IState {}

@inject("categoryStore", "plataformStore")
@observer
export default class ModalCategories extends React.Component<IProps, IState> {
  @observable scrollY = "0";

  handleScroll(event) {
    let lastScrollY = event.target.scrollTop;
    if (window.innerWidth > 991) this.scrollY = lastScrollY + "px";
  }

  shouldComponentUpdate() {
    return false;
  }

  render() {
    let data = {
      core: {
        data: this.props.categoryStore.data
          ? toJS(this.props.categoryStore.data)
          : [],
      },
    };

    return (
      <div>
        {this.props.categoryStore.data != undefined ? (
          <Modal
            show={true}
            onHide={() => {}}
            onScroll={this.handleScroll.bind(this)}
          >
            <Modal.Header>
              <Modal.Title>Mapeamento de Categoria</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Grid fluid={true}>
                <Row className="show-grid">
                  <Col xs={12} md={6} style={{ marginTop: this.scrollY }}>
                    <h4>
                      {this.props.categoryStore.hoboxCategorySelected.name}
                    </h4>
                    <label>
                      {" "}
                      Código:{" "}
                      {this.props.categoryStore.hoboxCategorySelected.id}
                    </label>
                    <hr />
                    <h3> {this.props.platformName}</h3>
                    {this.props.categoryStore.selectedCategory ? (
                      <div>
                        <h4>
                          {this.props.categoryStore.selectedCategory.path}
                        </h4>
                        <label>
                          Código:
                          <span>
                            {" "}
                            {this.props.categoryStore.selectedCategory.id}
                          </span>
                        </label>
                      </div>
                    ) : (
                      "Selecione uma categoria"
                    )}
                    <Modal.Footer>
                      <Button
                        bsStyle="default"
                        onClick={() => this.props.categoryStore.closeModal()}
                      >
                        Cancelar
                      </Button>
                      <Button
                        bsStyle="primary"
                        onClick={() => this.props.save()}
                      >
                        Salvar
                      </Button>
                    </Modal.Footer>
                  </Col>
                  <Col xs={6} md={6}>
                    <TreeView
                      treeData={data}
                      onChange={this.props.handleChangeTreeView}
                    />
                  </Col>
                </Row>
              </Grid>
            </Modal.Body>
          </Modal>
        ) : (
          ""
        )}
      </div>
    );
  }
}
