import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Button } from "react-bootstrap";
import { history } from "../../Route/RouteStore";
import { plataformStore } from "../../../stores/rootStore";
import Spinner from "../../../components/Spinner";
import Select from "../../../components/Select";
import { Row, Col, Grid } from "react-bootstrap";
const s = require("./style.scss");

interface IProps {}

interface IState {}

@observer
export default class StatusOrder extends React.PureComponent<IProps, IState> {
  @observable plataformStore = plataformStore;
  plataformId = undefined;
  @observable modalSimple = false;

  componentWillMount() {
    this.plataformId = this.props;
    this.plataformId = this.plataformId.match.params.plataformId;
    if (this.plataformId != undefined) {
      this.plataformStore.getOrderStatus(this.plataformId);
    }
  }

  changeOrderStatus(index, value) {
    this.plataformStore.mappedOrderStatus[index].hobox = value;
  }

  save() {
    this.plataformStore.setOrderStatus(
      this.plataformId,
      this.plataformStore.mappedOrderStatus
    );
  }

  render() {
    return (
      <div>
        <h2>Mapeamento de Status de Pedidos</h2>
        {this.plataformStore.inProgress ? (
          <Spinner />
        ) : (
          <form className={s.form}>
            <Grid fluid={true}>
              <Row className="show-grid">
                <Col xs={6} md={5}>
                  <h2>{this.plataformStore.PlatformLabel}</h2>
                </Col>
                <Col xs={6} md={5}>
                  <h2>Hobox</h2>
                </Col>
              </Row>
              {this.plataformStore.mappedOrderStatus.map((attribute, index) => (
                <div className={s.field} key={index}>
                  <Row className="show-grid">
                    <Col xs={6} md={5}>
                      <label>{attribute.status}</label>
                    </Col>
                    <Col xs={6} md={5}>
                      <Select
                        externalKey={"select" + index}
                        options={this.plataformStore.hoboxStatus}
                        placeholder={attribute.hobox}
                        value={attribute.hobox}
                        onChange={(value) =>
                          this.changeOrderStatus(index, value)
                        }
                      />
                    </Col>
                  </Row>
                </div>
              ))}

              <div className={s.btnGroup}>
                <Button
                  type="button"
                  bsStyle="default"
                  onClick={history.goBack}
                >
                  Voltar
                </Button>
                <Button
                  type="button"
                  bsStyle="primary"
                  onClick={this.save.bind(this)}
                >
                  Salvar
                </Button>
              </div>
            </Grid>
          </form>
        )}
      </div>
    );
  }
}
