import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Button, Modal } from "react-bootstrap";
import stores from "../../../stores";
import Spinner from "../../../components/Spinner";
import Select from "../../../components/Select";
import { Row, Col, Grid } from "react-bootstrap";

const s = require("./style.scss");

interface IProps {
  data: {
    optionsMarketplace: {
      label: string;
      value: string;
    }[];
    optionsHobox: {
      label: any;
      value: any;
    }[];
    attributeName: string;
    attributeIndex: any;
  };

  show: boolean;
}

interface IState {}

@observer
export default class Options extends React.PureComponent<IProps, IState> {
  @observable plataformStore = stores.plataformStore;

  componentDidMount() {
    console.log("atributo name", this.props.data.attributeName);
    console.log("hobox", this.props.data.optionsHobox);
    console.log("mk", this.props.data.optionsMarketplace);
  }

  changeOptionValue(index, value) {
    this.plataformStore.mappedAttributes[
      this.props.data.attributeIndex
    ].options[index].ml = value;
  }

  close() {
    this.plataformStore.isOptionOpened = false;
  }

  render() {
    return (
      <Grid fluid={true}>
        <Modal show={true} onHide={() => {}}>
          <Modal.Header>
            <Modal.Title>
              Mapeamento de opções do atributo {this.props.data.attributeName}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.props == undefined ? (
              <Spinner />
            ) : (
              <form className={s.form}>
                <Row className="show-grid">
                  <Col xs={5} md={5}>
                    <h2>Opções do Hobox</h2>
                  </Col>
                  <Col xs={5} md={5}>
                    <h2>Opções do Mercado Livre</h2>
                  </Col>
                </Row>
                {this.props.data.optionsHobox.map((option, index) => (
                  <div className={s.field} key={index}>
                    <Row className="show-grid">
                      <Col xs={5} md={5}>
                        <label>{option.label}</label>
                      </Col>
                      <Col xs={5} md={5}>
                        <Select
                          externalKey={"select" + index}
                          options={this.props.data.optionsMarketplace}
                          placeholder={"Selecione"}
                          value={
                            this.plataformStore.mappedAttributes[
                              this.props.data.attributeIndex
                            ].options[index].ml
                              ? this.plataformStore.mappedAttributes[
                                  this.props.data.attributeIndex
                                ].options[index].ml
                              : ""
                          }
                          onChange={(value) =>
                            this.changeOptionValue(index, value)
                          }
                        />
                      </Col>
                    </Row>
                  </div>
                ))}
              </form>
            )}
          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle="primary" onClick={this.close.bind(this)}>
              OK
            </Button>
          </Modal.Footer>
        </Modal>
      </Grid>
    );
  }
}
