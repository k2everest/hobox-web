import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Button } from "react-bootstrap";
import { history } from "../../Route/RouteStore";
import { plataformStore } from "../../../stores/rootStore";
import Spinner from "../../../components/Spinner";
import Select from "../../../components/Select";
import { Row, Col, Grid } from "react-bootstrap";
const s = require("./style.scss");

interface IProps {}

interface IState {}

@observer
export default class AttributeTypes extends React.PureComponent<
  IProps,
  IState
> {
  @observable plataformStore = plataformStore;
  plataformId = undefined;
  @observable modalSimple = false;

  componentWillMount() {
    this.plataformId = this.props;
    this.plataformId = this.plataformId.match.params.plataformId;
    if (this.plataformId != undefined) {
      this.plataformStore.getAttributesTypes();
    }
  }

  changeAttributeTypeValue(index, value, attributeType, baseName) {
    this.plataformStore.mappedAttributesTypes[index] = {
      baseName: baseName,
      value: value,
      attributeType: attributeType,
    };
  }

  save() {
    this.plataformStore.setAttributesTypes(
      this.plataformId,
      this.plataformStore.mappedAttributesTypes
    );
  }

  render() {
    return this.plataformStore.mappedAttributesTypes == undefined ? (
      <div>Recurso Não Disponível</div>
    ) : (
      <div>
        <h2>Mapeamento Unidades de Atributos</h2>
        {this.plataformStore.inProgress ? (
          <Spinner />
        ) : (
          <form className={s.form}>
            <Grid fluid={true}>
              <Row className="show-grid">
                <Col xs={6} md={5}>
                  <h2>Tipo de atributo</h2>
                </Col>
                <Col xs={6} md={5}>
                  <h2>Unidade</h2>
                </Col>
              </Row>
              {this.plataformStore.attributesTypes.map((attribute, index) => (
                <div className={s.field} key={index}>
                  <Row className="show-grid">
                    <Col xs={6} md={5}>
                      <label>{attribute.attributeType}</label>
                    </Col>
                    <Col xs={6} md={5}>
                      <Select
                        externalKey={"select" + index}
                        options={attribute.options}
                        placeholder={
                          this.plataformStore.mappedAttributesTypes[index] &&
                          this.plataformStore.mappedAttributesTypes[index].value
                        }
                        value={
                          this.plataformStore.mappedAttributesTypes[index] &&
                          this.plataformStore.mappedAttributesTypes[index].value
                        }
                        onChange={(value) =>
                          this.changeAttributeTypeValue(
                            index,
                            value,
                            attribute.attributeType,
                            attribute.base.name
                          )
                        }
                      />
                    </Col>
                  </Row>
                </div>
              ))}

              <div className={s.btnGroup}>
                <Button
                  type="button"
                  bsStyle="default"
                  onClick={history.goBack}
                >
                  Voltar
                </Button>
                <Button
                  type="button"
                  bsStyle="primary"
                  onClick={this.save.bind(this)}
                >
                  Salvar
                </Button>
              </div>
            </Grid>
          </form>
        )}
      </div>
    );
  }
}
