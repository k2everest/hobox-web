import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Button } from "react-bootstrap";
import { history } from "../../Route/RouteStore";
import stores from "../../../stores";
import Spinner from "../../../components/Spinner";
import { Row, Col, Grid } from "react-bootstrap";
const MapSigns = require("react-icons/lib/fa/map-signs");
import Options from "./Options";
import SelectSearch from "../../../components/SelectSearch";

const s = require("./style.scss");

interface IProps {}

interface IState {}

@observer
export default class Attributes extends React.PureComponent<IProps, IState> {
  @observable plataformStore = stores.plataformStore;
  @observable categoryStore = stores.categoryStore;
  @observable optionsAttributes = undefined;
  plataformId = undefined;
  dataOptions = undefined;
  error = false;
  async componentWillMount() {
    this.plataformId = this.props;
    this.plataformId = this.plataformId.match.params.plataformId;
    if (this.plataformId != undefined) {
      try {
        await this.plataformStore.getPlataform(this.plataformId);
        await this.plataformStore.getAttributesPlatform();
        await this.categoryStore.getCategoriesFactory(
          this.plataformStore.platform.name,
          this.plataformId
        );
        this.plataformStore.getMapppedAttributes(this.plataformId);
      } catch (err) {
        this.error = true;
      }
    }
    this.plataformStore.getAttributes();
  }

  changeAttributeValue(index, attributeMl, value) {
    //verificar se tem opções no mercado livre,se tiver deve atribuir opções do hobox na variavel mapeada
    const optionsHobox = attributeMl.options
      ? this.plataformStore.attributes.filter((a) => {
          return a.name == value;
        })[0].options
      : null;

    this.plataformStore.mappedAttributes[index] = {
      marketplaceName: attributeMl.name,
      hoboxName: value,
      options: optionsHobox,
      type: attributeMl.type,
    };
    console.log("change", this.plataformStore.mappedAttributes);
  }

  save() {
    console.log("save", this.plataformStore.mappedAttributes);
    this.plataformStore.setMappedAttributes(
      this.plataformId,
      this.plataformStore.mappedAttributes
    );
  }

  openOption(index, attribute) {
    //passo 1: passar opções do mercado livre para proxima tela
    //passo 2: as opções do hobox ja estará na variavel de mapped
    if (this.plataformStore.mappedAttributes[index].options) {
      this.dataOptions = {
        attributeName: attribute.name,
        attributeIndex: index,
        optionsHobox: this.plataformStore.mappedAttributes[index].options,
        optionsMarketplace: attribute.options,
      };

      this.plataformStore.isOptionOpened = true;
    } else this.plataformStore.isOptionOpened = false;
  }

  sortAttributes(a, b) {
    if (a.label > b.label) {
      return 1;
    }
    if (a.label < b.label) {
      return -1;
    }
    return 0;
  }

  render() {
    return this.error == true ? (
      <div>Recurso Não disponível</div>
    ) : (
      <div>
        <h2>Mapeamento de Atributos</h2>
        <SelectSearch
          name="Categoria"
          label="Categoria"
          type="select"
          placeholder="Selecione uma Categoria para mapear atributos da categoria"
          options={this.categoryStore.data ? this.categoryStore.data : []}
          key="category"
          onChange={(value) => this.plataformStore.getAttributesPlatform(value)}
        />
        {this.plataformStore.isOptionOpened ? (
          <Options
            data={this.dataOptions}
            show={this.plataformStore.isOptionOpened}
          />
        ) : null}
        {(this.plataformStore.mappedAttributes.length < 1 &&
          this.plataformStore.inProgress) ||
        this.plataformStore.attributes == undefined ||
        this.plataformStore.inProgress ? (
          <Spinner />
        ) : (
          <form className={s.form}>
            <Grid fluid={true}>
              <Row className="show-grid">
                <Col xs={5} md={5}>
                  <h2>{this.plataformStore.PlatformLabel}</h2>
                </Col>
                <Col xs={5} md={5}>
                  <h2>Hobox</h2>
                </Col>
                <Col xs={1} md={2}></Col>
              </Row>
              {this.plataformStore.attributesML.map((attribute, index) => (
                <div className={s.field} key={index}>
                  <Row className="show-grid">
                    <Col xs={5} md={5}>
                      <label>{attribute.label}</label>
                    </Col>
                    <Col xs={5} md={5}>
                      <SelectSearch
                        name=""
                        label=""
                        value={
                          this.plataformStore.mappedAttributes[index] &&
                          this.plataformStore.mappedAttributes[index].hoboxName
                            ? this.plataformStore.mappedAttributes[index]
                                .hoboxName
                            : ""
                        }
                        type="select"
                        placeholder="Selecione"
                        options={this.plataformStore.attributes
                          .filter((a) =>
                            attribute.options && attribute.options.length
                              ? a.options != null
                              : a
                          )
                          .map((a) => {
                            return {
                              label: a.label as string,
                              value: a.name as string,
                            };
                          })
                          .sort(this.sortAttributes)}
                        key={"select" + index}
                        onChange={(value) =>
                          this.changeAttributeValue(index, attribute, value)
                        }
                      />
                    </Col>
                    <Col xs={1} md={2}>
                      {attribute.options && attribute.options.length > 0 ? (
                        <Button
                          onClick={() => this.openOption(index, attribute)}
                          className="btn btn-primary"
                          title="Mapear"
                        >
                          <MapSigns />{" "}
                          <label className="hidden-mobile">Opções</label>
                        </Button>
                      ) : (
                        ""
                      )}
                    </Col>
                  </Row>
                </div>
              ))}
              <Row>
                <div className={s.btnGroup}>
                  <Button
                    type="button"
                    bsStyle="default"
                    onClick={history.goBack}
                  >
                    Voltar
                  </Button>
                  <Button
                    type="button"
                    bsStyle="primary"
                    onClick={this.save.bind(this)}
                  >
                    Salvar
                  </Button>
                </div>
              </Row>
            </Grid>
          </form>
        )}
      </div>
    );
  }
}
