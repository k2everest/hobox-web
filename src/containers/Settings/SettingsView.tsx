import * as React from "react";
import { observer } from "mobx-react";
import { Route, Switch } from "react-router";
import { plataformStore } from "../../stores/rootStore";
import { observable } from "mobx";
import {
  PLATAFORMS_MERCADO_LIVRE_CONFIRM,
  PLATAFORMS_MERCADO_LIVRE,
  PLATAFORMS_MAGENTO,
  PLATAFORMS_AMAZON,
  PLATAFORMS_BLING,
} from "../Route/RouteStore";

//components
import PlataformList from "./PlataformsList";
import MagentoForm from "../Settings/Forms/MagentoForm";
import MercadoLivreForm from "../Settings/Forms/MercadoLivreForm";
import AmazonForm from "../Settings/Forms/AmazonForm";
import BlingForm from "../Settings/Forms/BlingForm";

interface ISettingsProps {
  route: Route;
}

interface ISettingsState {}

@observer
export default class SettingsView extends React.Component<
  ISettingsProps,
  ISettingsState
> {
  @observable error: string = "";
  plataformStore = plataformStore;

  constructor(props: ISettingsProps, context: any) {
    super(props, context);
  }

  render() {
    return (
      <div>
        <Switch>
          <Route
            exact
            path={PLATAFORMS_MAGENTO}
            render={(props) => <MagentoForm {...props} />}
          />
          <Route exact path="/plataforms" render={() => <PlataformList />} />
          <Route
            exact
            path={PLATAFORMS_MERCADO_LIVRE}
            render={(props) => <MercadoLivreForm {...props} />}
          />
          <Route
            exact
            path={PLATAFORMS_MERCADO_LIVRE_CONFIRM}
            render={(props) => <MercadoLivreForm {...props} />}
          />
          <Route
            exact
            path={PLATAFORMS_AMAZON}
            render={(props) => <AmazonForm {...props} />}
          />
          <Route
            exact
            path={PLATAFORMS_BLING}
            render={(props) => <BlingForm {...props} />}
          />
        </Switch>
      </div>
    );
  }
}
