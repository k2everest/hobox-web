import * as React from "react";
import { observer } from "mobx-react";
import { observable } from "mobx";
import { plataformStore } from "../../stores/rootStore";
import { Link } from "react-router-dom";

import {
  linkToPlataformDetails,
  history,
  linkToPlataformMapping,
  linkToPlataformRules,
} from "../Route/RouteStore";
const Spinner = require("react-spinkit");
const s = require("./style.scss");
const FaEdit = require("react-icons/lib/fa/edit");
const FaTrash = require("react-icons/lib/fa/trash");
const MapSigns = require("react-icons/lib/fa/map-signs");
const OnIcon = require("react-icons/lib/md/swap-vertical-circle");
const OffIcon = require("react-icons/lib/io/alert-circled");
const FaBan = require("react-icons/lib/fa/ban");
const Rules = require("react-icons/lib/fa/cog");

import { DropdownButton, MenuItem, ButtonGroup } from "react-bootstrap";
import ConfirmModal from "../../components/Modal/Confirm";

const StatusIcon = ({ status }) => {
  return status === true ? (
    <span style={{ color: "green", paddingRight: "0.3em" }} title="Conectado">
      {" "}
      <OnIcon size={15} />
    </span>
  ) : (
    <span style={{ color: "red", paddingRight: "0.3em" }} title="Desconectado">
      {" "}
      <OffIcon size={15} />
    </span>
  );
};

interface Props {}

@observer
export default class PlataformList extends React.Component<Props, any> {
  @observable error: string = "";
  @observable plataformStore = plataformStore;
  @observable confirmModal: {
    id: any;
    show: boolean;
  } = { id: "", show: false };

  constructor(props: Props, context: any) {
    super(props, context);
  }

  componentWillMount() {
    this.plataformStore.listPlataforms();
  }

  goMagento() {
    history.push("/magento/new");
  }

  delPlataform(idPlataform) {
    this.plataformStore.delMagento(idPlataform);
    this.confirmModal.show = false;
  }

  close() {
    this.confirmModal.show = false;
  }

  click(id) {
    this.confirmModal.show = true;
    this.confirmModal.id = id;
  }

  getRuleMenuItem(u) {
    if (u.plataform == "mercadolivre")
      return (
        <MenuItem
          eventKey="3"
          className={s.customMenu}
          href={linkToPlataformRules(u._id)}
        >
          <Rules />
          Regras
        </MenuItem>
      );
  }

  getMapMenuItem(u) {
    if (u.plataform == "mercadolivre" || u.plataform == "amazon")
      return (
        <MenuItem
          eventKey="2"
          className={s.customMenu}
          href={linkToPlataformMapping(u._id)}
        >
          {" "}
          <MapSigns />
          Mapear
        </MenuItem>
      );
  }

  render() {
    return (
      <div>
        <h1>Plataformas</h1>
        <ConfirmModal
          action={this.delPlataform.bind(this, this.confirmModal.id)}
          close={this.close.bind(this)}
          context={this}
          message="Deseja realmente excluir essa plataforma ?"
          show={this.confirmModal.show}
          title={"Excluir"}
        />
        {this.plataformStore.inProgress === true ? (
          <Spinner
            name="line-scale-pulse-out"
            fadeIn="none"
            className={s.spinner}
          />
        ) : (
          <div>
            <ButtonGroup bsStyle="primary pull-right">
              <DropdownButton
                bsStyle="primary"
                title="Cadastrar plataforma"
                id="right-panel-link"
              >
                <MenuItem eventKey="1" onClick={this.goMagento}>
                  Magento
                </MenuItem>
                <MenuItem
                  eventKey="2"
                  onClick={() => history.push("/mercadolivre/new")}
                >
                  Mercado livre
                </MenuItem>
                <MenuItem
                  eventKey="3"
                  onClick={() => history.push("/amazon/new")}
                >
                  Amazon
                </MenuItem>
                <MenuItem
                  eventKey="3"
                  onClick={() => history.push("/bling/new")}
                >
                  Bling
                </MenuItem>
              </DropdownButton>
            </ButtonGroup>

            <table className="table">
              <thead>
                <tr>
                  <th>Plataforma</th>
                  <th>Referência</th>
                  <th>Ações</th>
                </tr>
              </thead>
              <tbody>
                {this.plataformStore.plataforms.map((u) => (
                  <tr key={u._id}>
                    <td>
                      <StatusIcon status={u.connected} />
                      {u.plataform}
                    </td>
                    <td>{u.url}</td>
                    <td>
                      <ButtonGroup bsStyle="primary ">
                        <DropdownButton
                          bsStyle="info"
                          title="Ações"
                          id="right-panel-link"
                        >
                          <MenuItem
                            href={linkToPlataformDetails(u._id, u.plataform)}
                            className={s.customMenu}
                            eventKey="1"
                          >
                            {" "}
                            <FaEdit />
                            Editar
                          </MenuItem>
                          {this.getMapMenuItem(u)}
                          {this.getRuleMenuItem(u)}
                          <MenuItem
                            eventKey="3"
                            className={s.customMenu}
                            onClick={this.click.bind(this, u._id)}
                          >
                            <FaTrash />
                            Excluir
                          </MenuItem>
                        </DropdownButton>
                      </ButtonGroup>
                      {/* <div className={s.buttons} >
                                            <Link to={linkToPlataformDetails(u._id, u.plataform)} className="btn btn-primary" title="Editar" >
                                                <FaEdit />
                                            </Link>
                                            {
                                              u.plataform!='magento'?
                                                <Link  to={linkToPlataformMapping(u._id)} className="btn btn-primary" title="Mapear">
                                                    <MapSigns />
                                                </Link>
                                              :
                                                <button className="btn btn-primary" title="Ação indisponível" ><FaBan /></button>
                                            }
                                            <Link  to={linkToPlataformRules(u._id)} className="btn btn-primary" title="Regras">
                                                <Rules />
                                            </Link>
                                            <button  onClick={this.click.bind(this,u._id)} className="btn btn-danger" title="Excluir">
                                                <FaTrash />
                                            </button>
                                        </div> */}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        )}
      </div>
    );
  }
}
