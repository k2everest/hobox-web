import * as React from "react";
import { observer } from "mobx-react";
import { Route, Switch } from "react-router";
import { observable } from "mobx";
import { history } from "../Route/RouteStore";

//components
import ControlledTabs from "../../components/ControlledTabs";

//containers
import OrderStatus from "./OrderStatus";
import Categories from "./Categories";
import Attributes from "./Attributes";

//style
const s = require("./style.scss");

interface IProps {
  route: Route;
}

interface IState {}

@observer
export default class ProductDetails extends React.Component<IProps, IState> {
  @observable error: string = "";
  constructor(props: IProps, context: any) {
    super(props, context);
  }

  render() {
    return (
      <div className={s.content}>
        {console.log(history.location.pathname)}

        <h1>Definições do Hobox</h1>
        <ControlledTabs
          activeTab={history.location.pathname}
          tab={[
            { title: "Status de Pedidos", key: "/status-orders" },
            { title: "Categorias", key: "/categories" },
            { title: "Atributos", key: "/attributes" },
          ]}
        />
        <Switch>
          <Route
            path={"/status-orders"}
            render={(props) => <OrderStatus {...props} />}
          />
          <Route
            path={"/categories"}
            render={(props) => <Categories {...props} />}
          />
          <Route
            path={"/attributes"}
            render={(props) => <Attributes {...props} />}
          />
        </Switch>
      </div>
    );
  }
}
