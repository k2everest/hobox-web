import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Button } from "react-bootstrap";
import { history } from "../Route/RouteStore";
import Spinner from "../../components/Spinner";
import { Row, Col, Grid } from "react-bootstrap";
import LabelInput from "../../components/LabelInput";
import SimpleModal from "../../components/Modal/Simple";

import stores from "../../stores/";
const s = require("./style.scss");

interface IProps {}

interface IState {}

const Content = ({ context }) => {
  return (
    <form className={s.form}>
      <Grid fluid={true}>
        <Row className="show-grid">
          <Col xs={12} md={6}>
            <h2>Nome</h2>
          </Col>
        </Row>
        {context.hubStore.categories.map((category, index) => (
          <div className={s.field} key={index}>
            <Row className="show-grid">
              <Col xs={12} md={6}>
                <LabelInput
                  key={category.id}
                  label={""}
                  value={category.name}
                />
              </Col>
            </Row>
          </div>
        ))}
        <div className={s.btnGroup}>
          <Button type="button" bsStyle="default" onClick={history.goBack}>
            Voltar
          </Button>
        </div>
      </Grid>
    </form>
  );
};

@observer
export default class Categories extends React.PureComponent<IProps, IState> {
  @observable hubStore = stores.hubStore;
  @observable modalSimple = false;

  componentWillMount() {
    this.hubStore.getCategories();
  }

  close() {
    this.modalSimple = false;
  }

  requestSync() {
    this.modalSimple = true;
    this.hubStore.syncCategories();
  }

  render() {
    return (
      <div>
        <h2>Categorias</h2>
        <SimpleModal
          context={this}
          show={this.hubStore.done && this.modalSimple}
          message={
            this.hubStore.errors
              ? this.hubStore.errors.message
              : "Dados salvos com sucesso"
          }
          onClick={this.close.bind(this)}
        />
        <Row className="show-grid">
          <Col xs={12} md={12}>
            <Button
              type="button"
              bsStyle="primary pull-right"
              onClick={this.requestSync.bind(this)}
            >
              Sincronizar
            </Button>
          </Col>
        </Row>
        {this.hubStore.inProgress || !this.hubStore.categories ? (
          <Spinner />
        ) : this.hubStore.categories.length > 1 ? (
          <Content context={this} />
        ) : (
          "Lista vazia"
        )}
      </div>
    );
  }
}
