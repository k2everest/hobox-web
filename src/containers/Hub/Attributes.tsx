import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Button } from "react-bootstrap";
import Spinner from "../../components/Spinner";
import { Row, Col } from "react-bootstrap";
import LabelInput from "../../components/LabelInput";
import SimpleModal from "../../components/Modal/Simple";

import { hubStore } from "../../stores/rootStore";
const s = require("./style.scss");

interface IProps {}

interface IState {}

const Content = ({ context }) => {
  return (
    <form className={s.form}>
      <Row className="show-grid">
        <Col xs={6} md={6}>
          <h4>Label</h4>
        </Col>
        <Col xs={6} md={6}>
          <h4>Nome</h4>
        </Col>
      </Row>
      {context.hubStore.attributes.map((attribute, index) => (
        <div className={s.field} key={index}>
          <Row className="show-grid">
            <Col xs={6} md={6}>
              <LabelInput
                key={attribute.label}
                label={""}
                value={attribute.label}
              />
            </Col>
            <Col xs={6} md={6}>
              <LabelInput
                key={attribute.name}
                label={""}
                value={attribute.name}
              />
            </Col>
          </Row>
        </div>
      ))}
    </form>
  );
};

@observer
export default class Attributes extends React.PureComponent<IProps, IState> {
  @observable hubStore = hubStore;
  @observable modalSimple = false;

  componentWillMount() {
    this.hubStore.getAttributes();
  }

  close() {
    this.modalSimple = false;
  }

  requestSync() {
    this.modalSimple = true;
    this.hubStore.syncAttributes();
  }

  render() {
    return (
      <div>
        <h2>Atributos</h2>
        <SimpleModal
          context={this}
          show={this.hubStore.done && this.modalSimple}
          message={
            this.hubStore.errors
              ? this.hubStore.errors.message
              : "Dados salvos com sucesso"
          }
          onClick={this.close.bind(this)}
        />
        <Row className="show-grid">
          <Col xs={12} md={12}>
            <Button
              type="button"
              bsStyle="primary pull-right"
              onClick={this.requestSync.bind(this)}
            >
              Sincronizar
            </Button>
          </Col>
        </Row>
        {this.hubStore.inProgress || !this.hubStore.attributes ? (
          <Spinner />
        ) : this.hubStore.attributes.length > 0 ? (
          <Content context={this} />
        ) : (
          " Lista vazia"
        )}
      </div>
    );
  }
}
