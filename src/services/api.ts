import * as _superagent from "superagent";
import * as superagentPromise from "superagent-promise";
import { Config } from "../containers/App/Config";
import { commonStore, authStore } from "../stores/rootStore";

const API_ROOT = Config.BASE_URL;
const superagent = superagentPromise(_superagent, global.Promise);

const tokenPlugin = (req) => {
  req.accept("application/json");
  if (commonStore.user.token) {
    req.set("x-access-token", commonStore.user.token);
  }
};

const handleErrors = (err) => {
  if (err && err.response && err.response.status === 401) {
    authStore.logout();
  } else if (err && err.response && err.response.status === 405) {
    authStore.setRecoverMode();
  }

  return err;
};

const TIMEOUT_MILLISECONDS = 2000000;

const responseBody = (res) => res.body;

const requests = {
  del: (url) =>
    superagent
      .del(`${API_ROOT}${url}`)
      .timeout(TIMEOUT_MILLISECONDS)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
  get: (url) =>
    superagent
      .get(`${API_ROOT}${url}`)
      .timeout(TIMEOUT_MILLISECONDS)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
  put: (url, body) =>
    superagent
      .put(`${API_ROOT}${url}`, body)
      .timeout(TIMEOUT_MILLISECONDS)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
  patch: (url, body) =>
    superagent
      .patch(`${API_ROOT}${url}`, body)
      .timeout(TIMEOUT_MILLISECONDS)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
  post: (url, body) =>
    superagent
      .post(`${API_ROOT}${url}`, body)
      .timeout(TIMEOUT_MILLISECONDS)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
};

const Auth = {
  forgotPassword: (email) => requests.post("/auth/forgotPassword", { email }),
  resetPassword: (fields) => requests.post("/auth/resetPassword", fields),
  current: () => requests.get("/user"),
  login: (email, password) =>
    requests.post("/login/signIn", { credentials: { email, password } }),
  authGoogle: (accessToken) =>
    requests.post("/auth/google", { access_token: accessToken }),
  register: (username, email, password) =>
    requests.post("/users", { user: { username, email, password } }),
};

const User = {
  update: (user) => requests.put("/users", { user }),
  updatePassword: (userPassword) =>
    requests.put("/users/password", { userPassword }),
  updateCompany: (company) => requests.put("/users/company", { company }),
  getCompany: () => requests.get("/users/me/company"),
  deactivateAccount: (userPassword) =>
    requests.put("/users/deactivate", { userPassword }),
  recoverAccount: (userPassword) =>
    requests.put("/users/recover", { userPassword }),
  current: () => requests.get("/users/me"),
  list: () => requests.get("/users/all"),
  register: (user: { name; lastname; email; password }) =>
    requests.post("/users", { user }),
};

const Order = {
  list: (page, query) => requests.get(`/orders/?page=${page}&${query}`),
  getOrder: (orderId) => requests.get(`/orders/${orderId}`),
  getCart: (packId) => requests.get(`/orders/?packId=${packId}`),
  getOrdersSummary: () => requests.get(`/orders/reports/summary`),
  getOrdersReportProducts: () => requests.get(`/orders/reports/products`),
  getOrdersReportDates: () => requests.get(`/orders/reports/lastMonth`),
};

const Sync = {
  blingOrder: () => requests.post("/bling/orders/", {}),
  logs: (page, query) => requests.get(`/sync-logs/?page=${page}&${query}`),
  mercadolivreOrder: () => requests.post("/orders/sync/mercadolivre/", {}),
  magentoOrder: () => requests.post("/orders/sync/magento/", {}),
  mercadolivreProduct: (isFullSync) => requests.put("/products/mercadolivre", { isFullSync: isFullSync }),
  mercadolivreStock: () => requests.put("/products/mercadolivre/stock", {}),
  magentoProduct: (isFullSync) =>
    requests.post("/products/magento/sync", { isFullSync: isFullSync }),
  magentoStock: () => requests.post("/products/magento/sync/stock", {}),
  getSyncConfig: () => requests.get("/plataforms/syncConfig"),
  setSyncConfig: (id, syncConfig) =>
    requests.put(`/plataforms/${id}/syncConfig`, { syncConfig }),
};

const Product = {
  list: (page, query) => requests.get(`/products?page=${page}&${query}`),
  getDetails: (productId) => requests.get(`/products/${productId}`),
  getProductMercadolivre: (sku) =>
    requests.get(`/products/mercadolivre?sku=${sku}`),
  sendToMarketplace: (productList, command, marketplace, configuration) =>
    requests.post(`/products/marketplace/send`, {
      productList,
      command,
      marketplace,
      configuration,
    }),
  setOffsetFreightRule: (skus, action, value, attribute) =>
    requests.patch(`/products`, { skus, action, value, attribute }),
};

const Plataform = {
  getAmazonStatus: () => requests.get("/amazon/platform/status"),
  setAmazon: (platform) => requests.post("/amazon/platform", { platform }),
  setBling: (platform) => requests.post("/bling/platform", { platform }),
  getBlingStatus: () => requests.get("/bling/platform/status"),
  setMagento: (plataform) =>
    requests.post("/plataforms/magento", { plataform }),
  testMagento: () => requests.post("/plataforms/magento/test", {}),
  setMercadoLivre: (plataform) =>
    requests.post("/plataforms/mercadolivre", { plataform }),
  list: () => requests.get("/plataforms/"),
  authMercadoLivre: () => requests.get(`/plataforms/mercadolivre/auth`),
  getPlataform: (plataformId) => requests.get(`/plataforms/${plataformId}`),
  delMagento: (plataformId) => requests.del(`/plataforms/${plataformId}`),
  getOrderStatus: (plataformId) =>
    requests.get(`/plataforms/${plataformId}/orderStatus`),
  setOrderStatus: (plataformId, mappedStatus) =>
    requests.put(`/plataforms/${plataformId}/orderStatus`, { mappedStatus }),
  setAttributesTypes: (plataformId, mappedAttributesTypes) =>
    requests.put(`/plataforms/${plataformId}/attributesTypes`, {
      mappedAttributesTypes,
    }),
  setMappedAttributes: (plataformId, mappedAttributes) =>
    requests.put(`/plataforms/${plataformId}/mapped-attributes`, {
      mappedAttributes,
    }),
  setMappedCategories: (plataformId, mappedCategories) =>
    requests.put(`/plataforms/${plataformId}/mapped-categories`, {
      mappedCategories,
    }),
  getMappedCategories: (plataformId) =>
    requests.get(`/plataforms/${plataformId}/mapped-categories`),
  getAttributes: (plataformId, platformName, category = "") =>
    requests.get(
      `/${platformName}/attributes/?t=${plataformId}&category=${category}`
    ),
  getMappedAttributes: (plataformId) =>
    requests.get(`/plataforms/${plataformId}/mapped-attributes`),
  getCategoriesMercadolivre: (plataformId, categoryId) =>
    requests.get(`/categories/mercadolivre/${categoryId}/?t=${plataformId}`),
  getCategoriesAmazon: () => requests.get(`/amazon/categories`),
  getAttributesTypesAmazon: () => requests.get(`/amazon/types`),
};

const Rules = {
  setOffsetFeeMercadolivre: (platformId, currentOffsetFee) =>
    requests.put(`/plataforms/${platformId}/rules`, {
      offsetFee: currentOffsetFee,
    }),
  setProductsToRuleMercadolivre: ({ platformId, rule, products }) =>
    requests.put(`/plataforms/${platformId}/mercadolivre/rules`, {
      settings: { rule: rule, products: products },
    }),
  getRules: (platformId) => requests.get(`/platforms/${platformId}/rules`),
};

const Hub = {
  getCategories: () => requests.get("/categories/"),
  getAttributes: () => requests.get("/attributes/"),
  syncAttributes: () => requests.post("/attributes/magento/sync/", {}),
  syncCategories: () => requests.post("/categories/magento/sync/", {}),
  syncOrderStatus: () => requests.post("/orders/status/magento/sync/", {}),
  getOrderStatus: () => requests.get("/orders/status"),
};

const Payment = {
  create: () => requests.post("/payments", {}),
  subscribe: () => requests.post("/payments/subscribe", {}),
  unsubscribe: () => requests.post("/payments/subscribe/cancel", {}),
  get: () => requests.get("/payments"),
};

export default {
  Auth,
  Product,
  Plataform,
  User,
  Order,
  Sync,
  Hub,
  Rules,
  Payment,
};
