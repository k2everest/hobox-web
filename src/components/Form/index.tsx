import * as React from "react";
//import createHistory from 'history/createBrowserHistory';
import { createBrowserHistory } from "history";
export const history = createBrowserHistory({});
import { Button, FormControl } from "react-bootstrap";
import { ReCaptcha } from "react-recaptcha-google";
import { FormGroup, ControlLabel, HelpBlock } from "react-bootstrap";
import { observable } from "mobx";
import { observer } from "mobx-react";
import Select from "react-select";

const s = require("./style.scss");
interface IProps {
  onSubmit(verified: true | false): void;
  enabledRecaptcha?: boolean;
  buttonLabel?: string;
  fields: {
    label: string;
    type: "text" | "number" | "password" | "email" | "date" | "multiSelect";
    name?: string;
    placeholder?: string;
    defaultValue: any;
    onChange(e): void;
    disabled?: true | false;
    stateValidate?: "success" | "warning" | "error" | null;
    errorMessage?: string;
    optionsSelect?: { label: string; value: string }[];
  }[];
}

interface IState {}

@observer
export default class Form extends React.Component<IProps, IState> {
  captchaDemo;
  @observable isVerified = false;
  validator;
  @observable shouldCheckCaptcha = false;

  constructor(props, context) {
    super(props, context);
    this.onLoadRecaptcha = this.onLoadRecaptcha.bind(this);
    this.verifyCallback = this.verifyCallback.bind(this);
  }

  componentDidMount() {
    if (this.captchaDemo) {
      this.captchaDemo.reset();
    }
  }
  onLoadRecaptcha() {
    console.log("entrou no onloa");
    if (this.captchaDemo) {
      this.captchaDemo.reset();
    }
  }
  verifyCallback(recaptchaToken) {
    console.log("entrou no vr");
    if (recaptchaToken) this.isVerified = true;
  }

  clearRecaptcha() {
    console.log("entrou no clear");
    this.isVerified = false;
    this.onLoadRecaptcha();
  }

  localSubmit(e) {
    console.log("isveri", this.isVerified);
    e.preventDefault();
    this.shouldCheckCaptcha = true;
    this.props.onSubmit(
      this.props.enabledRecaptcha
        ? this.shouldCheckCaptcha && this.isVerified
        : true
    );
    /*
      if( this.validator.allValid() ){
        alert('You submitted the form and stuff!');
      } else {
        this.validator.showMessages();
        // rerender to show messages for the first time
        this.forceUpdate();
      } */
    /*       this.props.enabledRecaptcha ==  true ?
        this.isVerified == true ?
          this.props.onSubmit(e)
        : this.shouldCheckCaptcha = true
      : this.props.onSubmit(e); */
  }

  render() {
    return (
      <form
        className={s.form}
        onSubmit={this.localSubmit.bind(this)}
        data-toggle="validator"
      >
        {this.props.fields.map((f, i) => (
          <FormGroup
            validationState={f.stateValidate ? f.stateValidate : null}
            key={i}
          >
            <div className={s.field} key={i}>
              <ControlLabel>{f.label}</ControlLabel>
              {f.type === "multiSelect" ? (
                <Select
                  placeholder={" "}
                  defaultValue={
                    f.defaultValue
                      ? f.defaultValue.map(
                          (v) => f.optionsSelect.filter((o) => o.value == v)[0]
                        )
                      : []
                  }
                  onChange={(value) => f.onChange(value)}
                  options={f.optionsSelect ? f.optionsSelect : []}
                  isMulti={true}
                />
              ) : (
                <FormControl
                  type={f.type}
                  name={f.name}
                  placeholder={f.placeholder ? f.placeholder : ""}
                  value={f.defaultValue}
                  onChange={(event) => {
                    f.onChange((event.target as any).value);
                  }}
                  disabled={f.disabled}
                  style={{ lineHeight: "2.5vh" }}
                />
              )}

              <FormControl.Feedback />
              <HelpBlock>
                {f.stateValidate == "error" && f.errorMessage
                  ? f.errorMessage
                  : ""}
              </HelpBlock>
            </div>
          </FormGroup>
        ))}
        {this.props.enabledRecaptcha ? (
          <div className={s.recaptcha}>
            <FormGroup
              validationState={
                this.shouldCheckCaptcha && !this.isVerified ? "error" : null
              }
            >
              <ReCaptcha
                ref={(el) => {
                  this.captchaDemo = el;
                }}
                size={window.innerWidth < 400 ? "compact" : "normal"}
                render="explicit"
                sitekey="6LdyLnAUAAAAAEL72WwUtEFrLAY0q5SxdN14ecSC"
                onloadCallback={this.onLoadRecaptcha}
                verifyCallback={this.verifyCallback}
                hl={"pt-BR"}
                expiredCallback={() => (this.isVerified = false)}
                erroredCallback={() => (this.isVerified = false)}
              />
              <HelpBlock>
                {this.shouldCheckCaptcha && !this.isVerified
                  ? "Mostre que é humano"
                  : null}
              </HelpBlock>
            </FormGroup>
          </div>
        ) : null}

        <div className={s.btnGroup}>
          <Button type="button" bsStyle="default" onClick={history.goBack}>
            Voltar
          </Button>
          <Button type="submit" bsStyle="primary">
            {this.props.buttonLabel ? this.props.buttonLabel : "Salvar"}
          </Button>
        </div>
      </form>
    );
  }
}
