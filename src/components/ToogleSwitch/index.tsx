import * as React from "react";
const s = require("./style.scss");

interface Props {
  value: boolean;
  onChange: (value) => void;
  active?: boolean;
  statusAlt: {
    activated: string;
    disabled: string;
    toggleInactive?: string;
  };
}

export default class ToogleSwitch extends React.Component<Props, {}> {
  constructor(props) {
    super(props);
  }

  onChanges = (val) => {
    this.props.onChange(val.target.checked);
  };

  getTitle() {
    if (this.props.active)
      return this.props.value == true
        ? this.props.statusAlt.activated
        : this.props.statusAlt.disabled;
    else
      return this.props.statusAlt.toggleInactive
        ? this.props.statusAlt.toggleInactive
        : "";
  }

  render() {
    return (
      <label className={s.switch} key={this.props.active as any}>
        <input
          type="checkbox"
          defaultChecked={this.props.value}
          onClick={(val) => this.onChanges(val)}
          disabled={this.props.active ? false : true}
        />
        <span title={this.getTitle()} className={s.slider}></span>
      </label>
    );
  }
}
