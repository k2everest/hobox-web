import * as React from "react";
import { Row, Col, Grid } from "react-bootstrap";
const MapSigns = require("react-icons/lib/fa/map-signs");
const FaTrash = require("react-icons/lib/fa/trash");
import { Button } from "react-bootstrap";
const s = require("./style.scss");

interface IProps {
  marketplaceName: string;
  mappedCategories: Array<any>;
  isMappedCategories: boolean;
  categories: Array<any>;
  filteredCategories: Array<any>;
  openOption: (category) => any;
  disassociate: (category) => any;
}

interface IState {}

const HeadTable = (props: {
  _marketplaceName: string;
  _isMappedCategories: boolean;
}) => {
  return (
    <Row className="show-grid">
      <Col xs={5} md={5}>
        <h2>Hobox</h2>
      </Col>
      <Col xs={4} md={5}>
        <h2>{props._marketplaceName}</h2>
      </Col>
      <Col xs={3} md={2}>
        <h2>{!!!props._isMappedCategories ? "Associar" : "Dissociar"}</h2>
      </Col>
    </Row>
  );
};

export default class CategoriesList extends React.PureComponent<
  IProps,
  IState
> {
  render() {
    return (
      <Grid fluid={true}>
        <HeadTable
          _marketplaceName={this.props.marketplaceName}
          _isMappedCategories={this.props.isMappedCategories}
        />
        {this.props.filteredCategories.map((category, index) => (
          <div className={s.field} key={index}>
            <Row className="show-grid ">
              <Col xs={5} md={5}>
                <span>{category.name}</span>
              </Col>
              <Col xs={4} md={5}>
                {this.props.mappedCategories.filter((a) => {
                  if (a.hobox == category.id) return a;
                })[0] ? (
                  <span>
                    <b>
                      {
                        this.props.mappedCategories.filter((a) => {
                          if (a.hobox == category.id) return a;
                        })[0].marketplace
                      }
                    </b>{" "}
                    -
                    {
                      this.props.mappedCategories.filter((a) => {
                        if (a.hobox == category.id) return a;
                      })[0].marketplaceName
                    }
                  </span>
                ) : (
                  "Não definido"
                )}
              </Col>
              <Col xs={3} md={2}>
                {!!!this.props.isMappedCategories ? (
                  <Button
                    onClick={() => this.props.openOption(category)}
                    className="btn btn-primary"
                    title="Mapear"
                  >
                    <MapSigns />
                  </Button>
                ) : (
                  <Button
                    className="btn btn-danger"
                    onClick={() => this.props.disassociate(category)}
                    title="Desassociar"
                  >
                    <FaTrash />
                  </Button>
                )}
              </Col>
            </Row>
          </div>
        ))}
      </Grid>
    );
  }
}
