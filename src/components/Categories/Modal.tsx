import * as React from "react";
import { Row, Col, Grid, Modal } from "react-bootstrap";
import { Button } from "react-bootstrap";
///import TreeView from 'react-simple-jstree';
import { observable } from "mobx";

interface IProps {
  hoboxCategory: {
    name: string;
    id: string | number;
  };
  selectedCategory?: {
    path: string;
    id: string | number;
  };
  marketplaceName: string;
  dataTreeView: any;
  save: () => any;
  close: () => any;
  handleChangeTreeView(data): void;
}

interface IState {}

export default class ModalCategories extends React.PureComponent<
  IProps,
  IState
> {
  @observable scrollY = "0";

  /* shouldComponentUpdate(nextProps) {
        return (this.props.dataTreeView != nextProps.dataTreeView);
    }
 */
  handleScroll(event) {
    let lastScrollY = event.target.scrollTop;
    if (window.innerWidth > 991) this.scrollY = lastScrollY + "px";
  }

  handleChange = (e, data) => {
    console.log("modal e", e, data);
    this.props.handleChangeTreeView(data);
  };

  render() {
    console.log("interno", this.props.dataTreeView.slice());
    //const data = { core: { data: this.props.dataTreeView.slice() } };
    return (
      <Modal
        show={true}
        onHide={() => {}}
        onScroll={this.handleScroll.bind(this)}
      >
        <Modal.Header>
          <Modal.Title>Mapeamento de Categoria</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Grid fluid={true}>
            <Row className="show-grid">
              <Col xs={12} md={6} style={{ marginTop: this.scrollY }}>
                <h4>{this.props.hoboxCategory.name}</h4>
                <label> Código: {this.props.hoboxCategory.id}</label>
                <hr />
                <h3> {this.props.marketplaceName}</h3>
                {this.props.selectedCategory ? (
                  <div>
                    <h4>{this.props.selectedCategory.path}</h4>
                    <label>
                      Código:<span> {this.props.selectedCategory.id}</span>
                    </label>
                  </div>
                ) : (
                  "Selecione uma categoria"
                )}
                <Modal.Footer>
                  <Button bsStyle="default" onClick={() => this.props.close()}>
                    Cancelar
                  </Button>
                  <Button bsStyle="primary" onClick={() => this.props.save()}>
                    Salvar
                  </Button>
                </Modal.Footer>
              </Col>
              <Col xs={6} md={6}>
                {this.props.children}
              </Col>
            </Row>
          </Grid>
        </Modal.Body>
      </Modal>
    );
  }
}
