import * as React from "react";
import { observer } from "mobx-react";
import { Button, Modal } from "react-bootstrap";
const s = require("./style.scss");

interface Props {
  show: boolean;
  context: any;
  onClick: { () };
  message: string;
  title?: string;
}

@observer
export default class SimpleModal extends React.Component<Props, any> {
  close() {
    this.props.onClick();
  }

  render() {
    return (
      <Modal
        show={this.props.show}
        container={this.props.context}
        onHide={() => {}}
      >
        {this.props.title ? (
          <Modal.Header>
            <Modal.Title>{this.props.title}</Modal.Title>
          </Modal.Header>
        ) : (
          ""
        )}
        <Modal.Body className={s.textFormat}>{this.props.message}</Modal.Body>
        <Modal.Footer>
          <Button bsStyle="primary" onClick={this.close.bind(this)}>
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
