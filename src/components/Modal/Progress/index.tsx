import * as React from "react";
import { observer } from "mobx-react";
import { Button, Modal } from "react-bootstrap";
const s = require("./style.scss");
const Done = require("react-icons/lib/md/done");
const Error = require("react-icons/lib/md/error-outline");

const Spinkit = require("react-spinkit");

interface Props {
  show: boolean;
  context: any;
  onClick: { () };
  message: Array<any>;
  title?: string;
  isFinished: boolean;
  errorMessage: any;
}

@observer
export default class ProgressModal extends React.Component<Props, any> {
  close() {
    this.props.onClick();
  }

  render() {
    return (
      <Modal
        show={this.props.show}
        container={this.props.context}
        onHide={() => {}}
      >
        {this.props.title ? (
          <Modal.Header>
            <Modal.Title>{this.props.title}</Modal.Title>
          </Modal.Header>
        ) : (
          ""
        )}
        <Modal.Body className={s.textFormat}>
          {this.props.message.map((m, i) => (
            <p key={i}>
              {m}
              {this.props.message.length - 1 == i &&
              !!!this.props.isFinished ? (
                <Spinkit
                  name="circle"
                  className={s.inlineSpinner}
                  fadeIn="none"
                />
              ) : this.props.errorMessage &&
                this.props.message.length - 1 == i ? (
                <span style={{ color: "red", paddingLeft: "0.5em" }}>
                  <Error size={20} />
                </span>
              ) : (
                <span style={{ color: "green", paddingLeft: "0.5em" }}>
                  <Done size={20} />
                </span>
              )}
            </p>
          ))}
          <p style={{ color: "red" }}>
            {" "}
            {this.props.errorMessage ? this.props.errorMessage : ""}{" "}
          </p>
        </Modal.Body>

        <Modal.Footer>
          {this.props.isFinished ? (
            <Button bsStyle="primary" onClick={this.close.bind(this)}>
              Fechar
            </Button>
          ) : (
            <Spinkit name="line-scale-pulse-out" fadeIn="none" />
          )}
        </Modal.Footer>
      </Modal>
    );
  }
}
