import * as React from "react";
import { observer } from "mobx-react";
import { Button, Modal } from "react-bootstrap";

interface Props {
  show: boolean;
  context: any;
  close: { () };
  action: { () };
  message: string;
  title: string;
}

@observer
export default class ConfirmModal extends React.Component<Props, any> {
  close() {
    this.props.close();
  }

  action() {
    this.props.action();
  }

  render() {
    return (
      <Modal
        show={this.props.show}
        container={this.props.context}
        onHide={() => {}}
      >
        <Modal.Header>
          <Modal.Title>{this.props.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{this.props.message}</Modal.Body>
        <Modal.Footer>
          <Button bsStyle="default" onClick={this.close.bind(this)}>
            Cancelar
          </Button>
          <Button bsStyle="primary" onClick={this.action.bind(this)}>
            Salvar
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
