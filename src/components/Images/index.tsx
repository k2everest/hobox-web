import * as React from "react";
import { Grid, Row, Col, Thumbnail } from "react-bootstrap";

interface IProps {
  images: any[];
}

interface IState {}

const s = require("./style.scss");

export default class Images extends React.PureComponent<IProps, IState> {
  render() {
    return (
      <div>
        <label>Imagens</label>
        <Grid fluid={true} className={s.gridRow}>
          <Row className={s.gridRow}>
            {this.props.images.map((path, i) => (
              <Col sm={2} xs={3} md={2} key={i} className={s.col}>
                <Thumbnail src={path} key={i} />
              </Col>
            ))}
          </Row>
        </Grid>
      </div>
    );
  }
}
