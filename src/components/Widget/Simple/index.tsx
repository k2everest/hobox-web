import * as React from "react";

interface IProps {
  label: string;
  content: string | number;
}

const s = require("./style.scss");

export default class widgetSimple extends React.Component<IProps, {}> {
  render() {
    return (
      <div className={s.widgetSimple}>
        <p>{this.props.label}</p>
        <b>{this.props.content}</b>
      </div>
    );
  }
}
