import * as React from "react";

//style
const s = require("./style.scss");

interface Props {
  currentPage: number;
  totalPages: number;
  totalItems: number;
  seletedItems: number;
}

export default class FilterInfo extends React.Component<Props, any> {
  render() {
    return (
      <div className={s.info}>
        <span>{this.props.seletedItems} selecionados</span>
        <span>
          {this.props.currentPage} de {this.props.totalPages} páginas
        </span>
        <span>Total de {this.props.totalItems} encontrados </span>
      </div>
    );
  }
}
