import * as React from "react";
import { Tabs, Tab } from "react-bootstrap";
import { history } from "../../containers/Route/RouteStore";

interface IProps {
  tab: {
    title: string;
    content?: any;
    key: string;
  }[];
  activeTab: any;
}

export default class ControlledTabs extends React.Component<IProps, {}> {
  componentDidMount() {
    console.log(this.props.activeTab);
  }
  handleSelect(key) {
    history.push(key);
  }

  render() {
    return (
      <Tabs
        activeKey={this.props.activeTab}
        onSelect={this.handleSelect}
        id="controlled-tab-product"
      >
        {this.props.tab.map((tab) => (
          <Tab eventKey={tab.key} title={tab.title} key={tab.key}>
            {tab.content}
          </Tab>
        ))}
      </Tabs>
    );
  }
}
