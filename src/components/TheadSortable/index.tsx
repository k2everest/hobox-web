import * as React from "react";
//import createHistory from 'history/createBrowserHistory';
import { createBrowserHistory } from "history";
export const history = createBrowserHistory({});
import * as queryString from "qs";

const s = require("./style.scss");

interface IProps {
  columns: {
    label: string;
    name: string;
  }[];
  getQuery(url: any): any;
}

export default class TheadSortable extends React.Component<IProps, any> {
  sortQuery(field) {
    const values = queryString.parse(location.search);

    field == values.sortBy && values.direction != "desc"
      ? this.runSort(field, "desc")
      : this.runSort(field, "asc");
  }

  runSort(field, direction) {
    let currentUrlQuery = location.search;
    if (currentUrlQuery.indexOf("sortBy") > -1) {
      let queryArray = currentUrlQuery.split("&");
      currentUrlQuery = queryArray
        .map((e) => {
          if (e.indexOf("sortBy") > -1) e = `sortBy=${field}`;
          else if (e.indexOf("direction") > -1) e = `direction=${direction}`;
          else e;
          return e;
        })
        .join("&")
        .toString();
    } else {
      currentUrlQuery = currentUrlQuery.concat(
        `&sortBy=${field}&direction=${direction}`
      );
    }
    this.props.getQuery(currentUrlQuery.replace("?", ""));
    history.push({
      search: currentUrlQuery,
      pathname: location.pathname,
    });
  }

  defineArrow(name) {
    const values = queryString.parse(location.search);
    if (name == values.sortBy) {
      return values.direction == "asc" ? s.arrowUp : s.arrowDown;
    }
  }

  render() {
    return (
      <thead className={s.customThead}>
        <tr>
          {this.props.columns.map((c, i) => (
            <th key={i} onClick={() => this.sortQuery(c.name)}>
              <a className={this.defineArrow(c.name)}>{c.label}</a>
            </th>
          ))}
        </tr>
      </thead>
    );
  }
}
