import * as React from "react";

//style
const s = require("./style.scss");

interface Props {
  fields: {
    label: string;
    input: {
      name: string;
      type: "select" | "text" | "number" | "multiselect";
      value: string;
      onChange: () => any;
      placeholder?: string;
      style?: "small";
    }[];
  }[];
}

export class Filters extends React.Component<Props, any> {
  render() {
    return (
      <div className={s.filtersContainer}>
        <label>Filtros:</label>
        <div className={s.filters}>
          {this.props.fields.map((f) => (
            <th key={f.label}>
              <label>{f.label}</label>
              {f.input.map((i) => (
                <input
                  type={i.type}
                  key={i.name}
                  name={i.name}
                  value={i.value}
                  className={i.style ? s.smallField : ""}
                  placeholder={i.placeholder}
                  onChange={i.onChange}
                />
              ))}
            </th>
          ))}
          {this.props.children}
        </div>
      </div>
    );
  }
}
