import * as React from "react";
const s = require("./style.scss");
import { Modal } from "react-bootstrap";

export default class SpinnerModal extends React.Component<{}, {}> {
  render() {
    return (
      <Modal show={true} container={this} onHide={() => {}}>
        <div className={s.spinner} />
      </Modal>
    );
  }
}
