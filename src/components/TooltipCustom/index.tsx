import * as React from "react";
const GoInfo = require("react-icons/lib/fa/info-circle");
import { Tooltip, OverlayTrigger } from "react-bootstrap";
const s = require("./style.scss");

interface Props {
  message: [any] | string;
}

export default class TooltipCustom extends React.Component<Props, {}> {
  render() {
    return (
      <OverlayTrigger
        placement="left"
        delayShow={300}
        delayHide={150}
        overlay={
          <Tooltip id={"tooltip"}>
            {Array.isArray(this.props.message)
              ? this.props.message.map((m, index) => <p key={index}>{m}</p>)
              : this.props.message}
          </Tooltip>
        }
      >
        <GoInfo className={s.tooltip} />
      </OverlayTrigger>
    );
  }
}
