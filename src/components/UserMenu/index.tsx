import * as React from "react";
const s = require("./style.scss");
const UserIcon = require("react-icons/lib/fa/user");
import { MenuItem, Dropdown } from "react-bootstrap";
import { authStore, commonStore } from "../../stores/rootStore";
import { Link } from "react-router-dom";

export default class UserMenu extends React.Component<{}, {}> {
  render() {
    return !commonStore.logged ? null : (
      <div className={s.container}>
        <Dropdown id="userMenu" pullRight={true}>
          <Dropdown.Toggle className={s.toggle}>
            <UserIcon />
          </Dropdown.Toggle>

          <Dropdown.Menu className={s.menu}>
            <label className="dropdown-header">
              {commonStore.user.username}
            </label>
            <label className="dropdown-header">
              <Link to={"/users/me"}>Conta</Link>
            </label>
            <MenuItem divider />
            <MenuItem eventKey="1" onClick={authStore.logout}>
              Sair
            </MenuItem>
          </Dropdown.Menu>
        </Dropdown>
      </div>
    );
  }
}
