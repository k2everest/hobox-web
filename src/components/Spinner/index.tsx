import * as React from "react";
const Spinkit = require("react-spinkit");
const s = require("./style.scss");

export default class Spinner extends React.Component<{}, {}> {
  render() {
    return (
      <Spinkit
        name="line-scale-pulse-out"
        fadeIn="none"
        className={s.spinner}
      />
    );
  }
}
