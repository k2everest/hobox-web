import * as React from "react";

interface Props {
  label: string;
  color: string;
}

const s = require("./style.scss");

export default class SpanInfo extends React.Component<Props, {}> {
  render() {
    return (
      <span className={s.info} style={{ backgroundColor: this.props.color }}>
        {this.props.label}
      </span>
    );
  }
}
