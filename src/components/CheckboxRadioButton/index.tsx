import * as React from "react";
const ToggleButtonGroup = require("react-bootstrap/lib/ToggleButtonGroup");
const ToggleButton = require("react-bootstrap/lib/ToggleButton");
const s = require("./style.scss");

interface IProps {
  value: string | any;
  name: string;
  label: string;
  type: "radio" | "checkbox";
  options: {
    value: string | any;
    label: string | any;
    disabled?: boolean;
  }[];
  onChanges: (value) => {} | void;
}

export default class CheckboxRadioButton extends React.Component<IProps, any> {
  constructor(props) {
    super(props);
    this.state = {
      value: [],
    };
  }

  onChange = (value) => {
    //this.setState({ value });
    this.props.onChanges(value);
  };

  render() {
    return (
      <div className={s.checkboxButton}>
        <label>{this.props.label}</label>
        <ToggleButtonGroup
          type={this.props.type}
          value={this.props.value}
          onChange={this.onChange}
          name={this.props.name}
        >
          {this.props.options.map((checkbox) => (
            <ToggleButton
              key={checkbox.value}
              value={checkbox.value}
              disabled={checkbox.disabled ? true : false}
            >
              {checkbox.label}
            </ToggleButton>
          ))}
        </ToggleButtonGroup>
      </div>
    );
  }
}
