import * as React from "react";
//import createHistory from 'history/createBrowserHistory';
import { createBrowserHistory } from "history";
export const history = createBrowserHistory({});
import Select from "react-select";

//style
const s = require("./style.scss");

interface Props {
  title: string;
  fields: {
    label: string;
    name: string;
    type: "select" | "text" | "number" | "multiselect" | "datetime";
    value?: any;
    options?: { value: any; label: any }[];
    placeholder?: string;
  }[];

  getQuery(url: any): any;
}

const handleObj = (key: any, value: any) => {
  if (Array.isArray(value) && value.length >= 1) {
    return value.map((val) => key + "=" + val).join("&");
  } else {
    return key + "=" + value;
  }
};

const customStyles = {
  control: (styles) => ({
    ...styles,
    backgroundColor: "white",
    borderWidth: "1px",
    padding: "1em 1em 0em 0em",
  }),
};

const getAllUrlParams = (url) => {
  let queryString = url;
  var obj = {};
  // stuff after # is not part of query string, so get rid of it
  queryString = queryString.split("#")[0];

  // split our query string into its component parts
  var arr = queryString.split("&");

  for (var i = 0; i < arr.length; i++) {
    // separate the keys and the values
    var a = arr[i].split("=");
    // in case params look like: list[]=thing1&list[]=thing2
    var paramNum = undefined;
    var paramName = a[0];

    // set parameter value (use 'true' if empty)
    var paramValue = typeof a[1] === "undefined" ? true : a[1];

    // if parameter name already exists
    if (obj[paramName]) {
      // convert value to array (if still string)
      if (typeof obj[paramName] === "string") {
        obj[paramName] = [obj[paramName]];
      }
      // if no array index number specified...
      if (typeof paramNum === "undefined") {
        // put the value on the end of the array
        obj[paramName].push(paramValue);
      }
      // if array index number specified...
      else {
        // put the value at that index number
        obj[paramName][paramNum] = paramValue;
      }
    }
    // if param name doesn't exist yet, set it
    else {
      if (paramName.indexOf("[") > -1) {
        obj[paramName] = [];
        obj[paramName].push(paramValue);
      } else obj[paramName] = paramValue;
    }
  }

  return obj;
};

export default class AsyncFilters extends React.Component<Props, any> {
  state = {
    filters: {},
    selectedOption: null,
    isOpened: true,
  };

  componentWillMount() {
    this.props.getQuery(this.readUrlParams());
    this.convertQueryStringToState(this.readUrlParams());
  }

  readUrlParams() {
    let currentUrlQuery = location.search.replace("?", "");
    return currentUrlQuery;
  }

  convertToQueryString(obj: Object) {
    let queryString = Object.keys(obj)
      .map((key) => handleObj(key, obj[key]))
      .join("&");
    return queryString;
  }

  convertQueryStringToState(queryString: string) {
    if (queryString.length > 0) {
      //let objectQuery = JSON.parse('{"' + decodeURI(queryString).replace('?','').replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
      let objectQuery = getAllUrlParams(queryString);
      this.setState({
        filters: objectQuery,
      });
    }
  }

  updateFilter = (name, value) => {
    this.setState(
      {
        filters: { ...this.state.filters, [name]: value },
      },
      () => {
        this.props.getQuery(this.convertToQueryString(this.state.filters));
        history.push({
          search: this.convertToQueryString(this.state.filters),
          pathname: location.pathname,
        });
      }
    );
  };

  setIsOpened = () => {
    this.setState({
      isOpened: !this.state.isOpened,
    });
  };

  onChange = ({ target: { value, name } }) => {
    this.updateFilter(name, value);
  };

  handleChange = (name, selectedOption) => {
    const values = selectedOption.map((o) => o.value);
    this.updateFilter(name, values);
  };

  render() {
    return (
      <fieldset className={s.filters}>
        <legend className={s.filterTitle}>
          <a onClick={this.setIsOpened}>{this.props.title}</a>
        </legend>
        <div className={this.state.isOpened ? s.fields : s.hidden}>
          {this.props.fields.map((field, i) => {
            switch (field.type) {
              case "text":
              case "number":
                return (
                  <label
                    key={i}
                    className={
                      this.state.filters[field.name] &&
                      this.state.filters[field.name].length > 0
                        ? s.downsizingLabel
                        : ""
                    }
                  >
                    <input
                      onChange={this.onChange}
                      type={field.type}
                      key={i}
                      name={field.name}
                      defaultValue={this.state.filters[field.name]}
                      placeholder=" "
                    />
                    <span>{field.label}</span>
                  </label>
                );
              case "datetime":
                return (
                  <label key={i}>
                    <input type="datetime-local" />
                    <span>{field.label}</span>
                  </label>
                );
              case "multiselect":
                return (
                  <label
                    key={i}
                    className={
                      this.state.filters[field.name] &&
                      this.state.filters[field.name][0] &&
                      this.state.filters[field.name][0].length > 1
                        ? s.downsizingLabel
                        : ""
                    }
                  >
                    <Select
                      className={s.selectFilter}
                      name={field.name}
                      styles={customStyles}
                      placeholder={" "}
                      defaultValue={
                        this.state.filters[field.name]
                          ? this.state.filters[field.name].map(
                              (v) =>
                                field.options.filter((o) => o.value == v)[0]
                            )
                          : []
                      }
                      onChange={(value) => this.handleChange(field.name, value)}
                      options={field.options ? field.options : []}
                      isMulti={true}
                    />
                    <span>{field.label}</span>
                  </label>
                );
            }
          })}
        </div>
      </fieldset>
    );
  }
}
