import { Jumbotron } from "react-bootstrap";
import * as React from "react";

//style
const s = require("./style.scss");

interface IProps {
  title: string;
  content: {
    label: any;
    content: string;
  }[];
}

interface IState {}

export default class CustomJumbotron extends React.Component<IProps, IState> {
  render() {
    return (
      <Jumbotron className={s.jumbotron}>
        <h2>{this.props.title}</h2>
        {this.props.content.map((line, index) => {
          return (
            <div key={index} className={s.contentContainer}>
              <div className={s.content}>
                <b> {line.label ? line.label + ":" : ""} </b>
                <p>{line.content}</p>
              </div>
            </div>
          );
        })}
      </Jumbotron>
    );
  }
}
