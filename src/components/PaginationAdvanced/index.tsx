import * as React from "react";
import { Pagination } from "react-bootstrap";

interface IProps {
  items: number;
  maxButtons: number;
  onClick: (value) => {};
  page: number;
}

const s = require("./style.scss");

export default class PaginationAdvanced extends React.Component<IProps, any> {
  handleSelect(eventKey) {
    this.props.onClick(eventKey);
  }

  render() {
    return (
      <Pagination
        className={s.pagination}
        prev
        next
        first
        last
        ellipsis
        boundaryLinks
        items={this.props.items}
        maxButtons={this.props.maxButtons}
        activePage={this.props.page}
        onSelect={this.handleSelect.bind(this)}
      />
    );
  }
}
