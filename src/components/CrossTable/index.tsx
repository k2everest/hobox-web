import * as React from "react";
import { Table } from "react-bootstrap";

//style
const s = require("./style.scss");

type Column = {
  label: string;
  name: string;
};

interface IProps {
  title: string;
  collumns: Column[];
  lines: {
    label: string;
    name: string;
    value: any[];
  }[];
}

interface IState {}

export default class CrossTable extends React.Component<IProps, IState> {
  collumns = (collumns: Column[]) => {
    return collumns.map((col, index) => <th key={index}>{col.label}</th>);
  };

  render() {
    return (
      <div className={s.crossTable}>
        <label className={s.title}>{this.props.title}</label>

        <Table condensed>
          <thead>
            <tr>
              <th>Hobox</th>
              {this.collumns(this.props.collumns)}
            </tr>
          </thead>
          <tbody>
            {this.props.lines.map((line, index) => {
              return (
                <tr key={index}>
                  <td>
                    <b>{line.label}</b>
                  </td>
                  {line.value.map((c, index2) => {
                    return <td key={c + index2}>{c}</td>;
                  })}
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}
