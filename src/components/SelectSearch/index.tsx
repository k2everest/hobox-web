import * as React from "react";
import { createBrowserHistory } from "history";
export const history = createBrowserHistory({});
import Select from "react-select";

//style
const s = require("./style.scss");

interface Props {
  label: string;
  name: string;
  type: "select" | "text" | "number" | "multiselect" | "datetime";
  value?: any;
  options?: { value: any; label: any }[];
  placeholder?: string;
  onChange: (value) => void;
}

const customStyles = {
  control: (styles) => ({
    ...styles,
    backgroundColor: "white",
    borderWidth: "1px",
    padding: "1em 1em 0em 0em",
  }),
};

export default class SelectSearch extends React.Component<Props, any> {
  constructor(props) {
    super(props);
  }

  state = {
    filters: {},
    selectedOption: null,
    isOpened: true,
  };

  setIsOpened = () => {
    this.setState({
      isOpened: !this.state.isOpened,
    });
  };

  onChange = (e) => {
    console.log("onclico", e);
    e === null
      ? this.props.onChange(this.props.value)
      : this.props.onChange(e.value);
  };

  getValueFromOption(value) {
    return this.props.options.filter((o) => value === o.value)[0];
  }

  defineSelect() {
    switch (this.props.type) {
      case "text":
      case "number":
        return (
          <input
            onChange={this.onChange}
            type={this.props.type}
            name={this.props.name}
            defaultValue={this.props.name}
            placeholder=" "
          />
        );
      case "multiselect":
        return (
          <Select
            className={s.selectFilter}
            name={this.props.name}
            styles={customStyles}
            placeholder={this.props.placeholder}
            onChange={(value) => this.onChange(value)}
            options={this.props.options ? this.props.options : []}
            isMulti={true}
            isClearable={true}
          />
        );
      case "select":
        return (
          <Select
            className={s.selectFilter}
            defaultValue={this.getValueFromOption(this.props.value)}
            name={this.props.name}
            styles={customStyles}
            placeholder={this.props.placeholder}
            onChange={(value) => this.onChange(value)}
            options={this.props.options ? this.props.options : []}
            isClearable={true}
          />
        );
    }
  }

  render() {
    return this.defineSelect();
  }
}
