import * as React from "react";
import Loki, { LokiStepContainer } from "react-loki";
import { Modal, Button } from "react-bootstrap";

//style
require("./style.css");

interface Props {
  steps: {
    label: string;
    component: any;
  }[];
  onFinish: () => void;
  closeBox: () => void;
  messageValidate?: string;
  canFinish: boolean;
  finalMessage: string;
  isFinalPage: boolean;
}

export default class FormStepLoki extends React.Component<Props, any> {
  render() {
    return (
      <Modal show={true} onHide={() => {}}>
        <Modal.Body>
          {this.props.isFinalPage ? (
            <label>{this.props.finalMessage}</label>
          ) : (
            <LokiStepContainer>
              <Loki
                nextLabel={<Button>Avançar</Button>}
                backLabel={<Button>Voltar</Button>}
                finishLabel={
                  <Button
                    bsStyle="primary"
                    title={
                      this.props.messageValidate
                        ? this.props.messageValidate
                        : ""
                    }
                    disabled={!this.props.canFinish}
                  >
                    Concluir
                  </Button>
                }
                steps={this.props.steps}
                onFinish={this.props.onFinish}
              />
            </LokiStepContainer>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle="default" onClick={this.props.closeBox}>
            Fechar
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
