import * as React from "react";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";

interface IProps {
  options: {
    value: string;
    label?: string;
  }[];
  placeholder: string;
  controlLabel?: string;
  value: string;
  externalKey: string;
  onChange: (value) => void;
}

export default class Select extends React.Component<IProps, any> {
  onChanges = (val) => {
    this.props.onChange(val.target.value);
  };

  render() {
    return (
      <FormGroup
        controlId="formControlsSelect"
        key={this.props.externalKey}
        onChange={(val) => this.onChanges(val)}
      >
        {this.props.controlLabel ? (
          <ControlLabel>{this.props.controlLabel}</ControlLabel>
        ) : (
          ""
        )}
        <FormControl
          componentClass="select"
          key={this.props.externalKey}
          placeholder={this.props.placeholder}
          defaultValue={this.props.value}
        >
          <option value={""} key={""}>
            Selecione
          </option>
          {this.props.options.map((option, key) => (
            <option
              value={option.value}
              key={key}
              //selected={this.props.value===option.value} key={key}
            >
              {option.label ? option.label : option.value}
            </option>
          ))}
        </FormControl>
      </FormGroup>
    );
  }
}
