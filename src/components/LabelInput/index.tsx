import * as React from "react";

const s = require("./style.scss");

interface Props {
  label: string;
  value: string;
  textArea?: boolean;
}

export default class LabelInput extends React.Component<Props, {}> {
  render() {
    return (
      <div className={s.field}>
        <label>{this.props.label}</label>
        {this.props.textArea ? (
          <textarea value={this.props.value} rows={5} disabled />
        ) : (
          <input type="text" disabled value={this.props.value} />
        )}
      </div>
    );
  }
}
