import { observable, action } from "mobx";
import api from "../services/api";
import stores from "./index";

export type User = {
  _id?: string;
  name: string;
  lastname?: string;
  email: string;
};

export type Password = {
  currentPassword: string;
  newPassword: string;
  confirmPassword: string;
};

export type Company = {
  name: string;
  website: string;
  documentNumber: string;
  documentName: string;
  phone: string;
  address: {
    street: string;
    city: string;
    state: string;
    zip: string;
  };
};

const initialCompany = {
  name: "",
  website: "",
  documentNumber: "",
  documentName: "",
  phone: "",
  address: {
    street: "",
    city: "",
    state: "",
    zip: "",
  },
};

const initialPassword = {
  currentPassword: "",
  newPassword: "",
  confirmPassword: "",
};

class UserStore {
  @observable inProgress = false;
  @observable done = false;
  @observable errors = undefined;
  @observable users: User[] = undefined;
  @observable user: User = undefined;
  @observable userPassword: Password = initialPassword;
  @observable userCompany: Company = undefined;
  @observable currentUser;
  @observable loadingUser;
  @observable updatingUser;
  @observable updatingUserErrors;

  constructor() {}

  @action _setDone() {
    this.inProgress = false;
    this.done = true;
  }

  @action _unsetDone() {
    this.done = false;
    this.inProgress = true;
  }

  @action _setUsers(users) {
    this.users = users;
    this.inProgress = false;
  }

  @action _setCompany(company) {
    this.userCompany = company == null ? initialCompany : company;
    this.inProgress = false;
  }

  clearUserPassword() {
    this.userPassword = initialPassword;
  }

  @action listUsers() {
    this._unsetDone();
    return api.User.list()
      .then((users) => this._setUsers(users))
      .catch(
        action((err) => {
          console.log(err);
          this.errors = err;
          this._setDone();
          throw err;
        })
      );
  }

  @action pullUser() {
    this.loadingUser = true;
    return api.User.current()
      .then((user) => (this.currentUser = user))
      .finally(this._setDone());
  }

  @action register() {
    this.inProgress = true;
    this.errors = undefined;
    return api.User.register(this.currentUser)
      .then((user) => stores.commonStore.setUser(user))
      .catch((err) => {
        let error = err.response.body;
        this.errors = error.data;
        this._setDone();
      });
  }

  @action updateUser(newUser) {
    this._unsetDone();
    this.updatingUser = true;
    return api.User.update(newUser)
      .then((user) => {
        this.currentUser = user;
        this._setDone();
      })
      .catch((err) => {
        this.errors = err.message;
        this._setDone();
      });
  }

  @action updatePasswordUser() {
    this._unsetDone();
    return api.User.updatePassword(this.userPassword)
      .then(() => {
        this.clearUserPassword();
        this._setDone();
      })
      .catch((err) => {
        this.errors = err.message;
        this.clearUserPassword();
        this._setDone();
      });
  }

  @action updateCompanyUser() {
    this._unsetDone();
    return api.User.updateCompany(this.userCompany)
      .then(() => {
        this.clearUserPassword();
        this._setDone();
      })
      .catch((err) => {
        this.errors = err.message;
        this.clearUserPassword();
        this._setDone();
      });
  }

  @action getCompanyUser() {
    this._unsetDone();
    return api.User.getCompany()
      .then((company) => {
        this._setCompany(company);
        this._setDone();
      })
      .catch(
        action((err) => {
          console.log(err);
          this.errors = err;
          this._setDone();
          throw err;
        })
      );
  }

  @action deactivateAccount() {
    this._unsetDone();
    return api.User.deactivateAccount(this.userPassword)
      .then(() => {
        this.clearUserPassword();
        this._setDone();
      })
      .catch((err) => {
        this.errors = err.message;
        this.clearUserPassword();
        this._setDone();
      });
  }

  @action recoverAccount() {
    this._unsetDone();
    return api.User.recoverAccount(this.userPassword)
      .then(() => {
        this.clearUserPassword();
        this._setDone();
        stores.authStore.unsetRecoverMode();
      })
      .catch((err) => {
        this.errors = err.message;
        this.clearUserPassword();
        this._setDone();
      });
  }

  @action getInfo(_id: any) {
    console.log("get info store");
  }

  @action forgetUser() {
    this.currentUser = undefined;
  }
}

export default UserStore;
