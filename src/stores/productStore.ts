import { observable, action, computed } from "mobx";
import api from "../services/api";

export type Product = {
  _id: string;
  sku: string;
  name: string;
  qty: string;
  categories: [{}];
  price: string;
  image: string;
};

const dataMercadolivreInitial = {
  listingType: null,
  shippingMode: null,
  shippingFree: null,
};

class ProductStore {
  @observable inProgress = false;
  @observable done = false;
  @observable errors = undefined;
  @observable products: any = null;
  @observable productDetails: any = null;
  @observable productsPages: number = 0;
  @observable productsTotal: number = 0;
  @observable productMercadolivre: any = null;
  @observable selectedMarketplaces: any = [];
  @observable selectedStatus = [];
  @observable selectedMarketplace = "mercadolivre";
  @observable selectedCategories = [];
  @observable selectedsProducts = [];
  @observable stepFormIsOpened: boolean = false;
  @observable responseApi = [];
  @observable openedBox = false;

  @observable dataMercadolivre: {
    listingType: string;
    shippingMode: string;
    shippingFree: string;
  } = dataMercadolivreInitial;

  query: any = [
    {
      name: "sort",
      value: "",
    },
    {
      name: "sku",
      value: "",
    },
    {
      name: "name",
      value: "",
    },
    {
      name: "qtyFrom",
      value: "",
    },
    {
      name: "qtyTo",
      value: "",
    },
    {
      name: "priceFrom",
      value: "",
    },
    {
      name: "direction",
      value: 1,
    },
    {
      name: "priceTo",
      value: "",
    },
  ];

  constructor() {}

  @action unsetProducts() {
    this.products = [];
    this.productsPages = 0;
    this.productsTotal = 0;
  }

  @action setProducts(result) {
    this.products = result.products;
    this.productsPages = result.pages;
    this.productsTotal = result.total;
    this.inProgress = false;
  }

  @action _setProductDetails(productDetails) {
    this.productDetails = productDetails;
    this.inProgress = false;
  }

  @action _setProductMercadolivre(productMercadolivre) {
    this.productMercadolivre = productMercadolivre;
    this.inProgress = false;
  }
  @action _openStepForm() {
    this.stepFormIsOpened = true;
  }

  @action _closeStepForm() {
    this.stepFormIsOpened = false;
    this.closeBox();
  }

  @action closeBox() {
    this.responseApi = [];
    this.openedBox = false;
  }

  @action setDone() {
    this.inProgress = false;
    this.done = true;
  }

  @action unsetDone() {
    this.inProgress = true;
    this.done = false;
    this.errors = undefined;
  }

  @action listProducts(page, query = "") {
    this.inProgress = true;
    return api.Product.list(page, query)
      .then((products) => this.setProducts(products))
      .catch(
        action((err) => {
          this.errors = err;
          throw err;
        })
      );
  }

  @action getProductDetails(productId) {
    this.inProgress = true;
    return api.Product.getDetails(productId)
      .then((productDetails) => this._setProductDetails(productDetails))
      .catch(
        action((err) => {
          this.errors = err;
        })
      );
  }

  @action getProductMercadolivre(sku) {
    this.inProgress = true;
    return api.Product.getProductMercadolivre(sku)
      .then((product) => this._setProductMercadolivre(product))
      .catch(
        action((err) => {
          this.errors = err;
        })
      );
  }

  @action sendToMarketplace(command) {
    this.selectedsProducts;
    this.stepFormIsOpened = false;
    this.openedBox = true;
    if (this.selectedsProducts.length < 1)
      return (this.errors = { message: "Selecione ao menos um produto\n" });
    if (!this.selectedMarketplace)
      return (this.errors = { message: "Selecione um marketplace\n" });
    this.unsetDone();
    return api.Product.sendToMarketplace(
      this.selectedsProducts,
      command,
      this.selectedMarketplace,
      this.dataMercadolivre
    )
      .then((response) => {
        this.responseApi = response;
      })
      .catch((err) => {
        this.errors =
          err.status == 403
            ? {
                message:
                  "Persmissão de sincronização de produto negada.\n Por favor, verfique as configurações de sincronização",
              }
            : { message: "Houve um problema na sincronização" };
      })
      .then(async () => {
        await this.resetDataMercadolivre();
        await this.listProducts(1);
        this.setDone();
      });
  }

  chooseMarketplace(marketplace) {
    this.selectedMarketplace = marketplace;
  }
  resetDataMercadolivre() {
    this.dataMercadolivre = dataMercadolivreInitial;
    this.selectedsProducts = [];
  }

  @computed get errorMessage() {
    let message = "";
    if (this.dataMercadolivre.listingType == null)
      message = "Defina o Tipo de frete\n";
    if (this.dataMercadolivre.shippingMode == null)
      message = message + "Defina o Tipo de frete\n";
    if (this.dataMercadolivre.shippingFree == null)
      message = message + "Defina a opção para Frete Grátis\n";

    return message;
  }

  @action setOffsetFreightRule(action, value) {
    this.unsetDone();
    let attribute = "offsetFreight";
    if (this.selectedsProducts.length < 1)
      this.errors = { message: "Selecione ao menos um item para regra" };
    return api.Product.setOffsetFreightRule(
      this.selectedsProducts,
      action,
      value,
      attribute
    )
      .then(() => {
        this.selectedsProducts = [];
        this.setDone();
      })
      .catch((err) => {
        this.errors = err;
      });
  }
}

export default ProductStore;
