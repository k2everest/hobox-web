import { observable, action, computed } from 'mobx';
import api from '../services/api';


export type MappedCategories = {
  hobox: string,
  marketplace: string,
  marketplaceName?: string
}

class CategoryStore {

    @observable mappedCategories : MappedCategories[] = [];
    @observable categories : Array<any> = []
    @observable inProgress = false;
    @observable done = false;
    @observable errors = undefined;
    @observable queryCategory = '';
    @observable isMappedCategories: boolean = false;
    @observable isModalOpened : boolean = false;
    @observable hoboxCategorySelected : any =  null;
    @observable platformId : string = '';
    @observable marketplaceCategorySelected : {id: string, path: string} = null;
    @observable data: any = undefined;


    @action setDone() {
        this.inProgress = false;
        this.done =  true;
    }

    @action unsetDone() {
      this.done =  false;
      this.inProgress = true;
      this.errors = undefined;
    }

    @action openModal(category){
      this.isModalOpened = true;
      this.hoboxCategorySelected = category;
    }

    @action closeModal(){
      this.isModalOpened = false;
      this.data = undefined;
      this.marketplaceCategorySelected = null;
      this.hoboxCategorySelected = null;
    }

    @action setPlatformId(id){
      this.platformId = id;
    }
  

    @action getCategories(){
        return api.Hub.getCategories()
        .then((categories) => this.categories = categories)
        .catch(action((err) => {
          this.errors = err;
          throw err;
        }))
        .then(()=>this.setDone());
    }

    @action getMappedCategories(platformId){
      this.platformId = platformId;
      return api.Plataform.getMappedCategories(platformId)
      .then((mappedCategories)=>this.mappedCategories = mappedCategories)
      .catch(err => {
        this.errors = err;
        this.setDone();
        throw err;
      })
      .then(()=>  this.setDone());
    }


    @action setMarketplaceCategorySelected(category){
      console.log('set mapeed category',category);
      this.marketplaceCategorySelected = category;
    }
    

    @computed get filteredCategories(){
      
      let categories = []

      if(this.isMappedCategories === true){
        categories = this.mappedCategories.map(mapped=>{
          return this.categories.filter(category=>{
            if(mapped.hobox==category.id)
              return category;
          })[0];
        });

      }else{
        categories = this.categories.filter(category=>{
         if(this.mappedCategories.filter(mapped=>{
          if(mapped.hobox== category.id)
            return category;        
          }).length == 0)
            return category;
        });
      }

      return categories.filter((cat)=>cat.name.toLowerCase().search(this.queryCategory)!== -1);
    }


    @action setMappedCategory( mappedCategories: MappedCategories ){
      this.closeModal();
      this.unsetDone();
      return api.Plataform.setMappedCategories(this.platformId, mappedCategories)
      .then(()=>this.setDone())
      .then(()=>this.getMappedCategories(this.platformId))
      .catch((err) => {
        this.errors = err.response.body;
        this.setDone();
        throw err;
      });
    }


    @action setMarketplaceCategorySelectedML(selected){
      if(selected.id)
        this.marketplaceCategorySelected = {
          id: selected.id,
          path: selected.path_from_root.map(a=>a.name).join('/')
        }; 
    }  

    @action getCategoryMercadolivre(categoryId){
      this.data = this._selectCategoryDinamic(categoryId);      
      return api.Plataform.getCategoriesMercadolivre(this.platformId, categoryId)
      .then((categories) => {
        this.data = this._parseDataMl(categories);     
      })
      .catch(action((err) => {
        this.errors = err;
        throw err;
      }))
      .then(()=>this.setDone());
    }

    @action async getCategoriesFactory(platformName,platformId = 0){
      switch(platformName){
        case 'amazon': {
          await this.getAmazonCategories();
          console.log('entrou aqui');
          let tempCategories = [];
          this.data.map(elem=>elem.children.map(sub=> tempCategories.push({ label:`${elem.text}-${sub}`, value: `${elem.text}-${sub}`)  ) );
          this.data = tempCategories;
          console.log(tempCategories);
          break;
        }
        case 'mercadolivre':  await this.getMappedCategories(platformId); 
        let data = this.mappedCategories.map(e=>{ return {"value": e.marketplace, "label":e.marketplaceName}});
        const uniqueElements = Array.from(new Set(data.map(a => a.value)))
        .map(id => {
          return data.find(a => a.value === id)
        });
        this.data = uniqueElements;
        break;
        default : console.log('default')
      }
    }

    @action getAmazonCategories(){
      this.unsetDone();
      return api.Plataform.getCategoriesAmazon()
      .then((categories) => {
        this.data = categories;
     
      })
      .catch(action((err) => {
        this.errors = err;
        throw err;
      }))
      .then(()=>this.setDone());
    }

    _selectCategoryDinamic(categoryId){

      if(categoryId!=0){
          return this.data.slice().map(
              function(a){
                  if(a.id==categoryId){
                  return {
                      id: a.id,
                      parent: a.parent,
                      text: a.text,
                      state: {
                      opened : true,
                          selected : true,
                          loading: true,
                      focus: true
                      }
                  };
                  }
                  else
                  return {
                      id: a.id,
                      parent: a.parent,
                      text: a.text,
                      state: {
                          opened: true,
                          selected : false,
                          loading: false,
                      },
                      focus: false
  
                  };
              }
        );
      }
    }

    _parseDataMl(selected){
        
      if(selected.id){
        const children = selected.children_categories.map(c=> 
          {
            return {id: c.id, text:c.name, parent:selected.id}
          }
        );
        let data =  this.data.slice().map(a=>
           a.id==selected.id?
           {
              id: a.id,
              parent: a.parent,
              text: a.text,
              state: {
              opened : true,
              selected : true,
              focus: true,
              loading: false

              }
            }
            : {
              id: a.id,
              parent: a.parent,
              text: a.text,
              state: {
              opened : true,
              selected : false,
              focus: false,
              loading: false
              }
            }
          );

        if(data.filter(a=>a.parent==selected.id).length <= 0){
          data.push(...children);
        }

        return data;          
      }else
          return selected.map(a=>{
              return {
                  text: a.name,
                  id: a.id,
                  parent:'#',
                  state: {
                      opened: false
                  }
              }
          });
    }   

}

export default CategoryStore;
