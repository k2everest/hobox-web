import { observable, action } from "mobx";
import api from "../services/api";
//import commonStore from './commonStore';

export type Product = {
  _id: string;
  sku: string;
  name: string;
  qty: string;
  categories: [{}];
  price: string;
  image: string;
};

class OrderStore {
  @observable inProgress = false;
  @observable done = false;
  @observable errors = undefined;
  @observable orders: any = null;
  @observable order: any = null;
  @observable ordersSummaries = undefined;
  @observable ordersReportsProducts: Array<any> = [].slice();
  @observable ordersReportsDates = undefined;

  constructor() {}

  @action _setOrders(orders) {
    this.orders = orders;
    this.inProgress = false;
  }

  @action _setOrdersSumaries(result) {
    console.log("entrou sumaries");
    this.ordersSummaries = result;
    this.inProgress = false;
  }

  @action _setOrdersReportsProducts(result) {
    this.ordersReportsProducts = result.slice();
    this.inProgress = false;
  }

  @action _setOrdersReportsDates(result) {
    this.ordersReportsDates = result;
    this.inProgress = false;
  }

  @action _setOrder(order) {
    console.log("order", order);
    this.order = order;
    this.inProgress = false;
  }

  @action setDone() {
    this.inProgress = false;
    this.done = true;
  }

  @action unsetDone() {
    this.inProgress = false;
    this.done = false;
  }

  @action listOrders(page, query = "") {
    this.inProgress = true;
    return api.Order.list(page, query)
      .then((orders) => this._setOrders(orders))
      .catch(
        action((err) => {
          this.errors = err;
          throw err;
        })
      );
    //.finally(action(() => { this.loadingUser = false; }))
  }

  @action getOrder(orderId) {
    this.inProgress = true;
    return api.Order.getOrder(orderId)
      .then((order) => this._setOrder(order))
      .catch(
        action((err) => {
          this.errors = err;
        })
      );
  }

  @action getOrderCart(packId) {
    this.inProgress = true;
    return api.Order.getCart(packId)
      .then((result) => this._setOrder(result.orders[0]))
      .catch(
        action((err) => {
          this.errors = err;
        })
      );
  }

  @action getOrdersSummaries() {
    this.inProgress = true;
    return api.Order.getOrdersSummary()
      .then((summary) => this._setOrdersSumaries(summary))
      .catch(
        action((err) => {
          this.errors = err;
        })
      );
  }

  @action getOrdersReportsProducts() {
    this.inProgress = true;
    return api.Order.getOrdersReportProducts()
      .then((reports) => this._setOrdersReportsProducts(reports))
      .catch(
        action((err) => {
          this.errors = err;
        })
      );
  }

  @action getOrdersReportsDates() {
    this.inProgress = true;
    return api.Order.getOrdersReportDates()
      .then((reports) => this._setOrdersReportsDates(reports))
      .catch(
        action((err) => {
          this.errors = err;
        })
      );
  }
}

export default OrderStore;
