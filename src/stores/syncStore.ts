import { observable, action } from "mobx";
import api from "../services/api";

export type Config = {
  send: boolean;
  receive: boolean;
};

export type SyncConfig = {
  order: Config;
  product: Config;
  stock: Config;
};

class SyncStore {
  @observable inProgress = false;
  @observable done = false;
  @observable errors = undefined;
  @observable syncLogs = undefined;
  @observable syncConfigPlatforms = [];
  @observable loading = false;

  constructor() { }

  @action setDone() {
    this.inProgress = false;
    this.done = true;
  }

  @action unsetDone() {
    this.inProgress = false;
    this.done = false;
  }

  @action setError(err) {
    this.errors = err.response
      ? err.response.body.message
      : "Falha na requisicão";
  }

  @action getSyncConfig() {
    return api.Sync.getSyncConfig()
      .then((result) => (this.syncConfigPlatforms = result))
      .catch(
        action((err) => {
          this.errors = err;
        })
      );
  }

  @action setSyncConfig(id, syncConfig) {
    return api.Sync.setSyncConfig(id, syncConfig)
      .then(() => this.getSyncConfig())
      .catch(
        action((err) => {
          this.errors = err;
        })
      );
  }

  @action getSyncLogs(page, query = "") {
    this.loading = true;
    return api.Sync.logs(page, query)
      .then((result) => {
        this.syncLogs = result;
        this.loading = false;
      })
      .catch(
        action((err) => {
          this.errors = err;
          throw err;
        })
      );
    //.then(this.loading = false);
  }

  @action sync(plataform, item, isFullSync) {
    this.inProgress = true;
    this.errors = undefined;

    if (!plataform || !item || plataform[0] === undefined) {
      this.errors = "Selecione uma plataforma e uma opção válida";
      return this.setDone();
    }

    plataform.map((plataform) => {
      if (plataform == "magento") {
        switch (item) {
          case "product":
            return api.Sync.magentoProduct(isFullSync)
              .then(() => this.setDone())
              .catch((err) => {
                this.setError(err);
                this.setDone();
              });
          case "order":
            return api.Sync.magentoOrder()
              .then(() => {
                this.errors = null;
                this.setDone();
              })
              .catch((err) => {
                this.setError(err);
                this.setDone();
                throw err;
              });
          case "stock":
            return api.Sync.magentoStock()
              .then(() => this.setDone())
              .catch((err) => {
                this.setError(err);
                this.setDone();
              });
          default:
            this.errors = "Opção indisponivel";
            return this.setDone();
        }
      }
      if (plataform == "mercadolivre") {
        switch (item) {
          case "order":
            return api.Sync.mercadolivreOrder()
              .then(() => {
                this.errors = null;
                this.setDone();
              })
              .catch((err) => {
                this.setError(err);
                this.setDone();
                throw err;
              });
          case "product":
            return api.Sync.mercadolivreProduct(isFullSync)
              .then(() => this.setDone())
              .catch((err) => {
                this.setError(err);
                this.setDone();
              });
          case "stock":
            return api.Sync.mercadolivreStock()
              .then(() => this.setDone())
              .catch((err) => {
                this.setError(err);
                this.setDone();
              });
          default:
            this.errors = "Opção indisponivel";
            return this.setDone();
        }
      }
      if (plataform == "bling") {
        switch (item) {
          case "order":
            return api.Sync.blingOrder()
              .then(() => {
                this.errors = null;
                this.setDone();
              })
              .catch((err) => {
                this.setError(err);
                this.setDone();
                throw err;
              });
          default:
            this.errors = "Opção indisponivel";
            return this.setDone();
        }
      } else {
        this.errors = "Selecione uma plataforma";
        return this.setDone();
      }
    });
  }
}

export default SyncStore;
