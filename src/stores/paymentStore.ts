import { observable, action } from "mobx";
import { userStore } from "./rootStore";

import api from "../services/api";

enum PaymentType {
  "_all",
  "amex",
  "boleto",
  "diners",
  "discover",
  "elo",
  "hipercard",
  "mastercard",
  "visa",
}

class PaymentStore {
  @observable inProgress = false;
  @observable done = false;
  @observable errors = undefined;
  @observable response = null;
  @observable currentPayment = {
    redirectUrl: undefined,
    error: null,
    response: undefined,
  };

  @observable paymentList: {
    _id: any;
    orderIncrementId: number;
    dueDate: Date;
    openDate: Date;
    confirmDate: any;
    paymentType: PaymentType;
    description: string;
    status: string;
    total: any;
  }[] = [];

  openNewTab(redirectUrl) {
    window.open(redirectUrl, "_blank");
  }

  @action _setDone() {
    this.inProgress = false;
    this.done = true;
    this.currentPayment.redirectUrl = undefined;
  }

  @action _unsetDone() {
    this.done = false;
    this.inProgress = true;
  }

  @action create() {
    this._unsetDone();
    return api.Payment.create()
      .then((payment) => console.log(payment))
      .catch((error) => (this.errors = error))
      .finally(this._setDone());
  }

  @action subscribe() {
    this._unsetDone();
    return api.Payment.subscribe()
      .then((result) => {
        result.redirect_url ? this.openNewTab(result.redirect_url) : "";
        userStore.pullUser();
      })
      .catch((error) => (this.errors = error))
      .finally(this._setDone());
  }

  @action unsubscribe() {
    this._unsetDone();
    return api.Payment.unsubscribe()
      .then(() => userStore.pullUser())
      .catch((error) => (this.errors = error))
      .finally(this._setDone());
  }

  @action get() {
    this._unsetDone();
    return api.Payment.get()
      .then((payment) => (this.paymentList = payment))
      .catch((error) => (this.errors = error))
      .finally(this._setDone());
  }
}

export default PaymentStore;
