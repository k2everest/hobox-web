import { observable, action } from "mobx";
import api from "../services/api";
import { productStore, plataformStore } from "./rootStore";

class RuleStore {
  @observable inProgress = false;
  @observable errors = undefined;
  @observable rules = undefined;

  @action async setOffsetFeeMercadolivre(plataformId, offsetFee) {
    return api.Rules.setOffsetFeeMercadolivre(plataformId, offsetFee)
      .then((rules) => (this.rules = rules))
      .catch(
        action((err) => {
          this.errors = err;
        })
      );
  }

  mergeArrays(...arrays) {
    let jointArray = [];

    arrays.forEach((array) => {
      jointArray = [...jointArray, ...array];
    });
    return [...new Set([...jointArray])];
  }

  @action async addProductsToRuleMercadolivre(rule: "offsetFee") {
    this.inProgress = true;
    let products = this.mergeArrays(
      plataformStore.rules.offsetFee.products,
      productStore.selectedsProducts.slice()
    );

    return api.Rules.setProductsToRuleMercadolivre({
      platformId: plataformStore.platform.id,
      rule,
      products: products,
    })
      .then(
        async () =>
          await plataformStore.getPlataform(plataformStore.platform.id)
      )
      .catch(
        action((err) => {
          this.errors = err;
        })
      )
      .finally(() => (this.inProgress = false));
  }

  @action async removeProductsToRuleMercadolivre(rule: "offsetFee") {
    console.log(productStore.selectedsProducts);
    let products = plataformStore.rules.offsetFee.products;
    products = products.filter(
      (val) => !productStore.selectedsProducts.includes(val)
    );
    return api.Rules.setProductsToRuleMercadolivre({
      platformId: plataformStore.platform.id,
      rule,
      products: products.slice(),
    })
      .then(
        async () =>
          await plataformStore.getPlataform(plataformStore.platform.id)
      )
      .catch(
        action((err) => {
          this.errors = err;
        })
      );
  }

  /*  @action async getRulesByPlatform() {
        return plataformStore.rules;
    } */
}

export default RuleStore;
