import { observable, action, reaction } from "mobx";
import api from "../services/api";
import { userStore, commonStore } from "./rootStore";

type ResetFields = {
  newPassword: string;
  confirmPassword: string;
  token: string;
};

class AuthStore {
  @observable inProgress = false;
  @observable errors = undefined;
  @observable logged = !!commonStore.user.token;
  @observable username = commonStore.user.username;
  @observable done = false;
  @observable successMessage = "";
  @observable resetedPassword = false;
  @observable recoverMode = false;
  @action setDone() {
    this.inProgress = false;
    this.done = true;
  }

  @observable values = {
    username: "",
    email: "",
    password: "",
  };

  constructor() {
    reaction(
      () => this.recoverMode,
      (recoverMode) => {
        if (recoverMode == true) {
          window.localStorage.setItem("recoverMode", "true");
        } else {
          window.localStorage.removeItem("recoverMode");
        }
      }
    );
  }

  @action setUsername(username) {
    this.values.username = username;
  }

  @action setEmail(email) {
    this.values.email = email;
  }

  @action setPassword(password) {
    this.values.password = password;
  }

  @action reset() {
    this.values.username = "";
    this.values.email = "";
    this.values.password = "";
  }

  @action login() {
    this.inProgress = true;
    this.errors = undefined;
    return api.Auth.login(this.values.email, this.values.password)
      .then((user) => commonStore.setUser(user))
      .catch(
        action((err) => {
          this.inProgress = false;
          this.errors = err;
          throw err;
        })
      );
  }

  @action googleResponse(response) {
    return api.Auth.authGoogle(response.accessToken)
      .then((user) => commonStore.setUser(user))
      .catch(
        action((err) => {
          this.inProgress = false;
          this.errors = err;
          throw err;
        })
      );
  }

  @action logout() {
    commonStore.setUser({ username: "", token: "" });
    userStore.forgetUser();
    return new Promise((res) => res());
  }

  @action setRecoverMode() {
    this.recoverMode = true;
    !window.localStorage.getItem("recoverMode")
      ? (window.location.href = "/")
      : null;
  }

  @action unsetRecoverMode() {
    window.localStorage.removeItem("recoverMode");
    window.location.href = "/";
  }

  @action forgotPassword() {
    this.inProgress = true;
    this.errors = undefined;
    return api.Auth.forgotPassword(this.values.email)
      .then(() => {
        this.successMessage = "E-mail enviado. Por favor, verifique seu e-mail";
        this.setDone();
      })
      .catch((err) => {
        this.errors = err.response.text;
        this.setDone();
      });
  }

  @action setPasswordReset(fields: ResetFields) {
    this.inProgress = true;
    this.errors = undefined;
    return api.Auth.resetPassword(fields)
      .then(() => {
        this.successMessage = "Senha alterada com sucesso";
        this.setDone();
        this.resetedPassword = true;
      })
      .catch((err) => {
        this.errors = err.response.body.message;
        this.setDone();
      });
  }
}

export default AuthStore;
