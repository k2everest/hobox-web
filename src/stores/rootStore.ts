import AuthStore from "./authStore";
import SyncStore from "./syncStore";
import ProductStore from "./productStore";
import Plataform from "./plataformStore";
import CommonStore from "./commonStore";
import HubStore from "./hubStore";
import OrderStore from "./orderStore";
import UserStore from "./userStore";
import PaymentStore from "./paymentStore";
import CategogyStore from "./categoryStore";
import RuleStore from "./ruleStore";

const commonStore = new CommonStore();
const authStore = new AuthStore();
const syncStore = new SyncStore();
const productStore = new ProductStore();
const plataformStore = new Plataform();
const hubStore = new HubStore();
const orderStore = new OrderStore();
const userStore = new UserStore();
const paymentStore = new PaymentStore();
const categoryStore = new CategogyStore();
const ruleStore = new RuleStore();

export default {
  authStore,
  syncStore,
  productStore,
  plataformStore,
  commonStore,
  hubStore,
  orderStore,
  userStore,
  categoryStore,
  paymentStore,
  ruleStore,
};
export {
  authStore,
  syncStore,
  productStore,
  plataformStore,
  commonStore,
  hubStore,
  orderStore,
  userStore,
  categoryStore,
  paymentStore,
  ruleStore,
};
