import { observable, action, computed } from "mobx";
import api from "../services/api";

export type Plataform = {
  id: string | null;
  plataform: string;
  settings: {};
};

const settingsEmpty = {
  url: "",
  url_api: "",
  password: "",
  username: "",
  store: "",
};

export type MappedCategories = {
  hobox: string;
  marketplace: string;
  marketplaceName?: string;
};

class PlataformStore {
  @observable inProgress = false;
  @observable done = false;
  @observable errors = undefined;
  @observable plataforms = undefined;
  @observable mappedOrderStatus = undefined;
  @observable hoboxStatus = undefined;
  @observable attributes = undefined;
  @observable attributesML = undefined;
  @observable categoriesML = [];
  @observable categoryML = undefined;
  @observable responseMessage: Array<any>;
  @observable isFinished = false;
  @observable currentCategory = "";
  @observable platform: {
    id: string;
    name: string;
  };
  @observable attributesTypes = [];
  @observable rules = undefined;

  @observable mappedCategories: MappedCategories[] = [];

  @observable mappedAttributes: {
    marketplaceName: string;
    hoboxName?: string;
    options?: {
      ml: string;
      hobox: string;
    }[];
    type?: string;
  }[] = [];

  @observable mappedAttributesTypes: {
    baseName: string;
    value: string;
    attributeType: string;
  }[] = [];

  @observable isOptionOpened: boolean = false;
  @observable modalSimple = false;

  @observable magentoSettings: {
    url: string;
    url_api: string;
    password: string;
    username: string;
    store: string;
  } = settingsEmpty;

  @observable mercadoLivreSettings: {
    email: string;
    nickname: string;
    syncOrdersFrom: string;
  } = { email: "", nickname: "", syncOrdersFrom: new Date().toDateString() };

  @observable amazonSettings: {
    sellerId: string;
    accessKeyId: string;
    secretKey: string;
  } = { sellerId: "", accessKeyId: "", secretKey: "" };

  @observable blingSettings: {
    apiKey: string;
    syncOrdersFrom: string;
    platforms: [];
  } = { apiKey: "", syncOrdersFrom: "", platforms: [] };

  constructor() {}

  @computed get PlatformLabel(): string {
    return {
      mercadolivre: "Mercado Livre",
      amazon: "Amazon",
      bling: "Bling",
    }[this.platform.name];
  }

  @action resetMagentoSettings() {
    this.magentoSettings = settingsEmpty;
  }

  @action resetAmazonSettings() {
    this.amazonSettings = { sellerId: "", accessKeyId: "", secretKey: "" };
  }

  @action resetBlingSettings() {
    this.blingSettings = {
      syncOrdersFrom: new Date().toDateString(),
      apiKey: "",
      platforms: [],
    };
  }

  @action _setDone() {
    this.inProgress = false;
    this.done = true;
  }

  @action _unsetDone() {
    this.isFinished = false;
    this.done = false;
    this.inProgress = true;
    this.errors = undefined;
    this.responseMessage = [];
  }

  @action delMagento(plataformId) {
    this._unsetDone();
    return api.Plataform.delMagento(plataformId)
      .then(() => this.listPlataforms())
      .catch(
        action((err) => {
          this.errors = err;
          throw err;
        })
      );
  }

  @action _setPlataforms(plataforms) {
    this.plataforms = plataforms;
    this.inProgress = false;
  }

  @action _setMagentoSettings(settings) {
    this.magentoSettings = settings;
    this.inProgress = false;
  }

  @action _setAttributesTypes(attributesTypes) {
    this.attributesTypes = attributesTypes;
    this.inProgress = false;
  }

  @action setMagento(plataform: Plataform) {
    this._unsetDone();
    return api.Plataform.setMagento(plataform)
      .then(() => {
        this.errors = null;
        this._setDone();
        this.responseMessage.push("Dados salvos com sucesso");
        this.responseMessage.push("Verificando conexão com Magento...");
        return api.Plataform.testMagento();
      })
      .then(() => {
        this._setDone();
        this.responseMessage.push(
          "Conexão com Magento estabelecida com sucesso"
        );
        this.isFinished = true;
      })
      .catch((err) => {
        /*     this.errors = err;
      this.responseMessage.push(err); */
        this.errors =
          "Houve uma falha. Favor verificar os dados da plataforma.";
        this._setDone();
        this.isFinished = true;
        throw err;
      });
  }

  @action setAmazon(platform: Plataform) {
    this._unsetDone();
    return api.Plataform.setAmazon(platform)
      .then(() => {
        this.errors = null;
        this._setDone();
        this.responseMessage.push("Dados salvos com sucesso");
        this.responseMessage.push("Verificando conexão com Amazon...");
        return api.Plataform.getAmazonStatus();
      })
      .then(() => {
        this._setDone();
        this.responseMessage.push(
          "Conexão com Amazon estabelecida com sucesso"
        );
        this.isFinished = true;
      })
      .catch((err) => {
        /*     this.errors = err;*/
        this.errors =
          "Houve uma falha. Favor verificar os dados da plataforma.";
        this.responseMessage.push(err.response.body.Message);
        this._setDone();
        this.isFinished = true;
        throw err;
      });
  }

  @action setBling(platform: Plataform) {
    this._unsetDone();
    return api.Plataform.setBling(platform)
      .then(() => {
        this.errors = null;
        this._setDone();
        this.responseMessage.push("Dados salvos com sucesso");
        this.responseMessage.push("Verificando conexão com Bling...");
        return api.Plataform.getBlingStatus();
      })
      .then(() => {
        this._setDone();
        this.responseMessage.push("Conexão com Bling estabelecida com sucesso");
        this.isFinished = true;
      })
      .catch((err) => {
        /*     this.errors = err;*/
        this.errors =
          "Houve uma falha. Favor verificar os dados da plataforma.";
        this.responseMessage.push(err.response.body.Message);
        this._setDone();
        this.isFinished = true;
        throw err;
      });
  }

  @action listPlataforms() {
    this._unsetDone();
    return api.Plataform.list()
      .then((plataforms) => this._setPlataforms(plataforms))
      .catch(
        action((err) => {
          console.log(err);
          this.errors = err;
          this._setDone();
          throw err;
        })
      );
  }

  @action authMercadoLivre() {
    this._unsetDone();
    return (
      api.Plataform.authMercadoLivre()
        .then((redirect) => (window.location.href = redirect))
        //.then(( redirect ) =>  window.location.href = redirect)
        .catch(
          action((err) => {
            this.errors = err;
            throw err;
          })
        )
    );
  }

  @action getMagento(plataformId) {
    this._unsetDone();
    return api.Plataform.getPlataform(plataformId)
      .then((plataform) => this._setMagentoSettings(plataform.settings))
      .catch(
        action((err) => {
          this.errors = err;
          console.log(err);
          throw err;
        })
      );
    //.finally(action(() => { this.loadingUser = false; }))
  }

  @action resetMercadoLivreSettings() {
    this.mercadoLivreSettings = {
      email: "",
      nickname: "",
      syncOrdersFrom: new Date().toDateString(),
    };
  }

  @action _setMercadoLivre(settings) {
    this.mercadoLivreSettings = settings;
    this.inProgress = false;
  }

  @action _setAmazon(settings) {
    this.amazonSettings = settings;
    this.inProgress = false;
  }

  @action _setBling(settings) {
    this.blingSettings = settings;
    this.inProgress = false;
  }

  @action setMercadoLivre(plataform) {
    this._unsetDone();
    return api.Plataform.setMercadoLivre(plataform)
      .then(() => this._setDone())
      .catch((err) => {
        this.errors = err.response.body.message;
        this._setDone();
        throw err;
      });
  }

  @action getMercadoLivre(plataformId) {
    this._unsetDone();
    return api.Plataform.getPlataform(plataformId)
      .then((platform) => this._setMercadoLivre(platform.settings))
      .catch(
        action((err) => {
          this.errors = err;
          console.log(err);
          throw err;
        })
      );
  }

  @action getAmazon(plataformId) {
    this._unsetDone();
    return api.Plataform.getPlataform(plataformId)
      .then((platform) => this._setAmazon(platform.settings))
      .catch(
        action((err) => {
          this.errors = err;
          throw err;
        })
      );
  }

  @action getBling(plataformId) {
    this._unsetDone();
    return api.Plataform.getPlataform(plataformId)
      .then((platform) => this._setBling(platform.settings))
      .catch(
        action((err) => {
          this.errors = err;
          throw err;
        })
      );
  }

  _setPlatform(platform: any) {
    this.platform = {
      id: platform._id,
      name: platform.plataform,
    };
    this.mappedAttributesTypes = platform.mappedAttributesTypes;
    this.rules = platform.rules;
  }

  @action getPlataform(plataformId) {
    this._unsetDone();
    return api.Plataform.getPlataform(plataformId)
      .then((platform) => this._setPlatform(platform))
      .catch(
        action((err) => {
          this.errors = err;
          console.log(err);
          throw err;
        })
      );
  }

  @action getOrderStatus(plataformId) {
    this._unsetDone();
    return api.Plataform.getOrderStatus(plataformId)
      .then((mappedOrderStatus) => this._setOrderStatus(mappedOrderStatus))
      .catch(
        action((err) => {
          this.errors = err;
          console.log(err);
          throw err;
        })
      );
  }

  @action getAttributesTypes() {
    this._unsetDone();
    return api.Plataform.getAttributesTypesAmazon()
      .then((attributesTypes) => this._setAttributesTypes(attributesTypes))
      .catch(
        action((err) => {
          this.errors = err;
          console.log(err);
          throw err;
        })
      );
  }

  @action _setOrderStatus(OrderStatus) {
    this.hoboxStatus = OrderStatus.hoboxStatus;
    this.mappedOrderStatus = OrderStatus.mappedStatus;
    this.inProgress = false;
  }

  @action setOrderStatus(plataformId, orderStatus) {
    this._unsetDone();
    return api.Plataform.setOrderStatus(plataformId, orderStatus)
      .then(() => this._setDone())
      .catch((err) => {
        this.errors = err.response.body.message;
        this._setDone();
        throw err;
      })
      .then(() => (this.modalSimple = true));
  }

  @action setAttributesTypes(plataformId, attributesTypes) {
    this._unsetDone();
    return api.Plataform.setAttributesTypes(plataformId, attributesTypes)
      .then(() => this._setDone())
      .catch((err) => {
        this.errors = err.response.body.message;
        this._setDone();
        throw err;
      })
      .then(() => (this.modalSimple = true));
  }

  @action setMappedAttributes(plataformId, mappedAttributes) {
    this._unsetDone();
    console.log("mapeamento", mappedAttributes);
    return api.Plataform.setMappedAttributes(plataformId, mappedAttributes)
      .then(() => this._setDone())
      .catch((err) => {
        this.errors = err.response.body.message;
        this._setDone();
        throw err;
      })
      .then(() => (this.modalSimple = true));
  }

  @action getAttributes() {
    this._unsetDone();
    return api.Hub.getAttributes()
      .then((attributes) => (this.attributes = attributes))
      .catch(
        action((err) => {
          this.errors = err;
          throw err;
        })
      )
      .then(() => this._setDone());
  }

  @action getMapppedAttributes(idMarketplace) {
    let tempMappedAttributes = [];
    this._unsetDone();
    tempMappedAttributes = this.attributesML
      .map((a) => {
        return { marketplaceName: a.name };
      })
      .slice();

    return api.Plataform.getMappedAttributes(idMarketplace)
      .then(
        (mappedAttributes) =>
          (this.mappedAttributes = tempMappedAttributes
            .map((local) => {
              let element = mappedAttributes.reduce((v = local, e) => {
                if (e.marketplaceName == local.marketplaceName) v = e;
                return v;
              }, local);
              return element;
            })
            .slice())
      )
      .catch((err) => {
        this.errors = err;
        throw err;
      })
      .then(() => this._setDone());
  }

  @action getAttributesPlatform(category = undefined) {
    this._unsetDone();
    return api.Plataform.getAttributes(
      this.platform.id,
      this.platform.name,
      category
    )
      .then((attributes) => (this.attributesML = attributes))
      .catch(
        action((err) => {
          this.errors = err;
          throw err;
        })
      )
      .then(() => this.getMapppedAttributes(this.platform.id))
      .then(() => this._setDone());
  }

  @action getCategoriesMercadolivre(marketplaceId, categoryId) {
    this._unsetDone();
    return api.Plataform.getCategoriesMercadolivre(marketplaceId, categoryId)
      .then((categories) => (this.categoriesML = categories))
      .catch(
        action((err) => {
          this.errors = err;
          throw err;
        })
      )
      .then(() => this._setDone());
  }

  @action getCategoryMercadolivre(marketplaceId, categoryId) {
    this.currentCategory = categoryId;
    this._unsetDone();
    return api.Plataform.getCategoriesMercadolivre(marketplaceId, categoryId)
      .then((categories) => (this.categoryML = categories))
      .catch(
        action((err) => {
          this.errors = err;
          throw err;
        })
      )
      .then(() => this._setDone());
  }

  @action setMappedCategory(plataformId, mappedCategories: MappedCategories) {
    console.log("mapped categoires", mappedCategories);
    this._unsetDone();
    return api.Plataform.setMappedCategories(plataformId, mappedCategories)
      .then(() => this._setDone())
      .then(() => this.getMappedCategories(plataformId))
      .catch((err) => {
        this.errors = err.response.body;
        this._setDone();
        throw err;
      })
      .then(() => {
        this.categoryML = undefined;
        this.modalSimple = true;
      });
  }

  @action getMappedCategories(plataformId) {
    this._unsetDone();
    return api.Plataform.getMappedCategories(plataformId)
      .then((mappedCategories) => (this.mappedCategories = mappedCategories))
      .catch((err) => {
        this.errors = err;
        this._setDone();
        throw err;
      })
      .then(() => this._setDone());
  }
}

export default PlataformStore;
