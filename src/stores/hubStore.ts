import { observable, action } from "mobx";
import api from "../services/api";

class HubStore {
  @observable inProgress = false;
  @observable done = false;
  @observable errors = undefined;
  @observable categories = undefined;
  @observable orderStatus = undefined;
  @observable attributes = undefined;

  constructor() {}

  @action getCategories() {
    //this._unsetDone();
    return api.Hub.getCategories()
      .then((categories) => (this.categories = categories))
      .catch(
        action((err) => {
          this.errors = err;
          throw err;
        })
      )
      .then(() => this._setDone());
  }

  @action getAttributes() {
    //this._unsetDone();
    return api.Hub.getAttributes()
      .then((attributes) => (this.attributes = attributes))
      .catch(
        action((err) => {
          this.errors = err;
          throw err;
        })
      )
      .then(() => this._setDone());
  }

  @action getOrderStatus() {
    //this._unsetDone();
    return api.Hub.getOrderStatus()
      .then((orderStatus) => {
        console.log("v", orderStatus);
        this.orderStatus = orderStatus;
      })
      .catch((err) => {
        this.errors = err;
        console.log(err);
        this._setDone();
      })
      .then(() => this._setDone());
  }

  @action syncAttributes() {
    this._unsetDone();
    return api.Hub.syncAttributes()
      .catch(
        action((err) => {
          this.errors = err;
          this._setDone();
        })
      )
      .then(() => this._setDone())
      .then(() => this.getAttributes());
  }

  @action syncCategories() {
    this._unsetDone();
    return api.Hub.syncCategories()
      .catch(
        action((err) => {
          this.errors = err;
          this._setDone();
        })
      )
      .then(() => this._setDone())
      .then(() => this.getCategories());
  }

  @action syncOrderStatus() {
    this._unsetDone();
    return api.Hub.syncOrderStatus()
      .then(() => this._setDone())
      .catch((err) => {
        console.log("teste");
        this.errors = err;
        this._setDone();
      })
      .then(() => this._setDone())
      .then(() => this.getOrderStatus());
  }

  @action _setDone() {
    this.inProgress = false;
    this.done = true;
  }

  @action _unsetDone() {
    this.errors = undefined;
    this.done = false;
    this.inProgress = true;
  }
}

export default HubStore;
