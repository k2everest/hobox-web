import { observable, action, reaction } from "mobx";
import { history } from "../containers/Route/RouteStore";
//import agent from '../services/api';

class CommonStore {
  //@observable token = window.localStorage.getItem('jwt');
  @observable user = {
    token: window.localStorage.getItem("jwt"),
    username: window.localStorage.getItem("username"),
  };
  @observable logged = !!this.user.token;

  @observable appLoaded = false;
  @observable isLoadingTags = false;

  constructor() {
    reaction(
      () => this.user,
      (user) => {
        if (user.token) {
          window.localStorage.setItem("jwt", user.token);
          window.localStorage.setItem("username", user.username);
          this.logged = true;
        } else {
          localStorage.clear();
          if (
            history.location.pathname != "/login" &&
            !window.localStorage.getItem("jwt")
          )
            window.location.href = "/login";
        }
      }
    );
  }

  /*  @action loadTags() {
    this.isLoadingTags = true;
    return agent.Tags.getAll()
      .then(action(({ tags }) => { this.tags = tags.map(t => t.toLowerCase()); }))
      .finally(action(() => { this.isLoadingTags = false; }))
  } */

  @action setUser(user) {
    this.user = user;
  }

  @action setAppLoaded() {
    this.appLoaded = true;
  }
}

export default CommonStore;
