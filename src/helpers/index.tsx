let removeDuplicateUsingFilter = function (arr) {
  console.log(arr);
  let unique_array = arr.filter(function (elem, index, self) {
    return index == self.indexOf(elem);
  });
  console.log(unique_array);
  return unique_array;
};

let dedup = function (arr) {
  var hashTable = {};

  return arr.filter(function (el) {
    var key = JSON.stringify(el);
    var match = Boolean(hashTable[key]);

    return match ? false : (hashTable[key] = true);
  });
};

let formatBrMoney = function (n) {
  n = parseFloat(n);
  return (
    "R$ " +
    n
      .toFixed(2)
      .replace(".", ",")
      .replace(/(\d)(?=(\d{3})+\,)/g, "$1.")
  );
};

export { removeDuplicateUsingFilter, dedup, formatBrMoney };
