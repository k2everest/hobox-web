const webpack = require('webpack');
const merge = require('webpack-merge');
const commons = require('./webpack.commons.js');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = merge(commons, {
    output: {
        publicPath: '/public/'
    },
    plugins: [
        new CleanWebpackPlugin(['out']),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': "'production'"
        }),
         new webpack.optimize.CommonsChunkPlugin({
          name: 'vendors',
          filename: 'js/vendor.min.js',
          minChunks: function (module) {
              return isExternal(module);
          }
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
              warnings: true
            }
        })
    ]
});

function isExternal(module) {
  var userRequest = module.userRequest;

  if (typeof userRequest !== 'string') {
      return false;
  }

  return userRequest.indexOf('/node_modules/') >= 0;
}
